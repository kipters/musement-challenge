//
//  MainViewController.m
//  MusementChallenge
//
//  Created by Fabio on 18/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import "MainViewController.h"
#import "SearchViewController.h"
#import "EventDetailsViewController.h"
#import "../Categories/UIViewController+StatusBarColor.h"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UIView *fakeSearchBar;
@property (strong, nonatomic) UITapGestureRecognizer *fakeSearchTapRecognizer;
@property (strong, nonatomic) SWGEvent *event;
@property (strong, nonatomic) SearchViewController *searchViewController;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setStatusBarColor:UIColor.clearColor];
    
    self.fakeSearchBar.layer.cornerRadius = 4;
    self.fakeSearchBar.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fakeSearchTapped:)];
    [self.fakeSearchBar addGestureRecognizer:tr];
    
    self.fakeSearchTapRecognizer = tr;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        self.searchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        self.searchViewController.navDelegate = self;
    });
    
    [self addChildViewController:self.searchViewController];
    [self.view addSubview:self.searchViewController.view];
    [self.searchViewController didMoveToParentViewController:self];
    self.searchViewController.view.hidden = YES;
    [self setStatusBarColor:UIColor.clearColor];
    
    CAGradientLayer *shadowLayer = [CAGradientLayer layer];
    CGRect frame = self.shadowView.frame;
    shadowLayer.frame = frame;
    shadowLayer.colors = @[
                           (id)[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4].CGColor,
                           (id)[UIColor colorWithRed:0 green:0 blue:0 alpha:0.1].CGColor
                           ];
    
    [self.shadowView.layer insertSublayer:shadowLayer atIndex:0];
}

- (void)fakeSearchTapped:(UITapGestureRecognizer *) tapGestureRecognizer {
    //[self performSegueWithIdentifier:@"presentSearch" sender:self];
    [self setBrandingStatusBarColor];
    [self animateSearchViewControllerHiddenTo:NO];
    [self.searchViewController focusSearchField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetailsSegue"]) {
        EventDetailsViewController *edvc = (EventDetailsViewController *)segue.destinationViewController;
        edvc.event = self.event;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (void)showEvent:(SWGEvent *)event {
    self.event = event;
    [self setStatusBarColor:UIColor.clearColor];
    [self performSegueWithIdentifier:@"showDetailsSegue" sender:self];
}

- (void)hideSearch {
    [self setStatusBarColor:UIColor.clearColor];
    [self animateSearchViewControllerHiddenTo:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.searchViewController.view.hidden) {
        [self setStatusBarColor:UIColor.clearColor];
    } else {
        [self setBrandingStatusBarColor];
    }
}

- (void)animateSearchViewControllerHiddenTo:(BOOL) hidden {
    [UIView transitionWithView:self.searchViewController.view duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.searchViewController.view.hidden = hidden;
    } completion:nil];
}

@end
