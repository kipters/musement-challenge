//
//  SearchViewController.h
//  MusementChallenge
//
//  Created by Fabio on 18/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Api.h"

@protocol SearchViewControllerDelegate
- (void)showEvent:(SWGEvent *)event;
- (void)hideSearch;
@end

@interface SearchViewController : UIViewController
@property (weak) id <SearchViewControllerDelegate> navDelegate;
- (void)focusSearchField;
@end
