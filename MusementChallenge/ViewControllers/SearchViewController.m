//
//  SearchViewController.m
//  MusementChallenge
//
//  Created by Fabio on 18/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import "SearchViewController.h"
#import "../Categories/UIViewController+StatusBarColor.h"
#import "../Views/SearchTableViewCell.h"
#import "../AppDelegate.h"
#import "../FilterHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SearchViewController () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *searchField;
- (IBAction)closeButtonTapped:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;
@property (weak, nonatomic) IBOutlet UIView *noDataView;

@property (strong, nonatomic) SWGCityApi *api;
@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (strong, nonatomic) NSMutableArray<SWGEvent> *items;
@property (assign) unsigned long offset;
@property (assign) BOOL endReached;
@property (strong, nonnull) NSString *searchTerm;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchField.delegate = self;
    self.resultsTableView.delegate = self;
    self.resultsTableView.dataSource = self;
    
    SWGApiClient *client = AppDelegate.sharedApiClient;
    self.api = [[SWGCityApi alloc] initWithApiClient:client];
    
    self.items = [[NSMutableArray<SWGEvent> alloc] init];
    
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    NSNotificationCenter *nc = NSNotificationCenter.defaultCenter;
    [nc addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardDidShow:(NSNotification *)notification {
    [self.view addGestureRecognizer:self.tapRecognizer];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self.view removeGestureRecognizer:self.tapRecognizer];
}

- (void)viewTapped:(UITapGestureRecognizer *)sender {
    [self.searchField resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.endReached || indexPath.row != self.items.count - 3) {
        return;
    }
    NSNumber *offset = [[NSNumber alloc] initWithUnsignedLong:self.offset];
    [self.api citiesCityIdActivitiesGetWithCityId:[[NSNumber alloc] initWithInt:1] xMusementVersion:nil acceptLanguage:nil xMusementCurrency:nil vertical:nil category:nil venue:nil editorialCategory:nil dateFrom:nil dateTo:nil offset:offset limit:nil sortBy:nil completionHandler:^(NSArray<SWGEvent> *output, NSError *error) {
        
        if (output.count == 0) {
            self.endReached = YES;
            return;
        }
        
        NSMutableArray<SWGEvent> *newEvents = [FilterHelper mutableArrayFromArray:output withFilter:self.searchTerm];
        
        self.offset += output.count;
        
        [self.items addObjectsFromArray:newEvents];
        [self reloadTableData];
    }];
}

- (IBAction)closeButtonTapped:(UIButton *)sender {
    [self.searchField resignFirstResponder];
    [self.navDelegate hideSearch];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.activityIndicator startAnimating];
    [self.api citiesCityIdActivitiesGetWithCityId:[[NSNumber alloc] initWithInt:1] xMusementVersion:nil acceptLanguage:nil xMusementCurrency:nil vertical:nil category:nil venue:nil editorialCategory:nil dateFrom:nil dateTo:nil offset:nil limit:nil sortBy:nil completionHandler:^(NSArray<SWGEvent> *output, NSError *error) {
        [self.activityIndicator stopAnimating];
        
        self.offset = output.count;
        
        self.searchTerm = textField.text;
        self.items = [FilterHelper mutableArrayFromArray:output withFilter:self.searchTerm];
        
        [self reloadTableData];
        self.endReached = NO || output.count < 100;
        [self.resultsTableView setContentOffset:CGPointZero animated:YES];
    }];
    
    [textField resignFirstResponder];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SWGEvent *event = self.items[indexPath.row];
    [self.navDelegate showEvent:event];
    [self dismissViewControllerAnimated:NO completion:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row == self.items.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"paddingCell"];
        return cell;
    }
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchResultCell" forIndexPath:indexPath];
    SWGEvent *event = self.items[indexPath.row];
    
    NSURL *coverImageUrl = [NSURL URLWithString:event.coverImageUrl];
    [cell.coverView sd_setImageWithURL:coverImageUrl];
    cell.captionLabel.text = event.title;
    SWGCategory *category = event.categories[0];
    cell.tagLabel.text = category.name.uppercaseString;
    cell.dayLabel.text = event.operationalDays;
    
    NSString *price = event.retailPrice.formattedValue;
    if ([price hasSuffix:@".00"]) {
        price = [price stringByReplacingOccurrencesOfString:@".00" withString:@""];
    }
    cell.priceLabel.text = price;
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section { 
    return self.items.count > 0 ? self.items.count + 1 : 0;
}

- (void)focusSearchField {
    [self.searchField becomeFirstResponder];
}

- (void)reloadTableData {
    NSString *trimmedSpace = [self.searchTerm stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.noDataView.hidden = self.items.count > 0 || [trimmedSpace isEqualToString:@""];
    self.resultsTableView.hidden = !self.noDataView.hidden;
    [self.resultsTableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == self.items.count ? 10 : 280;
}

@end
