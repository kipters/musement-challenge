//
//  EventDetailsViewController.h
//  MusementChallenge
//
//  Created by Fabio on 22/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Api.h"

@interface EventDetailsViewController : UIViewController
@property (strong, nonatomic) SWGEvent *event;
@end
