//
//  MainViewController.h
//  MusementChallenge
//
//  Created by Fabio on 18/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchViewController.h"

@interface MainViewController : UIViewController <SearchViewControllerDelegate>

@end
