//
//  EventDetailsViewController.m
//  MusementChallenge
//
//  Created by Fabio on 22/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import "EventDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "../Categories/UIViewController+StatusBarColor.h"
#import "../Categories/UIColor+Interpolate.h"
#include <math.h>

@interface EventDetailsViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverContainerHeight;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *coverOverlayView;
@property (weak, nonatomic) IBOutlet UIView *coverShadowView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverShadowHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *navBarShadowView;
- (IBAction)shareButtonTapped:(UIButton *)sender;
- (IBAction)dummyTapped:(UIButton *)sender;

@end

@implementation EventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationBar *navbar = self.navigationController.navigationBar;
    [navbar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    navbar.shadowImage = [UIImage new];
    navbar.translucent = YES;
    self.navigationController.view.backgroundColor = UIColor.clearColor;
    navbar.backgroundColor = UIColor.clearColor;
    navbar.tintColor = UIColor.whiteColor;
    
    NSURL *coverImageUrl = [[NSURL alloc] initWithString:self.event.coverImageUrl];
    [self.coverImageView sd_setImageWithURL:coverImageUrl];
    
    NSLog(@"%lu", (long)self.event.categories.count);
    
    SWGCategory *category = self.event.categories[0];
    self.categoryLabel.text = category.name.uppercaseString;
    self.titleLabel.text = self.event.title;
    self.descriptionLabel.text = self.event.about;
    
    NSMutableString *captionString = [NSMutableString stringWithString:self.event.city.name];
    for (SWGFlavour *flavour in self.event.flavours) {
        [captionString appendFormat:@" • %@", flavour.name];
    }
    self.captionLabel.text = captionString;
    
    self.ratingLabel.text = [NSString stringWithFormat:@"Rating: %.2f", self.event.reviewsAvg.floatValue];
    self.reviewLabel.text = [NSString stringWithFormat:@"%d recensioni", (int) self.event.reviewsNumber.intValue];
    
    NSString *price = self.event.retailPrice.formattedValue;
    if ([price hasSuffix:@".00"]) {
        price = [price stringByReplacingOccurrencesOfString:@".00" withString:@""];
    }
    self.priceLabel.text = price;
    
    self.scrollView.contentOffset = CGPointMake(0, -293);
    self.coverContainerHeight.constant = 293;
    self.scrollView.delegate = self;
    
    CAGradientLayer *shadowLayer = [CAGradientLayer layer];
    CGRect frame = self.coverShadowView.frame;
    shadowLayer.frame = frame;
    shadowLayer.colors = @[
                           (id)[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6].CGColor,
                           (id)[UIColor colorWithRed:0 green:0 blue:0 alpha:0].CGColor
                           ];
    
    [self.coverShadowView.layer insertSublayer:shadowLayer atIndex:0];
    

    CGFloat statusBarHeight = UIApplication.sharedApplication.statusBarFrame.size.height;
    self.coverShadowHeightConstraint.constant = statusBarHeight + 44;
    
    CALayer *nsl = self.navBarShadowView.layer;
    CGFloat shadowColor = 98/255.0;
    nsl.shadowColor = [UIColor colorWithRed:shadowColor green:shadowColor blue:shadowColor alpha:1.0].CGColor;  
    nsl.shadowOpacity = .6;
    nsl.shadowRadius = 4.5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat y = scrollView.contentOffset.y;
    CGFloat delta = y + 293;
    
    if (y > 0)
        return;
    
    CGFloat height = -y;
    
    if (delta <= 0) {
        self.coverContainerHeight.constant = height - delta * 0.1;
        self.navigationController.navigationBar.backgroundColor = UIColor.clearColor;
        
        self.coverShadowView.hidden = NO;
        return;
    } else {
        self.coverContainerHeight.constant = height;
        
        if (delta > 230)
            delta = 230;
        
        CGFloat alpha = (delta - 120) / 100;
        alpha = fmin(alpha, 1.0);
        self.coverOverlayView.alpha = alpha;
        UIColor *color = alpha == 1 ? UIColor.whiteColor : UIColor.clearColor;
        self.navBarShadowView.hidden = alpha != 1;
        self.navigationController.navigationBar.backgroundColor = color;
        
        if (alpha > 0) {
            self.coverShadowView.hidden = YES;
            [self setBrandingStatusBarColor];
        } else {
            self.coverShadowView.hidden = NO;
            [self setStatusBarColor:UIColor.clearColor];
        }
        
        UIColor *tint = [UIColor interpolateHSVColorFrom:UIColor.whiteColor to:UIColor.blackColor withFraction:alpha];
        self.navigationController.navigationBar.tintColor = tint;
        
        return;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    [self setStatusBarColor:UIColor.clearColor];
}

- (IBAction)shareButtonTapped:(id)sender {
    NSURL *url = [NSURL URLWithString:self.event.url];
    UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems:@[self.event.title, url] applicationActivities:nil];
    avc.popoverPresentationController.sourceView = self.view;
    [self presentViewController:avc animated:YES completion:nil];
}

- (IBAction)dummyTapped:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Dummy button" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
