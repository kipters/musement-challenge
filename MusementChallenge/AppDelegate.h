//
//  AppDelegate.h
//  MusementChallenge
//
//  Created by Fabio on 18/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../SwaggerClient/SwaggerClient/Core/SWGApiClient.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (SWGApiClient *) sharedApiClient;

@end

