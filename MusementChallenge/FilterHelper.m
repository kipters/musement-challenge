//
//  FilterHelper.m
//  MusementChallenge
//
//  Created by Fabio Di Peri on 24/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import "FilterHelper.h"

@implementation FilterHelper
+ (NSMutableArray<SWGEvent> *)mutableArrayFromArray:(NSArray<SWGEvent> *)array withFilter:(NSString *)filter {
    NSString *searchText = [filter stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([searchText isEqualToString:@""]) {
        NSMutableArray<SWGEvent> *value = [NSMutableArray<SWGEvent> arrayWithArray: array];
        return value;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@", searchText];
    NSArray<SWGEvent> *filteredArray = (NSArray<SWGEvent> *) [array filteredArrayUsingPredicate:predicate];
    return (NSMutableArray<SWGEvent> *) [NSMutableArray arrayWithArray:filteredArray];
}
@end
