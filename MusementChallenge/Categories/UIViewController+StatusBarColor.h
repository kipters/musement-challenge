//
//  UIViewController+StatusBarColor.h
//  MusementChallenge
//
//  Created by Fabio on 22/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (StatusBarColor)
- (void)setStatusBarColor:(UIColor *) color;
- (void)setBrandingStatusBarColor;
@end
