//
//  UIViewController+StatusBarColor.m
//  MusementChallenge
//
//  Created by Fabio on 22/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import "UIViewController+StatusBarColor.h"

@implementation UIViewController (StatusBarColor)

- (void)setStatusBarColor:(UIColor *)color {
    UIView *statusBar = [[UIApplication.sharedApplication valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

- (void)setBrandingStatusBarColor {
    UIColor *brandingColor = [UIColor colorWithRed:251/255.0 green:86/255.0 blue:60/255.0 alpha:1.0];
    [self setStatusBarColor:brandingColor];
}

@end
