//
//  UIColor+Interpolate.h
//  MusementChallenge
//
//  Created by Fabio on 22/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Interpolate)

+ (UIColor *)interpolateRGBColorFrom:(UIColor *)start to:(UIColor *)end withFraction:(float)f;
+ (UIColor *)interpolateHSVColorFrom:(UIColor *)start to:(UIColor *)end withFraction:(float)f;

@end
