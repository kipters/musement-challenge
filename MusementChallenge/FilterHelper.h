//
//  FilterHelper.h
//  MusementChallenge
//
//  Created by Fabio Di Peri on 24/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Api.h"

@interface FilterHelper : NSObject
+ (NSMutableArray<SWGEvent> *)mutableArrayFromArray:(NSArray<SWGEvent> *)array withFilter:(NSString *)filter;
@end
