//
//  SearchTableViewCell.m
//  MusementChallenge
//
//  Created by Fabio Di Peri on 23/04/2018.
//  Copyright © 2018 Kipters. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
