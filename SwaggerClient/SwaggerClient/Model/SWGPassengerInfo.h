#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGPassengerInfo
@end

@interface SWGPassengerInfo : SWGObject


@property(nonatomic) NSString* salutation;

@property(nonatomic) NSString* firstname;

@property(nonatomic) NSString* lastname;

@property(nonatomic) NSDate* dateOfBirth;

@property(nonatomic) NSString* passport;

@property(nonatomic) NSString* email;

@property(nonatomic) NSDate* passportExpiryDate;

@property(nonatomic) NSString* nationality;

@property(nonatomic) NSString* medicalNotes;

@property(nonatomic) NSString* address;
/* /_** [optional]
 */
@property(nonatomic) NSString* fanCard;

@end
