#import "SWGExternalLink.h"

@implementation SWGExternalLink

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"abstract": @"abstract", @"url": @"url", @"coverImageUrl": @"cover_image_url", @"countryName": @"country_name", @"authorName": @"author_name", @"authorUrl": @"author_url", @"authorAvatarImageUrl": @"author_avatar_image_url" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"abstract", @"url", @"coverImageUrl", @"countryName", @"authorName", @"authorUrl", @"authorAvatarImageUrl"];
  return [optionalProperties containsObject:propertyName];
}

@end
