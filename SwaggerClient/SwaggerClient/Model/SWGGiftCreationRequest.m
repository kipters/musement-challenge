#import "SWGGiftCreationRequest.h"

@implementation SWGGiftCreationRequest

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"eventId": @"event_id", @"donorName": @"donor_name", @"message": @"message", @"pictureUrl": @"picture_url", @"print": @"print", @"recipientEmail": @"recipient_email", @"recipientName": @"recipient_name", @"featureCode": @"feature_code", @"productsWithQuantities": @"products_with_quantities" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"eventId", @"donorName", @"message", @"pictureUrl", @"print", @"recipientEmail", @"recipientName", @"featureCode", @"productsWithQuantities"];
  return [optionalProperties containsObject:propertyName];
}

@end
