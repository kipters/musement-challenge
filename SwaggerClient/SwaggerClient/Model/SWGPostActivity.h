#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGPostActivity
@end

@interface SWGPostActivity : SWGObject

/* Activity type | Can be 'ACTIVITY' or 'TOUR' [optional]
 */
@property(nonatomic) NSString* type;
/* Seller gateway. | Get them using GET /seller-gateways [optional]
 */
@property(nonatomic) NSString* sellerGateway;
/* Vertical. [optional]
 */
@property(nonatomic) NSNumber* vertical;
/* Categories, related to the vertical, for the activity. [optional]
 */
@property(nonatomic) NSArray<NSNumber*>* categories;
/* Highlights, for the activity | Get them using GET /activity-taxonomies/highlights. [optional]
 */
@property(nonatomic) NSArray<NSString*>* highlights;
/* Activity duration as ISO-8601 period. Only day, hours and minutes are accepted. [optional]
 */
@property(nonatomic) NSString* duration;
/* What's included ? | Get them using GET /activity-taxonomies/features [optional]
 */
@property(nonatomic) NSArray<NSString*>* inclusions;
/* What's excluded | Get them using GET /activity-taxonomies/features [optional]
 */
@property(nonatomic) NSArray<NSString*>* exclusions;
/* Features | Get them using GET /features [optional]
 */
@property(nonatomic) NSArray<NSString*>* features;
/* Services | Get them using GET /services [optional]
 */
@property(nonatomic) NSArray<NSString*>* services;
/* Is the mobile ticket accepted ? [optional]
 */
@property(nonatomic) NSNumber* mobileTicketIsAccepted;
/* Cutoff time as ISO-8601 period. Only days, hours and minutes are accepted. [optional]
 */
@property(nonatomic) NSString* cutoffTime;
/* Max period of time the supplier need to confirm the reservation. Only days, hours and minutes are accepted as ISO-8601 period. [optional]
 */
@property(nonatomic) NSString* maxConfirmationTime;
/* Internal code of the partner [optional]
 */
@property(nonatomic) NSString* partnerInternalCode;
/* Partner UUID [optional]
 */
@property(nonatomic) NSString* partner;
/* A list of people who can have reduction. [optional]
 */
@property(nonatomic) NSString* reduction;
/* City identifier. Get the value of id from the call GET /api/v3/cities [optional]
 */
@property(nonatomic) NSNumber* city;
/* Venues identifier. Get the value of id from the call GET /api/v3/venues [optional]
 */
@property(nonatomic) NSArray<NSNumber*>* venues;
/*   * ONLINE: The activity is visible on musement.com and it can be booked  * ARCHIVED: The activity is visible on musement.com but it cannot be booked  * INACTIVE: The activity is not visible on musement.com [optional]
 */
@property(nonatomic) NSString* status;

@end
