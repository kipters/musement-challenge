#import "SWGReviewsAggregatedInfo.h"

@implementation SWGReviewsAggregatedInfo

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_1": @"1", @"_2": @"2", @"_3": @"3", @"_4": @"4", @"_5": @"5" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_1", @"_2", @"_3", @"_4", @"_5"];
  return [optionalProperties containsObject:propertyName];
}

@end
