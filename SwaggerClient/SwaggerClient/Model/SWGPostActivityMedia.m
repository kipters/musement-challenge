#import "SWGPostActivityMedia.h"

@implementation SWGPostActivityMedia

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.isCover = @(NO);
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"url": @"url", @"title": @"title", @"isCover": @"is_cover" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"title", @"isCover"];
  return [optionalProperties containsObject:propertyName];
}

@end
