#import "SWGWish.h"

@implementation SWGWish

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"code": @"code", @"itemId": @"item_id", @"itemType": @"item_type", @"title": @"title", @"_description": @"description", @"coverImageUrl": @"cover_image_url", @"city": @"city", @"latitude": @"latitude", @"longitude": @"longitude" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"code", @"itemId", @"itemType", @"title", @"_description", @"coverImageUrl", @"city", @"latitude", @"longitude"];
  return [optionalProperties containsObject:propertyName];
}

@end
