#import "SWGEditorialCategory.h"

@implementation SWGEditorialCategory

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"imageUrl": @"image_url", @"active": @"active", @"name": @"name", @"subTitle": @"sub_title", @"content": @"content", @"slug": @"slug", @"minEventsForFiltering": @"min_events_for_filtering" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"imageUrl", @"active", @"name", @"subTitle", @"content", @"slug", @"minEventsForFiltering"];
  return [optionalProperties containsObject:propertyName];
}

@end
