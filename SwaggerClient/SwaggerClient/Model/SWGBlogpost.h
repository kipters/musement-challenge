#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGBlogger.h"
#import "SWGBlogpostCategory.h"
#import "SWGCategory.h"
#import "SWGCity.h"
#import "SWGMusementList.h"
#import "SWGVenue.h"
#import "SWGVertical.h"
@protocol SWGBlogger;
@class SWGBlogger;
@protocol SWGBlogpostCategory;
@class SWGBlogpostCategory;
@protocol SWGCategory;
@class SWGCategory;
@protocol SWGCity;
@class SWGCity;
@protocol SWGMusementList;
@class SWGMusementList;
@protocol SWGVenue;
@class SWGVenue;
@protocol SWGVertical;
@class SWGVertical;



@protocol SWGBlogpost
@end

@interface SWGBlogpost : SWGObject


@property(nonatomic) NSString* _id;

@property(nonatomic) NSString* title;

@property(nonatomic) NSString* abstract;

@property(nonatomic) NSDate* publishDate;

@property(nonatomic) NSString* url;

@property(nonatomic) NSString* backgroundImage;

@property(nonatomic) NSString* backgroundImageCredits;

@property(nonatomic) SWGCategory* category;

@property(nonatomic) SWGVertical* vertical;

@property(nonatomic) NSArray<SWGBlogpostCategory>* categories;

@property(nonatomic) SWGMusementList* list;

@property(nonatomic) SWGCity* city;

@property(nonatomic) SWGVenue* venue;

@property(nonatomic) SWGBlogger* author;

@property(nonatomic) NSString* shareImage;

@end
