#import "SWGGenericEvent.h"

@implementation SWGGenericEvent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"verticals": @"verticals", @"whereIs": @"where_is", @"whenIs": @"when_is", @"latitude": @"latitude", @"longitude": @"longitude", @"date": @"date", @"startAt": @"start_at", @"endAt": @"end_at", @"temporary": @"temporary", @"price": @"price", @"phone": @"phone", @"website": @"website", @"rating": @"rating", @"votes": @"votes", @"city": @"city", @"venue": @"venue", @"images": @"images" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"verticals", @"whereIs", @"whenIs", @"latitude", @"longitude", @"date", @"startAt", @"endAt", @"temporary", @"price", @"phone", @"website", @"rating", @"votes", @"city", @"venue", @"images"];
  return [optionalProperties containsObject:propertyName];
}

@end
