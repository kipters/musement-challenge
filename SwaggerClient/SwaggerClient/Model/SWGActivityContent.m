#import "SWGActivityContent.h"

@implementation SWGActivityContent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"locale": @"locale", @"title": @"title", @"_description": @"description", @"when": @"when", @"operationalDays": @"operational_days", @"isDefault": @"is_default" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"locale", @"title", @"_description", @"when", @"operationalDays", @"isDefault"];
  return [optionalProperties containsObject:propertyName];
}

@end
