#import "SWGGiftboxCustomizationInfo.h"

@implementation SWGGiftboxCustomizationInfo

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"coverImageUrl": @"cover_image_url", @"donorName": @"donor_name", @"recipientEmail": @"recipient_email", @"recipientName": @"recipient_name", @"message": @"message" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"coverImageUrl", @"donorName", @"recipientEmail", @"recipientName", @"message"];
  return [optionalProperties containsObject:propertyName];
}

@end
