#import "SWGOrder.h"

@implementation SWGOrder

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"identifier": @"identifier", @"uuid": @"uuid", @"date": @"date", @"status": @"status", @"tickets": @"tickets", @"trustpilotUrl": @"trustpilot_url", @"customer": @"customer", @"items": @"items", @"totalPrice": @"total_price", @"supplierTotalPrice": @"supplier_total_price", @"affiliateId": @"affiliate_id", @"promoCodes": @"promo_codes", @"extraData": @"extra_data" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"identifier", @"uuid", @"date", @"status", @"tickets", @"trustpilotUrl", @"customer", @"items", @"totalPrice", @"supplierTotalPrice", @"affiliateId", @"promoCodes", @"extraData"];
  return [optionalProperties containsObject:propertyName];
}

@end
