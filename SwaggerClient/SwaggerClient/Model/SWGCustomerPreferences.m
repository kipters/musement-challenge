#import "SWGCustomerPreferences.h"

@implementation SWGCustomerPreferences

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"varNewsletterFrequency": @"newsletter_frequency", @"varNewsletterFromThirdparty": @"newsletter_from_thirdparty", @"interests": @"interests", @"travelWith": @"travel_with" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"varNewsletterFrequency", @"varNewsletterFromThirdparty", @"interests", @"travelWith"];
  return [optionalProperties containsObject:propertyName];
}

@end
