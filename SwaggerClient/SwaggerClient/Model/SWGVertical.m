#import "SWGVertical.h"

@implementation SWGVertical

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"name": @"name", @"active": @"active", @"code": @"code", @"count": @"count", @"slug": @"slug", @"url": @"url", @"metaTitle": @"meta_title", @"metaDescription": @"meta_description", @"coverImageUrl": @"cover_image_url", @"topEvents": @"top_events", @"relevance": @"relevance" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"count", @"metaTitle", @"metaDescription", @"coverImageUrl", @"topEvents", ];
  return [optionalProperties containsObject:propertyName];
}

@end
