#import "SWGPostPromoIntent.h"

@implementation SWGPostPromoIntent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"promo": @"promo", @"triggeringProduct": @"triggering_product", @"targetProduct": @"target_product" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"promo", @"triggeringProduct", @"targetProduct"];
  return [optionalProperties containsObject:propertyName];
}

@end
