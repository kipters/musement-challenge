#import "SWGPostGiftboxCustomizationInfo.h"

@implementation SWGPostGiftboxCustomizationInfo

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"keepSecret": @"keep_secret", @"donorName": @"donor_name", @"message": @"message", @"pictureUrl": @"picture_url", @"recipientName": @"recipient_name", @"recipientEmail": @"recipient_email" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"keepSecret", @"donorName", @"message", @"pictureUrl", @"recipientName", @"recipientEmail"];
  return [optionalProperties containsObject:propertyName];
}

@end
