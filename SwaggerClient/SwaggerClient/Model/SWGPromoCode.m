#import "SWGPromoCode.h"

@implementation SWGPromoCode

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"code": @"code", @"active": @"active", @"percentage": @"percentage", @"discount": @"discount", @"maxUsage": @"max_usage", @"validFrom": @"valid_from", @"validUntil": @"valid_until", @"minimumAmount": @"minimum_amount" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"code", @"active", @"percentage", @"discount", @"maxUsage", @"validFrom", @"validUntil", @"minimumAmount"];
  return [optionalProperties containsObject:propertyName];
}

@end
