#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGCategory.h"
#import "SWGPrice.h"
#import "SWGVertical.h"
@protocol SWGCategory;
@class SWGCategory;
@protocol SWGPrice;
@class SWGPrice;
@protocol SWGVertical;
@class SWGVertical;



@protocol SWGGiftboxTypeItem
@end

@interface SWGGiftboxTypeItem : SWGObject


@property(nonatomic) NSNumber* _id;

@property(nonatomic) SWGPrice* giftboxTypePrice;

@property(nonatomic) NSNumber* numberOfPeople;

@property(nonatomic) NSString* title;

@property(nonatomic) NSString* _description;

@property(nonatomic) NSString* coverImageUrl;

@property(nonatomic) NSNumber* reviewsNumber;

@property(nonatomic) NSNumber* reviewsAvg;

@property(nonatomic) NSNumber* latitude;

@property(nonatomic) NSNumber* longitude;

@property(nonatomic) NSString* url;

@property(nonatomic) NSArray<SWGVertical>* verticals;

@property(nonatomic) NSArray<SWGCategory>* categories;

@end
