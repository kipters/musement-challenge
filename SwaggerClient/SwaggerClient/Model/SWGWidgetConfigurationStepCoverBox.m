#import "SWGWidgetConfigurationStepCoverBox.h"

@implementation SWGWidgetConfigurationStepCoverBox

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"title": @"title", @"reviews": @"reviews", @"price": @"price", @"discount": @"discount", @"mobileVoucher": @"mobile_voucher", @"customCtaText": @"custom_cta_text", @"customTitle": @"custom_title" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"title", @"reviews", @"price", @"discount", @"mobileVoucher", @"customCtaText", @"customTitle"];
  return [optionalProperties containsObject:propertyName];
}

@end
