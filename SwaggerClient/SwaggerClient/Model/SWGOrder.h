#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGCustomer.h"
#import "SWGOrderItem.h"
#import "SWGPrice.h"
#import "SWGPromoCode.h"
#import "SWGTicket.h"
@protocol SWGCustomer;
@class SWGCustomer;
@protocol SWGOrderItem;
@class SWGOrderItem;
@protocol SWGPrice;
@class SWGPrice;
@protocol SWGPromoCode;
@class SWGPromoCode;
@protocol SWGTicket;
@class SWGTicket;



@protocol SWGOrder
@end

@interface SWGOrder : SWGObject

/* Order unique internal identifier [optional]
 */
@property(nonatomic) NSString* identifier;
/* Order unique identifier [optional]
 */
@property(nonatomic) NSString* uuid;

@property(nonatomic) NSDate* date;

@property(nonatomic) NSString* status;

@property(nonatomic) NSArray<SWGTicket>* tickets;

@property(nonatomic) NSString* trustpilotUrl;

@property(nonatomic) SWGCustomer* customer;

@property(nonatomic) NSArray<SWGOrderItem>* items;

@property(nonatomic) SWGPrice* totalPrice;

@property(nonatomic) SWGPrice* supplierTotalPrice;

@property(nonatomic) NSNumber* affiliateId;

@property(nonatomic) NSArray<SWGPromoCode>* promoCodes;

@property(nonatomic) NSString* extraData;

@end
