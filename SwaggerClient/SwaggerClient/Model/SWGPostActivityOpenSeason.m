#import "SWGPostActivityOpenSeason.h"

@implementation SWGPostActivityOpenSeason

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"title": @"title", @"featureCode": @"feature_code", @"validity": @"validity", @"validUntil": @"valid_until", @"prices": @"prices", @"languages": @"languages" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"title", @"featureCode", @"validity", @"validUntil", @"prices", @"languages"];
  return [optionalProperties containsObject:propertyName];
}

@end
