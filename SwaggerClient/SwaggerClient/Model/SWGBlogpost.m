#import "SWGBlogpost.h"

@implementation SWGBlogpost

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"title": @"title", @"abstract": @"abstract", @"publishDate": @"publish_date", @"url": @"url", @"backgroundImage": @"background_image", @"backgroundImageCredits": @"background_image_credits", @"category": @"category", @"vertical": @"vertical", @"categories": @"categories", @"list": @"list", @"city": @"city", @"venue": @"venue", @"author": @"author", @"shareImage": @"share_image" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"title", @"abstract", @"publishDate", @"url", @"backgroundImage", @"backgroundImageCredits", @"category", @"vertical", @"categories", @"list", @"city", @"venue", @"author", @"shareImage"];
  return [optionalProperties containsObject:propertyName];
}

@end
