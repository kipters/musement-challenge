#import "SWGPostActivity.h"

@implementation SWGPostActivity

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"type": @"type", @"sellerGateway": @"seller_gateway", @"vertical": @"vertical", @"categories": @"categories", @"highlights": @"highlights", @"duration": @"duration", @"inclusions": @"inclusions", @"exclusions": @"exclusions", @"features": @"features", @"services": @"services", @"mobileTicketIsAccepted": @"mobile_ticket_is_accepted", @"cutoffTime": @"cutoff_time", @"maxConfirmationTime": @"max_confirmation_time", @"partnerInternalCode": @"partner_internal_code", @"partner": @"partner", @"reduction": @"reduction", @"city": @"city", @"venues": @"venues", @"status": @"status" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"type", @"sellerGateway", @"vertical", @"categories", @"highlights", @"duration", @"inclusions", @"exclusions", @"features", @"services", @"mobileTicketIsAccepted", @"cutoffTime", @"maxConfirmationTime", @"partnerInternalCode", @"partner", @"reduction", @"city", @"venues", @"status"];
  return [optionalProperties containsObject:propertyName];
}

@end
