#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGPostGiftboxCustomizationInfo
@end

@interface SWGPostGiftboxCustomizationInfo : SWGObject

/* Keep the giftbox secret [optional]
 */
@property(nonatomic) NSNumber* keepSecret;
/* Donor name [optional]
 */
@property(nonatomic) NSString* donorName;
/* Personal message for the receiver [optional]
 */
@property(nonatomic) NSString* message;
/* Cover image url [optional]
 */
@property(nonatomic) NSString* pictureUrl;
/* Recipient name [optional]
 */
@property(nonatomic) NSString* recipientName;
/* Recipient email address [optional]
 */
@property(nonatomic) NSString* recipientEmail;

@end
