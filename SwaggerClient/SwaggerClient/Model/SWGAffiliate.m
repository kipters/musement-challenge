#import "SWGAffiliate.h"

@implementation SWGAffiliate

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"code": @"code", @"name": @"name", @"logoUrl": @"logo_url", @"secondaryLogoUrl": @"secondary_logo_url", @"whitelabel": @"whitelabel", @"showCobrandedHeader": @"show_cobranded_header", @"showCobrandedVoucher": @"show_cobranded_voucher", @"showCobrandedItemConfirmationEmail": @"show_cobranded_item_confirmation_email", @"setupCookieAfterFirstVisit": @"setup_cookie_after_first_visit" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"secondaryLogoUrl", @"whitelabel", @"showCobrandedHeader", @"showCobrandedVoucher", @"showCobrandedItemConfirmationEmail", @"setupCookieAfterFirstVisit"];
  return [optionalProperties containsObject:propertyName];
}

@end
