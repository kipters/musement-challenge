#import "SWGProduct.h"

@implementation SWGProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"type": @"type", @"image": @"image", @"finalPrice": @"final_price", @"discountAmount": @"discount_amount", @"serviceFee": @"service_fee", @"_id": @"id", @"title": @"title", @"datetime": @"datetime", @"ticketHolder": @"ticket_holder", @"priceFeature": @"price_feature", @"activity": @"activity", @"supplierPrice": @"supplier_price", @"touristCategory": @"tourist_category", @"activityName": @"activity_name" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"type", @"image", @"finalPrice", @"discountAmount", @"serviceFee", @"_id", @"title", @"datetime", @"ticketHolder", @"priceFeature", @"activity", @"supplierPrice", @"touristCategory", @"activityName"];
  return [optionalProperties containsObject:propertyName];
}

@end
