#import "SWGVenue.h"

@implementation SWGVenue

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"city": @"city", @"tips": @"tips", @"status": @"status", @"name": @"name", @"metaTitle": @"meta_title", @"metaDescription": @"meta_description", @"headline": @"headline", @"latitude": @"latitude", @"longitude": @"longitude", @"reviewsNumber": @"reviews_number", @"reviewsAvg": @"reviews_avg", @"relevance": @"relevance", @"abstract": @"abstract", @"_description": @"description", @"descriptionHtml": @"description_html", @"address": @"address", @"events": @"events", @"verticals": @"verticals", @"coverImageUrl": @"cover_image_url", @"eventsCount": @"events_count", @"url": @"url", @"country": @"country", @"showCalendar": @"show_calendar", @"showFlavoursFilter": @"show_flavours_filter", @"showFiltersWhenEventsCount": @"show_filters_when_events_count" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"city", @"tips", @"status", @"name", @"metaTitle", @"metaDescription", @"headline", @"latitude", @"longitude", @"reviewsNumber", @"reviewsAvg", @"relevance", @"abstract", @"_description", @"descriptionHtml", @"address", @"events", @"verticals", @"coverImageUrl", @"eventsCount", @"url", @"country", @"showCalendar", @"showFlavoursFilter", @"showFiltersWhenEventsCount"];
  return [optionalProperties containsObject:propertyName];
}

@end
