#import "SWGGiftScheduleFeature.h"

@implementation SWGGiftScheduleFeature

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"featureCode": @"feature_code", @"name": @"name", @"_description": @"description", @"_default": @"default", @"products": @"products" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"featureCode", @"name", @"_description", @"_default", @"products"];
  return [optionalProperties containsObject:propertyName];
}

@end
