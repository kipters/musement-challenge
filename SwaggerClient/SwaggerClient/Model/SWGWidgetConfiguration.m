#import "SWGWidgetConfiguration.h"

@implementation SWGWidgetConfiguration

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"widgetType": @"widget_type", @"modal": @"modal", @"entityIds": @"entity_ids", @"entityType": @"entity_type", @"customCss": @"custom_css", @"currency": @"currency", @"locale": @"locale", @"steps": @"steps" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"widgetType", @"modal", @"entityIds", @"entityType", @"customCss", @"currency", @"locale", @"steps"];
  return [optionalProperties containsObject:propertyName];
}

@end
