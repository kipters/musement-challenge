#import "SWGCart.h"

@implementation SWGCart

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"uuid": @"uuid", @"items": @"items", @"tickets": @"tickets", @"discountCode": @"discount_code", @"giftCard": @"gift_card", @"promoCode": @"promo_code", @"customer": @"customer", @"customerId": @"customer_id", @"fullPrice": @"full_price", @"discount": @"discount", @"retailPrice": @"retail_price", @"serviceFee": @"service_fee" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"uuid", @"items", @"tickets", @"discountCode", @"giftCard", @"promoCode", @"customer", @"customerId", @"fullPrice", @"discount", @"retailPrice", @"serviceFee"];
  return [optionalProperties containsObject:propertyName];
}

@end
