#import "SWGGiftScheduleProduct.h"

@implementation SWGGiftScheduleProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"holderCode": @"holder_code", @"name": @"name", @"type": @"type", @"productId": @"product_id", @"minBuy": @"min_buy", @"maxBuy": @"max_buy", @"availability": @"availability", @"rawPrice": @"raw_price", @"discountAmount": @"discount_amount", @"retailPrice": @"retail_price", @"serviceFee": @"service_fee" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"holderCode", @"name", @"type", @"productId", @"minBuy", @"maxBuy", @"availability", @"rawPrice", @"discountAmount", @"retailPrice", @"serviceFee"];
  return [optionalProperties containsObject:propertyName];
}

@end
