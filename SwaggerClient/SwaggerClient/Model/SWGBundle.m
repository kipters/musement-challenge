#import "SWGBundle.h"

@implementation SWGBundle

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"discount": @"discount", @"discountType": @"discount_type", @"bundleType": @"bundle_type", @"ruleId": @"rule_id", @"activities": @"activities" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"discount", @"discountType", @"bundleType", @"ruleId", @"activities"];
  return [optionalProperties containsObject:propertyName];
}

@end
