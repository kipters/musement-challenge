#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGPageLink.h"
@protocol SWGPageLink;
@class SWGPageLink;



@protocol SWGPage
@end

@interface SWGPage : SWGObject


@property(nonatomic) NSNumber* _id;

@property(nonatomic) NSString* pageCategory;

@property(nonatomic) NSString* name;

@property(nonatomic) NSString* type;

@property(nonatomic) NSNumber* parentId;

@property(nonatomic) NSArray<SWGPageLink>* children;

@property(nonatomic) NSString* path;

@property(nonatomic) NSString* title;

@property(nonatomic) NSString* content;

@property(nonatomic) NSString* coverImageUrl;

@property(nonatomic) NSString* location;

@property(nonatomic) NSNumber* active;

@property(nonatomic) NSDate* publishedAt;

@property(nonatomic) NSNumber* weight;

@end
