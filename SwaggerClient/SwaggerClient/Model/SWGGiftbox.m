#import "SWGGiftbox.h"

@implementation SWGGiftbox

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"redeemCode": @"redeem_code", @"finiteState": @"finite_state", @"apiUrl": @"api_url", @"url": @"url", @"coverImage": @"cover_image", @"finalPrice": @"final_price", @"discountAmount": @"discount_amount", @"serviceFee": @"service_fee", @"giftboxType": @"giftbox_type", @"customizationInfo": @"customization_info", @"type": @"type", @"title": @"title", @"_id": @"id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"redeemCode", @"finiteState", @"apiUrl", @"url", @"coverImage", @"finalPrice", @"discountAmount", @"serviceFee", @"giftboxType", @"customizationInfo", @"type", @"title", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
