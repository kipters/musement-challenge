#import "SWGTimeslotProduct.h"

@implementation SWGTimeslotProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"event": @"event", @"priceTag": @"price_tag", @"maxBuy": @"max_buy", @"minBuy": @"min_buy", @"retailPrice": @"retail_price", @"supplierPrice": @"supplier_price", @"discountAmount": @"discount_amount", @"serviceFee": @"service_fee", @"datetime": @"datetime" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"event", @"priceTag", @"maxBuy", @"minBuy", @"retailPrice", @"supplierPrice", @"discountAmount", @"serviceFee", @"datetime"];
  return [optionalProperties containsObject:propertyName];
}

@end
