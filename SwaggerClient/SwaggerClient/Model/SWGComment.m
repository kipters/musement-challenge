#import "SWGComment.h"

@implementation SWGComment

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"status": @"status", @"uuid": @"uuid", @"author": @"author", @"locale": @"locale", @"pictures": @"pictures", @"title": @"title", @"body": @"body", @"rating": @"rating", @"sentAt": @"sent_at", @"event": @"event" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"status", @"uuid", @"author", @"locale", @"pictures", @"title", @"body", @"rating", @"sentAt", @"event"];
  return [optionalProperties containsObject:propertyName];
}

@end
