#import "SWGPage.h"

@implementation SWGPage

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"pageCategory": @"page_category", @"name": @"name", @"type": @"type", @"parentId": @"parent_id", @"children": @"children", @"path": @"path", @"title": @"title", @"content": @"content", @"coverImageUrl": @"cover_image_url", @"location": @"location", @"active": @"active", @"publishedAt": @"published_at", @"weight": @"weight" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"pageCategory", @"name", @"type", @"parentId", @"children", @"path", @"title", @"content", @"coverImageUrl", @"location", @"active", @"publishedAt", @"weight"];
  return [optionalProperties containsObject:propertyName];
}

@end
