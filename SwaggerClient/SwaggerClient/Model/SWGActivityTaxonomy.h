#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGActivityTaxonomy
@end

@interface SWGActivityTaxonomy : SWGObject


@property(nonatomic) NSString* uuid;

@property(nonatomic) NSString* name;

@property(nonatomic) NSString* category;

@end
