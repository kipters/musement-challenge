#import "SWGPricetag.h"

@implementation SWGPricetag

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"code": @"code", @"priceFeature": @"price_feature", @"ticketHolder": @"ticket_holder", @"priceFeatureCode": @"price_feature_code", @"ticketHolderCode": @"ticket_holder_code" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"code", @"priceFeature", @"ticketHolder", @"priceFeatureCode", @"ticketHolderCode"];
  return [optionalProperties containsObject:propertyName];
}

@end
