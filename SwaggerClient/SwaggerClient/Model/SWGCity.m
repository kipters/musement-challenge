#import "SWGCity.h"

@implementation SWGCity

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"top": @"top", @"hasConcierge": @"has_concierge", @"name": @"name", @"code": @"code", @"content": @"content", @"more": @"more", @"weight": @"weight", @"latitude": @"latitude", @"longitude": @"longitude", @"country": @"country", @"coverImageUrl": @"cover_image_url", @"url": @"url", @"eventsCount": @"events_count" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"top", @"hasConcierge", @"name", @"code", @"content", @"more", @"weight", @"latitude", @"longitude", @"country", @"coverImageUrl", @"url", @"eventsCount"];
  return [optionalProperties containsObject:propertyName];
}

@end
