#import "SWGTicket.h"

@implementation SWGTicket

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"uuid": @"uuid", @"cartId": @"cart_id", @"product": @"product", @"code": @"code", @"quantity": @"quantity", @"totalPrice": @"total_price", @"supplierTotalPrice": @"supplier_total_price", @"passengersInfo": @"passengers_info", @"metadata": @"metadata", @"status": @"status", @"vouchers": @"vouchers" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"uuid", @"cartId", @"product", @"code", @"quantity", @"totalPrice", @"supplierTotalPrice", @"passengersInfo", @"metadata", @"status", @"vouchers"];
  return [optionalProperties containsObject:propertyName];
}

@end
