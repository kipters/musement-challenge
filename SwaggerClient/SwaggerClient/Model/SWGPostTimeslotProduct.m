#import "SWGPostTimeslotProduct.h"

@implementation SWGPostTimeslotProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"supplierPrice": @"supplier_price", @"retailPriceSupplier": @"retail_price_supplier", @"currency": @"currency", @"holderCode": @"holder_code", @"featureCode": @"feature_code" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"supplierPrice", @"retailPriceSupplier", @"currency", @"holderCode", @"featureCode"];
  return [optionalProperties containsObject:propertyName];
}

@end
