#import "SWGPatchCustomer.h"

@implementation SWGPatchCustomer

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"firstname": @"firstname", @"lastname": @"lastname", @"mobile": @"mobile", @"city": @"city", @"address": @"address", @"birthdate": @"birthdate", @"gender": @"gender" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"firstname", @"lastname", @"mobile", @"city", @"address", @"birthdate", @"gender"];
  return [optionalProperties containsObject:propertyName];
}

@end
