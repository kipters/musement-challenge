#import "SWGGiftboxTypeItem.h"

@implementation SWGGiftboxTypeItem

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"giftboxTypePrice": @"giftbox_type_price", @"numberOfPeople": @"number_of_people", @"title": @"title", @"_description": @"description", @"coverImageUrl": @"cover_image_url", @"reviewsNumber": @"reviews_number", @"reviewsAvg": @"reviews_avg", @"latitude": @"latitude", @"longitude": @"longitude", @"url": @"url", @"verticals": @"verticals", @"categories": @"categories" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"giftboxTypePrice", @"numberOfPeople", @"title", @"_description", @"coverImageUrl", @"reviewsNumber", @"reviewsAvg", @"latitude", @"longitude", @"url", @"verticals", @"categories"];
  return [optionalProperties containsObject:propertyName];
}

@end
