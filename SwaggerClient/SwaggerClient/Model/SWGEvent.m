#import "SWGEvent.h"

@implementation SWGEvent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"maxConfirmationTime": @"max_confirmation_time", @"cutoffTime": @"cutoff_time", @"sellerGateway": @"seller_gateway", @"partnerInternalCode": @"partner_internal_code", @"uuid": @"uuid", @"city": @"city", @"saves": @"saves", @"supplierCode": @"supplier_code", @"title": @"title", @"relevance": @"relevance", @"emergencyPhoneNumber": @"emergency_phone_number", @"relevanceVenue": @"relevance_venue", @"mustSee": @"must_see", @"lastChance": @"last_chance", @"topSeller": @"top_seller", @"printVoucher": @"print_voucher", @"temporary": @"temporary", @"_description": @"description", @"about": @"about", @"aboutMarkdown": @"about_markdown", @"aboutHtml": @"about_html", @"meetingPoint": @"meeting_point", @"duration": @"duration", @"validity": @"validity", @"numberedSeats": @"numbered_seats", @"hasPriceInfoOnDate": @"has_price_info_on_date", @"open": @"open", @"ticketNotIncluded": @"ticket_not_included", @"likelyToSellOut": @"likely_to_sell_out", @"specialOffer": @"special_offer", @"exclusive": @"exclusive", @"includedHotelNightNumber": @"included_hotel_night_number", @"greenFeeDays": @"green_fee_days", @"daily": @"daily", @"languages": @"languages", @"groupSize": @"group_size", @"food": @"food", @"services": @"services", @"translated": @"translated", @"automaticallyTranslated": @"automatically_translated", @"features": @"features", @"highlights": @"highlights", @"included": @"included", @"notIncluded": @"not_included", @"isAvailableToday": @"is_available_today", @"isAvailableTomorrow": @"is_available_tomorrow", @"hasMultipleOptions": @"has_multiple_options", @"coverImageUrl": @"cover_image_url", @"extraMediaUrl": @"extra_media_url", @"serviceFee": @"service_fee", @"retailPrice": @"retail_price", @"netPrice": @"net_price", @"bundledPrice": @"bundled_price", @"discount": @"discount", @"bucket": @"bucket", @"categories": @"categories", @"reviewsNumber": @"reviews_number", @"reviewsAvg": @"reviews_avg", @"reviewsAggregatedInfo": @"reviews_aggregated_info", @"latitude": @"latitude", @"longitude": @"longitude", @"url": @"url", @"agencyUrls": @"agency_urls", @"agencyPrice": @"agency_price", @"tags": @"tags", @"flavours": @"flavours", @"verticals": @"verticals", @"orderBoxElements": @"order_box_elements", @"giftable": @"giftable", @"hasPassengerInfo": @"has_passenger_info", @"hasExtraCustomerData": @"has_extra_customer_data", @"buyMultiplier": @"buy_multiplier", @"status": @"status", @"totalBookings": @"total_bookings", @"createdAt": @"created_at", @"publishedAt": @"published_at", @"merchantPrice": @"merchant_price", @"operationalDays": @"operational_days" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"maxConfirmationTime", @"cutoffTime", @"sellerGateway", @"partnerInternalCode", @"uuid", @"city", @"saves", @"supplierCode", @"title", @"relevance", @"emergencyPhoneNumber", @"relevanceVenue", @"mustSee", @"lastChance", @"topSeller", @"printVoucher", @"temporary", @"_description", @"about", @"aboutMarkdown", @"aboutHtml", @"meetingPoint", @"duration", @"validity", @"numberedSeats", @"hasPriceInfoOnDate", @"open", @"ticketNotIncluded", @"likelyToSellOut", @"specialOffer", @"exclusive", @"includedHotelNightNumber", @"greenFeeDays", @"daily", @"languages", @"groupSize", @"food", @"services", @"translated", @"automaticallyTranslated", @"features", @"highlights", @"included", @"notIncluded", @"isAvailableToday", @"isAvailableTomorrow", @"hasMultipleOptions", @"coverImageUrl", @"extraMediaUrl", @"serviceFee", @"retailPrice", @"netPrice", @"bundledPrice", @"discount", @"bucket", @"categories", @"reviewsNumber", @"reviewsAvg", @"reviewsAggregatedInfo", @"latitude", @"longitude", @"url", @"agencyUrls", @"agencyPrice", @"tags", @"flavours", @"verticals", @"orderBoxElements", @"giftable", @"hasPassengerInfo", @"hasExtraCustomerData", @"buyMultiplier", @"status", @"totalBookings", @"createdAt", @"publishedAt", @"merchantPrice", @"operationalDays"];
  return [optionalProperties containsObject:propertyName];
}

@end
