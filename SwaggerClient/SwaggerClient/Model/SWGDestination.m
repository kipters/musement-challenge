#import "SWGDestination.h"

@implementation SWGDestination

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"verticals": @"verticals", @"city": @"city", @"saves": @"saves", @"name": @"name", @"canonicalUrl": @"canonical_url", @"phone": @"phone", @"website": @"website", @"priceRange": @"price_range", @"latitude": @"latitude", @"longitude": @"longitude", @"address": @"address", @"photos": @"photos", @"comments": @"comments", @"openingHours": @"opening_hours", @"specialities": @"specialities", @"rating": @"rating" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"verticals", @"city", @"saves", @"name", @"canonicalUrl", @"phone", @"website", @"priceRange", @"latitude", @"longitude", @"address", @"photos", @"comments", @"openingHours", @"specialities", @"rating"];
  return [optionalProperties containsObject:propertyName];
}

@end
