#import "SWGReview.h"

@implementation SWGReview

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"locale": @"locale", @"rating": @"rating", @"picture": @"picture", @"title": @"title", @"body": @"body", @"sentAt": @"sent_at", @"approvedAt": @"approved_at" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"locale", @"rating", @"picture", @"title", @"body", @"sentAt", @"approvedAt"];
  return [optionalProperties containsObject:propertyName];
}

@end
