#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGTranslatedMetadata.h"
@protocol SWGTranslatedMetadata;
@class SWGTranslatedMetadata;



@protocol SWGActivityPoi
@end

@interface SWGActivityPoi : SWGObject


@property(nonatomic) NSNumber* latitude;

@property(nonatomic) NSNumber* longitude;

@property(nonatomic) NSArray<SWGTranslatedMetadata>* types;

@end
