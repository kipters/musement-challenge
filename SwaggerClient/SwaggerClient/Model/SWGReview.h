#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGReview
@end

@interface SWGReview : SWGObject


@property(nonatomic) NSString* locale;

@property(nonatomic) NSNumber* rating;

@property(nonatomic) NSString* picture;

@property(nonatomic) NSString* title;

@property(nonatomic) NSString* body;

@property(nonatomic) NSDate* sentAt;

@property(nonatomic) NSDate* approvedAt;

@end
