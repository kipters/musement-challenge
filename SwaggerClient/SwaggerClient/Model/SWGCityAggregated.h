#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGCountry.h"
@protocol SWGCountry;
@class SWGCountry;



@protocol SWGCityAggregated
@end

@interface SWGCityAggregated : SWGObject


@property(nonatomic) NSNumber* eventCount;

@property(nonatomic) NSNumber* listCount;

@property(nonatomic) NSNumber* venueCount;

@property(nonatomic) NSNumber* showInPopular;

@property(nonatomic) NSNumber* _id;
/* Indicate if the event is a Musement 'top' event [optional]
 */
@property(nonatomic) NSNumber* top;
/* True for cities with Musement's concierge service active [optional]
 */
@property(nonatomic) NSNumber* hasConcierge;
/* City name [optional]
 */
@property(nonatomic) NSString* name;
/* City code. Do not depends on locale [optional]
 */
@property(nonatomic) NSString* code;
/* Description [optional]
 */
@property(nonatomic) NSString* content;

@property(nonatomic) NSString* more;

@property(nonatomic) NSNumber* weight;

@property(nonatomic) NSNumber* latitude;

@property(nonatomic) NSNumber* longitude;

@property(nonatomic) SWGCountry* country;

@property(nonatomic) NSString* coverImageUrl;

@property(nonatomic) NSString* url;
/* Number of active events [optional]
 */
@property(nonatomic) NSNumber* eventsCount;

@end
