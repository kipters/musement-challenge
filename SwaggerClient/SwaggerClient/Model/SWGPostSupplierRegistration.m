#import "SWGPostSupplierRegistration.h"

@implementation SWGPostSupplierRegistration

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"firstname": @"firstname", @"lastname": @"lastname", @"companyLegalName": @"company_legal_name", @"companyName": @"company_name", @"contactName": @"contact_name", @"contactPhoneNumber": @"contact_phone_number", @"website": @"website", @"supportEmail": @"support_email", @"countryCode": @"country_code", @"address": @"address", @"zipCode": @"zip_code", @"city": @"city", @"taxId": @"tax_id", @"defaultCurrency": @"default_currency", @"logoUrl": @"logo_url", @"password": @"password", @"email": @"email" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"firstname", @"lastname", @"companyLegalName", @"companyName", @"contactName", @"contactPhoneNumber", @"website", @"supportEmail", @"countryCode", @"address", @"zipCode", @"city", @"taxId", @"defaultCurrency", @"logoUrl", @"password", @"email"];
  return [optionalProperties containsObject:propertyName];
}

@end
