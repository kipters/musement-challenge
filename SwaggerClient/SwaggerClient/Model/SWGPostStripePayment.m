#import "SWGPostStripePayment.h"

@implementation SWGPostStripePayment

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"orderUuid": @"order_uuid", @"stripeToken": @"stripe_token", @"stripeSourceId": @"stripe_source_id", @"isApplePay": @"is_apple_pay", @"clientId": @"client_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"orderUuid", @"stripeToken", @"stripeSourceId", @"isApplePay", @"clientId"];
  return [optionalProperties containsObject:propertyName];
}

@end
