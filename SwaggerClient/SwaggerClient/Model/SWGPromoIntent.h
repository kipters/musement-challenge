#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGPromoIntent
@end

@interface SWGPromoIntent : SWGObject


@property(nonatomic) NSString* uuid;

@property(nonatomic) NSNumber* cart;

@property(nonatomic) NSNumber* promo;

@property(nonatomic) NSString* triggeringProduct;

@property(nonatomic) NSString* targetProduct;

@end
