#import "SWGPostActivitySeasonSetup.h"

@implementation SWGPostActivitySeasonSetup

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"type": @"type", @"title": @"title", @"days": @"days", @"featureCode": @"feature_code", @"prices": @"prices", @"timeslots": @"timeslots", @"languages": @"languages", @"minPurchasableQty": @"min_purchasable_qty", @"maxPurchasableQty": @"max_purchasable_qty" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"type", @"title", @"days", @"featureCode", @"prices", @"timeslots", @"languages", @"minPurchasableQty", @"maxPurchasableQty"];
  return [optionalProperties containsObject:propertyName];
}

@end
