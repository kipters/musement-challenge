#import "SWGCustomer.h"

@implementation SWGCustomer

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"email": @"email", @"firstname": @"firstname", @"lastname": @"lastname", @"avatar": @"avatar", @"country": @"country", @"currency": @"currency", @"birthdate": @"birthdate", @"gender": @"gender", @"idNumber": @"id_number", @"mobile": @"mobile", @"address": @"address", @"favouriteCity": @"favourite_city" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"email", @"firstname", @"lastname", @"avatar", @"country", @"currency", @"birthdate", @"gender", @"idNumber", @"mobile", @"address", @"favouriteCity"];
  return [optionalProperties containsObject:propertyName];
}

@end
