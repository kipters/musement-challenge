#import "SWGMusementList.h"

@implementation SWGMusementList

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"title": @"title", @"subtitle": @"subtitle", @"_description": @"description", @"seoTitle": @"seo_title", @"seoDescription": @"seo_description", @"temporary": @"temporary", @"url": @"url", @"authors": @"authors", @"views": @"views", @"scroll": @"scroll", @"saves": @"saves", @"listType": @"list_type", @"items": @"items", @"itemsCount": @"items_count", @"city": @"city", @"coverImageUrl": @"cover_image_url", @"verticals": @"verticals" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"title", @"subtitle", @"_description", @"seoTitle", @"seoDescription", @"temporary", @"url", @"authors", @"views", @"scroll", @"saves", @"listType", @"items", @"itemsCount", @"city", @"coverImageUrl", @"verticals"];
  return [optionalProperties containsObject:propertyName];
}

@end
