#import "SWGGiftboxType.h"

@implementation SWGGiftboxType

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"code": @"code", @"url": @"url", @"relevance": @"relevance", @"level": @"level", @"price": @"price", @"vertical": @"vertical", @"title": @"title", @"_description": @"description", @"extendedDescription": @"extended_description", @"metaDescription": @"meta_description", @"seoTitle": @"seo_title", @"coverImageUrl": @"cover_image_url" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"code", @"url", @"relevance", @"level", @"price", @"vertical", @"title", @"_description", @"extendedDescription", @"metaDescription", @"seoTitle", @"coverImageUrl"];
  return [optionalProperties containsObject:propertyName];
}

@end
