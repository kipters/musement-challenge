#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/





@protocol SWGPostActivityTimeslotProducts
@end

@interface SWGPostActivityTimeslotProducts : SWGObject

/* The collection of products to update [optional]
 */
@property(nonatomic) NSArray<SWGObject *>* products;
/* Price the supplier wants for customers. If no discount is equal to retail price. [optional]
 */
@property(nonatomic) NSNumber* retailPriceSupplier;
/* Currency | Use 'code' field from GET /api/v3/currencies [optional]
 */
@property(nonatomic) NSString* currency;
/* Holder code [optional]
 */
@property(nonatomic) NSString* holderCode;
/* Feature code [optional]
 */
@property(nonatomic) NSString* featureCode;

@end
