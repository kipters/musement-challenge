#import "SWGOrderItem.h"

@implementation SWGOrderItem

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"uuid": @"uuid", @"code": @"code", @"quantity": @"quantity", @"totalPrice": @"total_price", @"supplierTotalPrice": @"supplier_total_price", @"status": @"status", @"vouchers": @"vouchers" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"uuid", @"code", @"quantity", @"totalPrice", @"supplierTotalPrice", @"status", @"vouchers"];
  return [optionalProperties containsObject:propertyName];
}

@end
