#import <Foundation/Foundation.h>
#import "SWGInvoice.h"
#import "SWGApi.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGSupplierInvoicesApi: NSObject <SWGApi>

extern NSString* kSWGSupplierInvoicesApiErrorDomain;
extern NSInteger kSWGSupplierInvoicesApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Get supplier invoices
/// 
///
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// @param page Page from which starting to return found events (mandatory if limit is given) (optional)
/// @param limit Max number of items in the response (optional) (default to 10)
/// 
///  code:200 message:"Returned when successful",
///  code:404 message:"Returned when supplier not exists",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return NSArray<SWGInvoice>*
-(NSURLSessionTask*) suppliersMeInvoicesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    page: (NSNumber*) page
    limit: (NSNumber*) limit
    completionHandler: (void (^)(NSArray<SWGInvoice>* output, NSError* error)) handler;


/// Get invoices for a specific supplier
/// 
///
/// @param supplierUuid Supplier UUID
/// @param page Page from which starting to return found events (mandatory if limit is given) (optional)
/// @param limit Max number of items in the response (optional) (default to 10)
/// 
///  code:200 message:"Returned when successful",
///  code:404 message:"Returned when supplier not exists",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return NSArray<SWGInvoice>*
-(NSURLSessionTask*) suppliersSupplierUuidInvoicesGetWithSupplierUuid: (NSString*) supplierUuid
    page: (NSNumber*) page
    limit: (NSNumber*) limit
    completionHandler: (void (^)(NSArray<SWGInvoice>* output, NSError* error)) handler;



@end
