#import <Foundation/Foundation.h>
#import "SWGActivityCalendarUpdateRequest.h"
#import "SWGBlackoutDay.h"
#import "SWGGiftSchedule.h"
#import "SWGPostActivityBlackoutDay.h"
#import "SWGPostActivityOpenSeason.h"
#import "SWGPostTimedSeason.h"
#import "SWGSchedule.h"
#import "SWGApi.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGDateApi: NSObject <SWGApi>

extern NSString* kSWGDateApiErrorDomain;
extern NSInteger kSWGDateApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Add a blackout day
/// 
///
/// @param activityUuid Activity identifier
/// @param blackoutDay Day to add to the blackout one
/// @param xMusementVersion  (optional)
/// 
///  code:200 message:"Returned when successful",
///  code:400 message:"Returned if sent data contains errors",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return SWGBlackoutDay*
-(NSURLSessionTask*) activitiesActivityUuidBlackoutDaysPostWithActivityUuid: (NSString*) activityUuid
    blackoutDay: (SWGPostActivityBlackoutDay*) blackoutDay
    xMusementVersion: (NSString*) xMusementVersion
    completionHandler: (void (^)(SWGBlackoutDay* output, NSError* error)) handler;


/// Get schedule for a day for an event
/// 
///
/// @param activityUuid Activity identifier
/// @param day Day
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// @param xMusementCurrency  (optional) (default to USD)
/// @param ticketsNumber Number of ticket requested tickets | Only useful for real time activities (optional)
/// 
///  code:200 message:"Returned when successful",
///  code:403 message:"Returned when dates for not active activity are requested.",
///  code:404 message:"Returned when resource is not found",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return NSArray<SWGSchedule>*
-(NSURLSessionTask*) activitiesActivityUuidDatesDayGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    ticketsNumber: (NSString*) ticketsNumber
    completionHandler: (void (^)(NSArray<SWGSchedule>* output, NSError* error)) handler;


/// Get available dates for an event
/// 
///
/// @param activityUuid Activity identifier
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// @param xMusementCurrency  (optional) (default to USD)
/// @param dateFrom Start date | If not specified set to today | Use format: YYYY-MM-DD (optional)
/// @param dateTo To date | If not specified set to next year | Use format: YYYY-MM-DD (optional)
/// 
///  code:200 message:"Returned when successful",
///  code:404 message:"Returned when resource is not found",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return NSDate*
-(NSURLSessionTask*) activitiesActivityUuidDatesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    completionHandler: (void (^)(NSDate* output, NSError* error)) handler;


/// Add an open season to an activity
/// 
///
/// @param openSeason Open season
/// @param activityUuid Activity identifier
/// 
///  code:200 message:"Returned when successful",
///  code:400 message:"Returned on error",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return SWGActivityCalendarUpdateRequest*
-(NSURLSessionTask*) activitiesActivityUuidOpenSeasonsPostWithOpenSeason: (SWGPostActivityOpenSeason*) openSeason
    activityUuid: (NSString*) activityUuid
    completionHandler: (void (^)(SWGActivityCalendarUpdateRequest* output, NSError* error)) handler;


/// Add a season to an activity
/// 
///
/// @param season Activity season
/// @param activityUuid Activity identifier
/// 
///  code:200 message:"Returned when successful",
///  code:400 message:"Returned on error",
///  code:503 message:"Returned when te service is unavailable"
///
/// @return SWGActivityCalendarUpdateRequest*
-(NSURLSessionTask*) activitiesActivityUuidTimedSeasonsPostWithSeason: (SWGPostTimedSeason*) season
    activityUuid: (NSString*) activityUuid
    completionHandler: (void (^)(SWGActivityCalendarUpdateRequest* output, NSError* error)) handler;


/// Get schedule for a 'Gift creation' for an Event
/// 
///
/// @param eventId Event identifier
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// @param xMusementCurrency  (optional) (default to USD)
/// 
///  code:200 message:"Returned when successful",
///  code:404 message:"Returned when Event is not found or is not giftable",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return SWGGiftSchedule*
-(NSURLSessionTask*) eventsEventIdGiftScheduleGetWithEventId: (NSNumber*) eventId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    completionHandler: (void (^)(SWGGiftSchedule* output, NSError* error)) handler;



@end
