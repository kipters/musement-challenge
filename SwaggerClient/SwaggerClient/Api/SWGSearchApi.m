#import "SWGSearchApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGActivityRelatedResult.h"
#import "SWGActivitySearchResult.h"


@interface SWGSearchApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGSearchApi

NSString* kSWGSearchApiErrorDomain = @"SWGSearchApiErrorDomain";
NSInteger kSWGSearchApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Activities
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param text Text to search (optional)
///
///  @param textOperator Represents text operator. If `AND` results will contains all of the words, if `OR` at least one, if `AUTO` performs a `AND` search first and an `OR` then (optional, default to AUTO)
///
///  @param extendOtherLanguages If set to `YES` will extend the search to other language than the one specified in `Accept-Language` header, if set to `AUTO` will do this automatically on 0 results (optional, default to AUTO)
///
///  @param extendContentFields If set to `YES` will extend the search to other textual field other than the title, if set to `AUTO` will do this automatically on 0 results (optional, default to AUTO)
///
///  @param fuzzinessLevel Level of fuzziness (optional, default to LEVEL-0)
///
///  @param zeroTermsQuery If set to `ALL`, if all of the stop words have been removed, search will be performed, if set to 'NONE' will not (optional, default to NONE)
///
///  @param sortBy Set sorting criteria. Add `-` to invert sorting direction. When `distance` is specified also `coordinates` parameter must be set. (optional)
///
///  @param categoryIn Filter by category | Category code need to be passed (optional)
///
///  @param cityIn Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
///
///  @param coordinates Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
///
///  @param countryIn Filter, include only results from given countries identified by a collection of 2 chars country code (optional)
///
///  @param defaultPriceRange Price range as comma separated values, accepts only floats positive or equals to 0, two points precision | Currency is inferred from X-Musement-Currency header | Example: 0.00,34.23 (optional)
///
///  @param distance Distance from given coordinates expressed with an integer followed by unit | `KM` for kilometers, `M` for miles | Requires the other parameters called `coordinates` to be set in the same request | Example: 334KM (optional)
///
///  @param featureIn Filter, include only results having at least one of the given features identified by a collection of codes (optional)
///
///  @param temporary Filter, include results on an temporary flag basis. Accepted values: YES or NOT (optional)
///
///  @param venueIn Filter, include only results from given verticals identified by a collection of ids (optional)
///
///  @param verticalIn Filter, include only results from given verticals identified by a collection of literal codes (optional)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @returns SWGActivitySearchResult*
///
-(NSURLSessionTask*) activitiesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    text: (NSString*) text
    textOperator: (NSString*) textOperator
    extendOtherLanguages: (NSString*) extendOtherLanguages
    extendContentFields: (NSString*) extendContentFields
    fuzzinessLevel: (NSString*) fuzzinessLevel
    zeroTermsQuery: (NSString*) zeroTermsQuery
    sortBy: (NSArray<NSString*>*) sortBy
    categoryIn: (NSArray<NSString*>*) categoryIn
    cityIn: (NSArray<NSNumber*>*) cityIn
    coordinates: (NSString*) coordinates
    countryIn: (NSArray<NSString*>*) countryIn
    defaultPriceRange: (NSString*) defaultPriceRange
    distance: (NSString*) distance
    featureIn: (NSArray<NSString*>*) featureIn
    temporary: (NSNumber*) temporary
    venueIn: (NSArray<NSNumber*>*) venueIn
    verticalIn: (NSArray<NSString*>*) verticalIn
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
    completionHandler: (void (^)(SWGActivitySearchResult* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (text != nil) {
        queryParams[@"text"] = text;
    }
    if (textOperator != nil) {
        queryParams[@"text_operator"] = textOperator;
    }
    if (extendOtherLanguages != nil) {
        queryParams[@"extend_other_languages"] = extendOtherLanguages;
    }
    if (extendContentFields != nil) {
        queryParams[@"extend_content_fields"] = extendContentFields;
    }
    if (fuzzinessLevel != nil) {
        queryParams[@"fuzziness_level"] = fuzzinessLevel;
    }
    if (zeroTermsQuery != nil) {
        queryParams[@"zero_terms_query"] = zeroTermsQuery;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sortBy format: @"csv"];
    }
    if (categoryIn != nil) {
        queryParams[@"category_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: categoryIn format: @"csv"];
    }
    if (cityIn != nil) {
        queryParams[@"city_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: cityIn format: @"csv"];
    }
    if (coordinates != nil) {
        queryParams[@"coordinates"] = coordinates;
    }
    if (countryIn != nil) {
        queryParams[@"country_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: countryIn format: @"csv"];
    }
    if (defaultPriceRange != nil) {
        queryParams[@"default_price_range"] = defaultPriceRange;
    }
    if (distance != nil) {
        queryParams[@"distance"] = distance;
    }
    if (featureIn != nil) {
        queryParams[@"feature_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: featureIn format: @"csv"];
    }
    if (temporary != nil) {
        queryParams[@"temporary"] = [temporary isEqual:@(YES)] ? @"true" : @"false";
    }
    if (venueIn != nil) {
        queryParams[@"venue_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: venueIn format: @"csv"];
    }
    if (verticalIn != nil) {
        queryParams[@"vertical_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: verticalIn format: @"csv"];
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGActivitySearchResult*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGActivitySearchResult*)data, error);
                                }
                            }];
}

///
/// Activities related search entrypoint
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param text Text to search (optional)
///
///  @param textOperator Represents text operator. If `AND` results will contains all of the words, if `OR` at least one, if `AUTO` performs a `AND` search first and an `OR` then (optional, default to AUTO)
///
///  @param extendOtherLanguages If set to `YES` will extend the search to other language than the one specified in `Accept-Language` header, if set to `AUTO` will do this automatically on 0 results (optional, default to AUTO)
///
///  @param extendContentFields If set to `YES` will extend the search to other textual field other than the title, if set to `AUTO` will do this automatically on 0 results (optional, default to AUTO)
///
///  @param fuzzinessLevel Level of fuzziness (optional, default to LEVEL-0)
///
///  @param zeroTermsQuery If set to `ALL`, if all of the stop words have been removed, search will be performed, if set to 'NONE' will not (optional, default to NONE)
///
///  @param sortBy Ordering criteria to apply, prepend `-` for descending order (optional)
///
///  @param coordinates Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
///
///  @param minimumEvents Minimum number of active events that must belong to the container subject, default 1 (optional)
///
///  @param cityLimit Maximum number of cities to return (optional)
///
///  @param cityOffset Offset for cities to return (optional)
///
///  @param listLimit Maximum number of lists to return (optional)
///
///  @param listOffset Offset for lists to return (optional)
///
///  @param venueLimit Maximum number of venues to return (optional)
///
///  @param venueOffset Offset for venues to return (optional)
///
///  @returns NSArray<SWGActivityRelatedResult>*
///
-(NSURLSessionTask*) activitiesRelatedGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    text: (NSString*) text
    textOperator: (NSString*) textOperator
    extendOtherLanguages: (NSString*) extendOtherLanguages
    extendContentFields: (NSString*) extendContentFields
    fuzzinessLevel: (NSString*) fuzzinessLevel
    zeroTermsQuery: (NSString*) zeroTermsQuery
    sortBy: (NSArray<NSString*>*) sortBy
    coordinates: (NSString*) coordinates
    minimumEvents: (NSNumber*) minimumEvents
    cityLimit: (NSNumber*) cityLimit
    cityOffset: (NSNumber*) cityOffset
    listLimit: (NSNumber*) listLimit
    listOffset: (NSNumber*) listOffset
    venueLimit: (NSNumber*) venueLimit
    venueOffset: (NSNumber*) venueOffset
    completionHandler: (void (^)(NSArray<SWGActivityRelatedResult>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities-related"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (text != nil) {
        queryParams[@"text"] = text;
    }
    if (textOperator != nil) {
        queryParams[@"text_operator"] = textOperator;
    }
    if (extendOtherLanguages != nil) {
        queryParams[@"extend_other_languages"] = extendOtherLanguages;
    }
    if (extendContentFields != nil) {
        queryParams[@"extend_content_fields"] = extendContentFields;
    }
    if (fuzzinessLevel != nil) {
        queryParams[@"fuzziness_level"] = fuzzinessLevel;
    }
    if (zeroTermsQuery != nil) {
        queryParams[@"zero_terms_query"] = zeroTermsQuery;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sortBy format: @"csv"];
    }
    if (coordinates != nil) {
        queryParams[@"coordinates"] = coordinates;
    }
    if (minimumEvents != nil) {
        queryParams[@"minimum_events"] = minimumEvents;
    }
    if (cityLimit != nil) {
        queryParams[@"city_limit"] = cityLimit;
    }
    if (cityOffset != nil) {
        queryParams[@"city_offset"] = cityOffset;
    }
    if (listLimit != nil) {
        queryParams[@"list_limit"] = listLimit;
    }
    if (listOffset != nil) {
        queryParams[@"list_offset"] = listOffset;
    }
    if (venueLimit != nil) {
        queryParams[@"venue_limit"] = venueLimit;
    }
    if (venueOffset != nil) {
        queryParams[@"venue_offset"] = venueOffset;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGActivityRelatedResult>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGActivityRelatedResult>*)data, error);
                                }
                            }];
}



@end
