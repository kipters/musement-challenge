#import "SWGListApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGListType.h"
#import "SWGMusementList.h"
#import "SWGRegion.h"


@interface SWGListApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGListApi

NSString* kSWGListApiErrorDomain = @"SWGListApiErrorDomain";
NSInteger kSWGListApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Get lists for a city
/// 
///  @param cityId City identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param xMusementDeviceType  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param vertical Vertical identifier (optional)
///
///  @param excludeNotTagged If `YES` will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) (optional, default to NO)
///
///  @param listtypes List type to filter by. A collection of list type (optional)
///
///  @param listtags List of tags to filter by. A collection of tags (optional)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @returns NSArray<SWGMusementList>*
///
-(NSURLSessionTask*) citiesCityIdListsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    xMusementDeviceType: (NSString*) xMusementDeviceType
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    excludeNotTagged: (NSString*) excludeNotTagged
    listtypes: (NSArray<NSString*>*) listtypes
    listtags: (NSArray<NSString*>*) listtags
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    completionHandler: (void (^)(NSArray<SWGMusementList>* output, NSError* error)) handler {
    // verify the required parameter 'cityId' is set
    if (cityId == nil) {
        NSParameterAssert(cityId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"cityId"] };
            NSError* error = [NSError errorWithDomain:kSWGListApiErrorDomain code:kSWGListApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cities/{cityId}/lists"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cityId != nil) {
        pathParams[@"cityId"] = cityId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (vertical != nil) {
        queryParams[@"vertical"] = vertical;
    }
    if (excludeNotTagged != nil) {
        queryParams[@"exclude_not_tagged"] = excludeNotTagged;
    }
    if (listtypes != nil) {
        queryParams[@"listtypes"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: listtypes format: @"csv"];
    }
    if (listtags != nil) {
        queryParams[@"listtags"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: listtags format: @"csv"];
    }
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (xMusementDeviceType != nil) {
        headerParams[@"X-Musement-Device-Type"] = xMusementDeviceType;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGMusementList>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGMusementList>*)data, error);
                                }
                            }];
}

///
/// Get all List types
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @returns NSArray<SWGListType>*
///
-(NSURLSessionTask*) listTypesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    completionHandler: (void (^)(NSArray<SWGListType>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/list-types"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGListType>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGListType>*)data, error);
                                }
                            }];
}

///
/// Get all lists
/// 
///  @param xMusementVersion  (optional)
///
///  @param xMusementDeviceType  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param cityIn Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
///
///  @param countryIn Filter, include only results from at least one of the given countries identified by a collection of ids (optional)
///
///  @param categoryIn Filter, include only results from at least one of the given categories identified by a collection of ids (optional)
///
///  @param vertical Vertical identifier (optional)
///
///  @param excludeNotTagged If `YES` will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) (optional, default to NO)
///
///  @param temporary When set to `YES` only temporary exhibitions are returned. (optional)
///
///  @param listtypes List type to filter by. A collection of list type (optional)
///
///  @param listtags List of tags to filter by. A collection of tags (optional)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @returns NSArray<SWGMusementList>*
///
-(NSURLSessionTask*) listsGetWithXMusementVersion: (NSString*) xMusementVersion
    xMusementDeviceType: (NSString*) xMusementDeviceType
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    cityIn: (NSArray<NSNumber*>*) cityIn
    countryIn: (NSArray<NSNumber*>*) countryIn
    categoryIn: (NSArray<NSNumber*>*) categoryIn
    vertical: (NSNumber*) vertical
    excludeNotTagged: (NSString*) excludeNotTagged
    temporary: (NSString*) temporary
    listtypes: (NSArray<NSString*>*) listtypes
    listtags: (NSArray<NSString*>*) listtags
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    completionHandler: (void (^)(NSArray<SWGMusementList>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/lists"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (cityIn != nil) {
        queryParams[@"city_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: cityIn format: @"csv"];
    }
    if (countryIn != nil) {
        queryParams[@"country_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: countryIn format: @"csv"];
    }
    if (categoryIn != nil) {
        queryParams[@"category_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: categoryIn format: @"csv"];
    }
    if (vertical != nil) {
        queryParams[@"vertical"] = vertical;
    }
    if (excludeNotTagged != nil) {
        queryParams[@"exclude_not_tagged"] = excludeNotTagged;
    }
    if (temporary != nil) {
        queryParams[@"temporary"] = temporary;
    }
    if (listtypes != nil) {
        queryParams[@"listtypes"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: listtypes format: @"csv"];
    }
    if (listtags != nil) {
        queryParams[@"listtags"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: listtags format: @"csv"];
    }
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (xMusementDeviceType != nil) {
        headerParams[@"X-Musement-Device-Type"] = xMusementDeviceType;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGMusementList>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGMusementList>*)data, error);
                                }
                            }];
}

///
/// Get list by ID
/// 
///  @param listId List identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @returns SWGMusementList*
///
-(NSURLSessionTask*) listsListIdGetWithListId: (NSNumber*) listId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    completionHandler: (void (^)(SWGMusementList* output, NSError* error)) handler {
    // verify the required parameter 'listId' is set
    if (listId == nil) {
        NSParameterAssert(listId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"listId"] };
            NSError* error = [NSError errorWithDomain:kSWGListApiErrorDomain code:kSWGListApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/lists/{listId}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (listId != nil) {
        pathParams[@"listId"] = listId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGMusementList*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGMusementList*)data, error);
                                }
                            }];
}

///
/// Get all available regions for the list
/// 
///  @param listId List identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGRegion>*
///
-(NSURLSessionTask*) listsListIdRegionsGetWithListId: (NSNumber*) listId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler {
    // verify the required parameter 'listId' is set
    if (listId == nil) {
        NSParameterAssert(listId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"listId"] };
            NSError* error = [NSError errorWithDomain:kSWGListApiErrorDomain code:kSWGListApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/lists/{listId}/regions"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (listId != nil) {
        pathParams[@"listId"] = listId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGRegion>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGRegion>*)data, error);
                                }
                            }];
}



@end
