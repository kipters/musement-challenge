#import "SWGSupplierOrderApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGOrder.h"


@interface SWGSupplierOrderApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGSupplierOrderApi

NSString* kSWGSupplierOrderApiErrorDomain = @"SWGSupplierOrderApiErrorDomain";
NSInteger kSWGSupplierOrderApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Update order item for the logged in supplier
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGOrder>*
///
-(NSURLSessionTask*) suppliersMeOrderItemsOrderItemUuidPatchWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/suppliers/me/order-items/{orderItemUuid}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PATCH"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGOrder>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGOrder>*)data, error);
                                }
                            }];
}

///
/// Get orders for the logged in supplier
/// 
///  @param offset Pagination offset (optional, default to 0)
///
///  @param page Page from which starting to return found events (mandatory if limit is given) (optional)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param withTicketStatus Filter, include only results with given ticket statuses (optional)
///
///  @returns NSArray<SWGOrder>*
///
-(NSURLSessionTask*) suppliersMeOrdersGetWithOffset: (NSNumber*) offset
    page: (NSNumber*) page
    limit: (NSNumber*) limit
    withTicketStatus: (NSArray<NSString*>*) withTicketStatus
    completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/suppliers/me/orders"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (page != nil) {
        queryParams[@"page"] = page;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (withTicketStatus != nil) {
        queryParams[@"with_ticket_status"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: withTicketStatus format: @"csv"];
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGOrder>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGOrder>*)data, error);
                                }
                            }];
}

///
/// Update order item for a particular supplier
/// 
///  @param supplierUuid Supplier UUID 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGOrder>*
///
-(NSURLSessionTask*) suppliersSupplierUuidOrderItemsOrderItemUuidPatchWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler {
    // verify the required parameter 'supplierUuid' is set
    if (supplierUuid == nil) {
        NSParameterAssert(supplierUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"supplierUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGSupplierOrderApiErrorDomain code:kSWGSupplierOrderApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/suppliers/{supplierUuid}/order-items/{orderItemUuid}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (supplierUuid != nil) {
        pathParams[@"supplierUuid"] = supplierUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PATCH"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGOrder>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGOrder>*)data, error);
                                }
                            }];
}

///
/// Get orders for a particular supplier
/// 
///  @param supplierUuid Supplier UUID 
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param page Page from which starting to return found events (mandatory if limit is given) (optional)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param withTicketStatus Filter, include only results with given ticket statuses (optional)
///
///  @param dateFrom Start date | Use format: YYYY-MM-DD (optional)
///
///  @param dateTo To date | Use format: YYYY-MM-DD (optional)
///
///  @param identifier Internal Musement order identifier | Use format: MUS12345 (optional)
///
///  @param customer Customer name (both first name & last name) (optional)
///
///  @param activityTitle Activity title (optional)
///
///  @param activityUuid Activity UUID (optional)
///
///  @param activityDateFrom Start date | Use format: YYYY-MM-DD (optional)
///
///  @param activityDateTo End date | Use format: YYYY-MM-DD (optional)
///
///  @returns NSArray<SWGOrder>*
///
-(NSURLSessionTask*) suppliersSupplierUuidOrdersGetWithSupplierUuid: (NSString*) supplierUuid
    offset: (NSNumber*) offset
    page: (NSNumber*) page
    limit: (NSNumber*) limit
    withTicketStatus: (NSArray<NSString*>*) withTicketStatus
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    identifier: (NSString*) identifier
    customer: (NSString*) customer
    activityTitle: (NSString*) activityTitle
    activityUuid: (NSString*) activityUuid
    activityDateFrom: (NSString*) activityDateFrom
    activityDateTo: (NSString*) activityDateTo
    completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler {
    // verify the required parameter 'supplierUuid' is set
    if (supplierUuid == nil) {
        NSParameterAssert(supplierUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"supplierUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGSupplierOrderApiErrorDomain code:kSWGSupplierOrderApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/suppliers/{supplierUuid}/orders"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (supplierUuid != nil) {
        pathParams[@"supplierUuid"] = supplierUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (page != nil) {
        queryParams[@"page"] = page;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (withTicketStatus != nil) {
        queryParams[@"with_ticket_status"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: withTicketStatus format: @"csv"];
    }
    if (dateFrom != nil) {
        queryParams[@"date_from"] = dateFrom;
    }
    if (dateTo != nil) {
        queryParams[@"date_to"] = dateTo;
    }
    if (identifier != nil) {
        queryParams[@"identifier"] = identifier;
    }
    if (customer != nil) {
        queryParams[@"customer"] = customer;
    }
    if (activityTitle != nil) {
        queryParams[@"activity_title"] = activityTitle;
    }
    if (activityUuid != nil) {
        queryParams[@"activity_uuid"] = activityUuid;
    }
    if (activityDateFrom != nil) {
        queryParams[@"activity_date_from"] = activityDateFrom;
    }
    if (activityDateTo != nil) {
        queryParams[@"activity_date_to"] = activityDateTo;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGOrder>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGOrder>*)data, error);
                                }
                            }];
}



@end
