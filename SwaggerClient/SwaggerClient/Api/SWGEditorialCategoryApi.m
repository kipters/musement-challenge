#import "SWGEditorialCategoryApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGEditorialCategory.h"
#import "SWGEvent.h"
#import "SWGFlavour.h"


@interface SWGEditorialCategoryApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGEditorialCategoryApi

NSString* kSWGEditorialCategoryApiErrorDomain = @"SWGEditorialCategoryApiErrorDomain";
NSInteger kSWGEditorialCategoryApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Search activities within a given editorial category
/// Get all activities for an editorial category sorted by relevance. Only the events that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'
///  @param editorialcategoryId Editorial category identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param vertical Vertical identifier (optional)
///
///  @param city City identifier (optional)
///
///  @param category Category identifier (optional)
///
///  @param venue Venue identifier (optional)
///
///  @param dateFrom Start date | Use format: YYYY-MM-DD (optional)
///
///  @param dateTo To date | Use format: YYYY-MM-DD (optional)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param sortBy Sorting strategy (optional, default to relevance)
///
///  @returns NSArray<SWGEvent>*
///
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdActivitiesGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    category: (NSNumber*) category
    venue: (NSNumber*) venue
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
    completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler {
    // verify the required parameter 'editorialcategoryId' is set
    if (editorialcategoryId == nil) {
        NSParameterAssert(editorialcategoryId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"editorialcategoryId"] };
            NSError* error = [NSError errorWithDomain:kSWGEditorialCategoryApiErrorDomain code:kSWGEditorialCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/editorial-categories/{editorialcategoryId}/activities"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (editorialcategoryId != nil) {
        pathParams[@"editorialcategoryId"] = editorialcategoryId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (vertical != nil) {
        queryParams[@"vertical"] = vertical;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (category != nil) {
        queryParams[@"category"] = category;
    }
    if (venue != nil) {
        queryParams[@"venue"] = venue;
    }
    if (dateFrom != nil) {
        queryParams[@"date_from"] = dateFrom;
    }
    if (dateTo != nil) {
        queryParams[@"date_to"] = dateTo;
    }
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = sortBy;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGEvent>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGEvent>*)data, error);
                                }
                            }];
}

///
/// Get all flavours connected to an editorial category
/// 
///  @param editorialcategoryId Editorial category identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGFlavour*
///
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdFlavoursGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGFlavour* output, NSError* error)) handler {
    // verify the required parameter 'editorialcategoryId' is set
    if (editorialcategoryId == nil) {
        NSParameterAssert(editorialcategoryId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"editorialcategoryId"] };
            NSError* error = [NSError errorWithDomain:kSWGEditorialCategoryApiErrorDomain code:kSWGEditorialCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/editorial-categories/{editorialcategoryId}/flavours"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (editorialcategoryId != nil) {
        pathParams[@"editorialcategoryId"] = editorialcategoryId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGFlavour*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGFlavour*)data, error);
                                }
                            }];
}

///
/// Get an editorial category by ID
/// 
///  @param editorialcategoryId Editorial category identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGEditorialCategory*
///
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGEditorialCategory* output, NSError* error)) handler {
    // verify the required parameter 'editorialcategoryId' is set
    if (editorialcategoryId == nil) {
        NSParameterAssert(editorialcategoryId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"editorialcategoryId"] };
            NSError* error = [NSError errorWithDomain:kSWGEditorialCategoryApiErrorDomain code:kSWGEditorialCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/editorial-categories/{editorialcategoryId}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (editorialcategoryId != nil) {
        pathParams[@"editorialcategoryId"] = editorialcategoryId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGEditorialCategory*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGEditorialCategory*)data, error);
                                }
                            }];
}

///
/// Get all editorial categories
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @returns NSArray<SWGEditorialCategory>*
///
-(NSURLSessionTask*) editorialCategoriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    completionHandler: (void (^)(NSArray<SWGEditorialCategory>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/editorial-categories"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGEditorialCategory>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGEditorialCategory>*)data, error);
                                }
                            }];
}



@end
