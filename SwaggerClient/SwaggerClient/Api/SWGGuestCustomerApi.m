#import "SWGGuestCustomerApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGCustomer.h"
#import "SWGPatchCustomerGuestCart.h"


@interface SWGGuestCustomerApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGGuestCustomerApi

NSString* kSWGGuestCustomerApiErrorDomain = @"SWGGuestCustomerApiErrorDomain";
NSInteger kSWGGuestCustomerApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Patch cart customer data
/// 
///  @param cartUuid Cart identifier 
///
///  @param patchCustomerGuestCart Customer data 
///
///  @param xMusementVersion  (optional)
///
///  @returns SWGCustomer*
///
-(NSURLSessionTask*) cartsCartUuidCustomerPatchWithCartUuid: (NSString*) cartUuid
    patchCustomerGuestCart: (SWGPatchCustomerGuestCart*) patchCustomerGuestCart
    xMusementVersion: (NSString*) xMusementVersion
    completionHandler: (void (^)(SWGCustomer* output, NSError* error)) handler {
    // verify the required parameter 'cartUuid' is set
    if (cartUuid == nil) {
        NSParameterAssert(cartUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"cartUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGGuestCustomerApiErrorDomain code:kSWGGuestCustomerApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'patchCustomerGuestCart' is set
    if (patchCustomerGuestCart == nil) {
        NSParameterAssert(patchCustomerGuestCart);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"patchCustomerGuestCart"] };
            NSError* error = [NSError errorWithDomain:kSWGGuestCustomerApiErrorDomain code:kSWGGuestCustomerApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/carts/{cartUuid}/customer"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cartUuid != nil) {
        pathParams[@"cartUuid"] = cartUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = patchCustomerGuestCart;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PATCH"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGCustomer*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGCustomer*)data, error);
                                }
                            }];
}



@end
