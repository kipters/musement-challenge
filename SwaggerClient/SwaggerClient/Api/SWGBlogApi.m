#import "SWGBlogApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGBlogpost.h"
#import "SWGBlogpostCategory.h"
#import "SWGBlogpostCategoryAggregated.h"


@interface SWGBlogApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGBlogApi

NSString* kSWGBlogApiErrorDomain = @"SWGBlogApiErrorDomain";
NSInteger kSWGBlogApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Get all available blogpost categories
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGBlogpostCategoryAggregated>*
///
-(NSURLSessionTask*) blogpostsCategoriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGBlogpostCategoryAggregated>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/blogposts-categories"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGBlogpostCategoryAggregated>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGBlogpostCategoryAggregated>*)data, error);
                                }
                            }];
}

///
/// Get a specific blogpost category
/// 
///  @param _id Category identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGBlogpostCategory*
///
-(NSURLSessionTask*) blogpostsCategoriesIdGetWithId: (NSNumber*) _id
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGBlogpostCategory* output, NSError* error)) handler {
    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGBlogApiErrorDomain code:kSWGBlogApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/blogposts-categories/{id}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGBlogpostCategory*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGBlogpostCategory*)data, error);
                                }
                            }];
}

///
/// Get a blogposts
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param blogpostCategory The blog post category uuid to filter by (optional)
///
///  @param homePageFeatured If true will include only home page featured posts (optional, default to false)
///
///  @param category Category identifier (optional)
///
///  @param vertical Vertical identifier (optional)
///
///  @param list List identifier to filter by (optional)
///
///  @param city City identifier to filter by (optional)
///
///  @param venue Venue identifier (optional)
///
///  @param page Page from which starting to return found events (mandatory if limit is given) (optional)
///
///  @param sortBy Ordering criteria: [publish_date|relevance], prepend `-` for descending order (optional)
///
///  @returns NSArray<SWGBlogpost>*
///
-(NSURLSessionTask*) blogpostsGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    blogpostCategory: (NSString*) blogpostCategory
    homePageFeatured: (NSNumber*) homePageFeatured
    category: (NSNumber*) category
    vertical: (NSNumber*) vertical
    list: (NSString*) list
    city: (NSString*) city
    venue: (NSNumber*) venue
    page: (NSNumber*) page
    sortBy: (NSArray<NSString*>*) sortBy
    completionHandler: (void (^)(NSArray<SWGBlogpost>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/blogposts"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (blogpostCategory != nil) {
        queryParams[@"blogpost_category"] = blogpostCategory;
    }
    if (homePageFeatured != nil) {
        queryParams[@"home_page_featured"] = [homePageFeatured isEqual:@(YES)] ? @"true" : @"false";
    }
    if (category != nil) {
        queryParams[@"category"] = category;
    }
    if (vertical != nil) {
        queryParams[@"vertical"] = vertical;
    }
    if (list != nil) {
        queryParams[@"list"] = list;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (venue != nil) {
        queryParams[@"venue"] = venue;
    }
    if (page != nil) {
        queryParams[@"page"] = page;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sortBy format: @"csv"];
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGBlogpost>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGBlogpost>*)data, error);
                                }
                            }];
}

///
/// Get blogpost by identifier
/// 
///  @param _id Blogpost unique identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGBlogpost*
///
-(NSURLSessionTask*) blogpostsIdGetWithId: (NSString*) _id
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGBlogpost* output, NSError* error)) handler {
    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGBlogApiErrorDomain code:kSWGBlogApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/blogposts/{id}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGBlogpost*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGBlogpost*)data, error);
                                }
                            }];
}

///
/// Get previous o next blogposts
/// 
///  @param _id Blogpost unique identifier 
///
///  @param position Indicate if you want the prev or the next post 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGBlogpost*
///
-(NSURLSessionTask*) blogpostsIdPositionGetWithId: (NSString*) _id
    position: (NSString*) position
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGBlogpost* output, NSError* error)) handler {
    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGBlogApiErrorDomain code:kSWGBlogApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'position' is set
    if (position == nil) {
        NSParameterAssert(position);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"position"] };
            NSError* error = [NSError errorWithDomain:kSWGBlogApiErrorDomain code:kSWGBlogApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/blogposts/{id}/{position}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }
    if (position != nil) {
        pathParams[@"position"] = position;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGBlogpost*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGBlogpost*)data, error);
                                }
                            }];
}

///
/// Get the blogposts for the city
/// 
///  @param cityId City identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGBlogpost>*
///
-(NSURLSessionTask*) citiesCityIdBlogpostsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGBlogpost>* output, NSError* error)) handler {
    // verify the required parameter 'cityId' is set
    if (cityId == nil) {
        NSParameterAssert(cityId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"cityId"] };
            NSError* error = [NSError errorWithDomain:kSWGBlogApiErrorDomain code:kSWGBlogApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cities/{cityId}/blogposts"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cityId != nil) {
        pathParams[@"cityId"] = cityId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGBlogpost>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGBlogpost>*)data, error);
                                }
                            }];
}



@end
