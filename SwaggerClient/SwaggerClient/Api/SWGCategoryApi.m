#import "SWGCategoryApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGCategory.h"
#import "SWGCategoryAggregated.h"
#import "SWGEvent.h"


@interface SWGCategoryApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGCategoryApi

NSString* kSWGCategoryApiErrorDomain = @"SWGCategoryApiErrorDomain";
NSInteger kSWGCategoryApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Search activities within a given category
/// Get all activities for a category sorted by category relevance. Only the activities that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'
///  @param categoryId Category identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param vertical Vertical identifier (optional)
///
///  @param city City identifier (optional)
///
///  @param venue Venue identifier (optional)
///
///  @param editorialCategory Editorial category identifier (optional)
///
///  @param dateFrom Start date | Use format: YYYY-MM-DD (optional)
///
///  @param dateTo To date | Use format: YYYY-MM-DD (optional)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param sortBy Sorting strategy (optional, default to venue-relevance)
///
///  @returns NSArray<SWGEvent>*
///
-(NSURLSessionTask*) categoriesCategoryIdActivitiesGetWithCategoryId: (NSNumber*) categoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    venue: (NSNumber*) venue
    editorialCategory: (NSNumber*) editorialCategory
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
    completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler {
    // verify the required parameter 'categoryId' is set
    if (categoryId == nil) {
        NSParameterAssert(categoryId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"categoryId"] };
            NSError* error = [NSError errorWithDomain:kSWGCategoryApiErrorDomain code:kSWGCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/categories/{categoryId}/activities"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (categoryId != nil) {
        pathParams[@"categoryId"] = categoryId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (vertical != nil) {
        queryParams[@"vertical"] = vertical;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (venue != nil) {
        queryParams[@"venue"] = venue;
    }
    if (editorialCategory != nil) {
        queryParams[@"editorial-category"] = editorialCategory;
    }
    if (dateFrom != nil) {
        queryParams[@"date_from"] = dateFrom;
    }
    if (dateTo != nil) {
        queryParams[@"date_to"] = dateTo;
    }
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = sortBy;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGEvent>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGEvent>*)data, error);
                                }
                            }];
}

///
/// Get a category by identifier
/// 
///  @param categoryId Category identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGCategory*
///
-(NSURLSessionTask*) categoriesCategoryIdGetWithCategoryId: (NSNumber*) categoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGCategory* output, NSError* error)) handler {
    // verify the required parameter 'categoryId' is set
    if (categoryId == nil) {
        NSParameterAssert(categoryId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"categoryId"] };
            NSError* error = [NSError errorWithDomain:kSWGCategoryApiErrorDomain code:kSWGCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/categories/{categoryId}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (categoryId != nil) {
        pathParams[@"categoryId"] = categoryId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGCategory*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGCategory*)data, error);
                                }
                            }];
}

///
/// Get categories
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param page Page from which starting to return found events (mandatory if limit is given) (optional)
///
///  @param sortBy Ordering criteria: [relevance|city_relevance|discount], prepend `-` for descending order (optional)
///
///  @param filteringAware If true the category is usable as filter (optional)
///
///  @returns NSArray<SWGCategory>*
///
-(NSURLSessionTask*) categoriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    page: (NSNumber*) page
    sortBy: (NSArray<NSString*>*) sortBy
    filteringAware: (NSNumber*) filteringAware
    completionHandler: (void (^)(NSArray<SWGCategory>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/categories"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (page != nil) {
        queryParams[@"page"] = page;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sortBy format: @"csv"];
    }
    if (filteringAware != nil) {
        queryParams[@"filtering_aware"] = [filteringAware isEqual:@(YES)] ? @"true" : @"false";
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGCategory>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGCategory>*)data, error);
                                }
                            }];
}

///
/// Categories for the city. Sorted by the number of events for the category
/// Categories for the city. Sorted by the number of activities for the category
///  @param cityId City identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGCategory>*
///
-(NSURLSessionTask*) citiesCityIdCategoriesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGCategory>* output, NSError* error)) handler {
    // verify the required parameter 'cityId' is set
    if (cityId == nil) {
        NSParameterAssert(cityId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"cityId"] };
            NSError* error = [NSError errorWithDomain:kSWGCategoryApiErrorDomain code:kSWGCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cities/{cityId}/categories"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cityId != nil) {
        pathParams[@"cityId"] = cityId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGCategory>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGCategory>*)data, error);
                                }
                            }];
}

///
/// Categories for the city for a specific vertical. Sorted by the number of activities for the category
/// 
///  @param cityId City identifier 
///
///  @param verticalId Vertical identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns NSArray<SWGCategoryAggregated>*
///
-(NSURLSessionTask*) citiesCityIdVerticalsVerticalIdCategoriesGetWithCityId: (NSNumber*) cityId
    verticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGCategoryAggregated>* output, NSError* error)) handler {
    // verify the required parameter 'cityId' is set
    if (cityId == nil) {
        NSParameterAssert(cityId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"cityId"] };
            NSError* error = [NSError errorWithDomain:kSWGCategoryApiErrorDomain code:kSWGCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'verticalId' is set
    if (verticalId == nil) {
        NSParameterAssert(verticalId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"verticalId"] };
            NSError* error = [NSError errorWithDomain:kSWGCategoryApiErrorDomain code:kSWGCategoryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cities/{cityId}/verticals/{verticalId}/categories"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cityId != nil) {
        pathParams[@"cityId"] = cityId;
    }
    if (verticalId != nil) {
        pathParams[@"verticalId"] = verticalId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGCategoryAggregated>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGCategoryAggregated>*)data, error);
                                }
                            }];
}



@end
