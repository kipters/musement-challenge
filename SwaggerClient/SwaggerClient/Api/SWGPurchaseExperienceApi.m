#import "SWGPurchaseExperienceApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGExceptionResponse.h"
#import "SWGFeedbackRequest.h"


@interface SWGPurchaseExperienceApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGPurchaseExperienceApi

NSString* kSWGPurchaseExperienceApiErrorDomain = @"SWGPurchaseExperienceApiErrorDomain";
NSInteger kSWGPurchaseExperienceApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Get feedback request details
/// 
///  @param feedbackRequestNonce Feedback request nonce 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @returns SWGFeedbackRequest*
///
-(NSURLSessionTask*) feedbackRequestsFeedbackRequestNonceGetWithFeedbackRequestNonce: (NSString*) feedbackRequestNonce
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGFeedbackRequest* output, NSError* error)) handler {
    // verify the required parameter 'feedbackRequestNonce' is set
    if (feedbackRequestNonce == nil) {
        NSParameterAssert(feedbackRequestNonce);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"feedbackRequestNonce"] };
            NSError* error = [NSError errorWithDomain:kSWGPurchaseExperienceApiErrorDomain code:kSWGPurchaseExperienceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/feedback-requests/{feedbackRequestNonce}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (feedbackRequestNonce != nil) {
        pathParams[@"feedbackRequestNonce"] = feedbackRequestNonce;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGFeedbackRequest*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGFeedbackRequest*)data, error);
                                }
                            }];
}



@end
