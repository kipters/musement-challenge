#import "SWGDateApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGActivityCalendarUpdateRequest.h"
#import "SWGBlackoutDay.h"
#import "SWGGiftSchedule.h"
#import "SWGPostActivityBlackoutDay.h"
#import "SWGPostActivityOpenSeason.h"
#import "SWGPostTimedSeason.h"
#import "SWGSchedule.h"


@interface SWGDateApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGDateApi

NSString* kSWGDateApiErrorDomain = @"SWGDateApiErrorDomain";
NSInteger kSWGDateApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Add a blackout day
/// 
///  @param activityUuid Activity identifier 
///
///  @param blackoutDay Day to add to the blackout one 
///
///  @param xMusementVersion  (optional)
///
///  @returns SWGBlackoutDay*
///
-(NSURLSessionTask*) activitiesActivityUuidBlackoutDaysPostWithActivityUuid: (NSString*) activityUuid
    blackoutDay: (SWGPostActivityBlackoutDay*) blackoutDay
    xMusementVersion: (NSString*) xMusementVersion
    completionHandler: (void (^)(SWGBlackoutDay* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'blackoutDay' is set
    if (blackoutDay == nil) {
        NSParameterAssert(blackoutDay);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"blackoutDay"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/blackout-days"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = blackoutDay;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGBlackoutDay*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGBlackoutDay*)data, error);
                                }
                            }];
}

///
/// Get schedule for a day for an event
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param ticketsNumber Number of ticket requested tickets | Only useful for real time activities (optional)
///
///  @returns NSArray<SWGSchedule>*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    ticketsNumber: (NSString*) ticketsNumber
    completionHandler: (void (^)(NSArray<SWGSchedule>* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (ticketsNumber != nil) {
        queryParams[@"tickets_number"] = ticketsNumber;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGSchedule>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGSchedule>*)data, error);
                                }
                            }];
}

///
/// Get available dates for an event
/// 
///  @param activityUuid Activity identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @param dateFrom Start date | If not specified set to today | Use format: YYYY-MM-DD (optional)
///
///  @param dateTo To date | If not specified set to next year | Use format: YYYY-MM-DD (optional)
///
///  @returns NSDate*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    completionHandler: (void (^)(NSDate* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (dateFrom != nil) {
        queryParams[@"date_from"] = dateFrom;
    }
    if (dateTo != nil) {
        queryParams[@"date_to"] = dateTo;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSDate*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSDate*)data, error);
                                }
                            }];
}

///
/// Add an open season to an activity
/// 
///  @param openSeason Open season 
///
///  @param activityUuid Activity identifier 
///
///  @returns SWGActivityCalendarUpdateRequest*
///
-(NSURLSessionTask*) activitiesActivityUuidOpenSeasonsPostWithOpenSeason: (SWGPostActivityOpenSeason*) openSeason
    activityUuid: (NSString*) activityUuid
    completionHandler: (void (^)(SWGActivityCalendarUpdateRequest* output, NSError* error)) handler {
    // verify the required parameter 'openSeason' is set
    if (openSeason == nil) {
        NSParameterAssert(openSeason);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"openSeason"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/open-seasons"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = openSeason;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGActivityCalendarUpdateRequest*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGActivityCalendarUpdateRequest*)data, error);
                                }
                            }];
}

///
/// Add a season to an activity
/// 
///  @param season Activity season 
///
///  @param activityUuid Activity identifier 
///
///  @returns SWGActivityCalendarUpdateRequest*
///
-(NSURLSessionTask*) activitiesActivityUuidTimedSeasonsPostWithSeason: (SWGPostTimedSeason*) season
    activityUuid: (NSString*) activityUuid
    completionHandler: (void (^)(SWGActivityCalendarUpdateRequest* output, NSError* error)) handler {
    // verify the required parameter 'season' is set
    if (season == nil) {
        NSParameterAssert(season);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"season"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/timed-seasons"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = season;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGActivityCalendarUpdateRequest*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGActivityCalendarUpdateRequest*)data, error);
                                }
                            }];
}

///
/// Get schedule for a 'Gift creation' for an Event
/// 
///  @param eventId Event identifier 
///
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @returns SWGGiftSchedule*
///
-(NSURLSessionTask*) eventsEventIdGiftScheduleGetWithEventId: (NSNumber*) eventId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    completionHandler: (void (^)(SWGGiftSchedule* output, NSError* error)) handler {
    // verify the required parameter 'eventId' is set
    if (eventId == nil) {
        NSParameterAssert(eventId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"eventId"] };
            NSError* error = [NSError errorWithDomain:kSWGDateApiErrorDomain code:kSWGDateApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/events/{eventId}/gift-schedule"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (eventId != nil) {
        pathParams[@"eventId"] = eventId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGiftSchedule*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGiftSchedule*)data, error);
                                }
                            }];
}



@end
