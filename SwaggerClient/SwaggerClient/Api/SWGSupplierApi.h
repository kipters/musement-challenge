#import <Foundation/Foundation.h>
#import "SWGPatchSupplierProfile.h"
#import "SWGSupplier.h"
#import "SWGTranslatedMetadata.h"
#import "SWGApi.h"

/**
* Musement API
* Musement API
*
* OpenAPI spec version: 3.3.0
* Contact: api-support@musement.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGSupplierApi: NSObject <SWGApi>

extern NSString* kSWGSupplierApiErrorDomain;
extern NSInteger kSWGSupplierApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Get all active suppliers
/// 
///
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// 
///  code:200 message:"Returned when successful",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return NSArray<SWGTranslatedMetadata>*
-(NSURLSessionTask*) suppliersGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;


/// Get supplier data
/// 
///
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// 
///  code:200 message:"Returned when successful",
///  code:404 message:"Returned when supplier not exists",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return SWGSupplier*
-(NSURLSessionTask*) suppliersSupplierUuidGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;


/// Update supplier data
/// 
///
/// @param patchSupplierRegistration Supplier registrations data
/// @param xMusementVersion  (optional)
/// @param acceptLanguage  (optional) (default to en-US)
/// 
///  code:200 message:"Returned when successful",
///  code:400 message:"Returned if sent data contains errors",
///  code:404 message:"Returned when supplier not exists",
///  code:503 message:"Returned when the service is unavailable"
///
/// @return SWGSupplier*
-(NSURLSessionTask*) suppliersSupplierUuidPatchWithPatchSupplierRegistration: (SWGPatchSupplierProfile*) patchSupplierRegistration
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;



@end
