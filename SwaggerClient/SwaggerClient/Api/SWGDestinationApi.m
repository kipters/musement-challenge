#import "SWGDestinationApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGDestination.h"


@interface SWGDestinationApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGDestinationApi

NSString* kSWGDestinationApiErrorDomain = @"SWGDestinationApiErrorDomain";
NSInteger kSWGDestinationApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Get one destination
/// 
///  @param destinationId Destination identifier 
///
///  @returns SWGDestination*
///
-(NSURLSessionTask*) destinationsDestinationIdGetWithDestinationId: (NSNumber*) destinationId
    completionHandler: (void (^)(SWGDestination* output, NSError* error)) handler {
    // verify the required parameter 'destinationId' is set
    if (destinationId == nil) {
        NSParameterAssert(destinationId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"destinationId"] };
            NSError* error = [NSError errorWithDomain:kSWGDestinationApiErrorDomain code:kSWGDestinationApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/destinations/{destinationId}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (destinationId != nil) {
        pathParams[@"destinationId"] = destinationId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGDestination*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGDestination*)data, error);
                                }
                            }];
}

///
/// Get destinations
/// 
///  @param categoryIn Filter, include only results from at least one of the given categories identified by a collection of ids (optional)
///
///  @param verticalIn Filter, include only results from given verticals identified by a collection of ids (optional)
///
///  @param cityIn Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
///
///  @param maxDistance Maximum distance expressed with its unit from a given point coordinates expressed in decimal grades, accepts kilometers and miles. E.G.: 44.12233|12.23233|100KM or 44.12233|12.23233|62M (optional)
///
///  @param maxDistanceFromCity Maximum distance expressed with its unit from a given city id, accepts kilometers and miles. E.G.: 231|100KM or 51234|12.23233|62M (optional)
///
///  @param nowOpen Is set to `YES` only activity open now are returned. (optional)
///
///  @param sortBy Ordering criteria to apply, prepend `-` for descending order (optional)
///
///  @param limit Max number of items in the response (optional, default to 10)
///
///  @param offset Pagination offset (optional, default to 0)
///
///  @param page Page from which starting to return found events (mandatory if limit is given) (optional)
///
///  @returns NSArray<SWGDestination>*
///
-(NSURLSessionTask*) destinationsGetWithCategoryIn: (NSArray<NSNumber*>*) categoryIn
    verticalIn: (NSArray<NSNumber*>*) verticalIn
    cityIn: (NSArray<NSNumber*>*) cityIn
    maxDistance: (NSString*) maxDistance
    maxDistanceFromCity: (NSString*) maxDistanceFromCity
    nowOpen: (NSNumber*) nowOpen
    sortBy: (NSArray<NSString*>*) sortBy
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
    page: (NSNumber*) page
    completionHandler: (void (^)(NSArray<SWGDestination>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/destinations"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (categoryIn != nil) {
        queryParams[@"category_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: categoryIn format: @"csv"];
    }
    if (verticalIn != nil) {
        queryParams[@"vertical_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: verticalIn format: @"csv"];
    }
    if (cityIn != nil) {
        queryParams[@"city_in"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: cityIn format: @"csv"];
    }
    if (maxDistance != nil) {
        queryParams[@"max_distance"] = maxDistance;
    }
    if (maxDistanceFromCity != nil) {
        queryParams[@"max_distance_from_city"] = maxDistanceFromCity;
    }
    if (nowOpen != nil) {
        queryParams[@"now_open"] = nowOpen;
    }
    if (sortBy != nil) {
        queryParams[@"sort_by"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sortBy format: @"csv"];
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (offset != nil) {
        queryParams[@"offset"] = offset;
    }
    if (page != nil) {
        queryParams[@"page"] = page;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGDestination>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGDestination>*)data, error);
                                }
                            }];
}



@end
