#import "SWGTimeslotApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGPatchTimeslot.h"
#import "SWGPostTimeslot.h"
#import "SWGPutTimeslotProduct.h"
#import "SWGPutTimeslotProducts.h"
#import "SWGTimeslot.h"
#import "SWGTimeslotProduct.h"


@interface SWGTimeslotApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGTimeslotApi

NSString* kSWGTimeslotApiErrorDomain = @"SWGTimeslotApiErrorDomain";
NSInteger kSWGTimeslotApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Add a timeslot
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param postTimeslot Timeslot post request 
///
///  @param xMusementVersion  (optional)
///
///  @returns SWGTimeslot*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsPostWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    postTimeslot: (SWGPostTimeslot*) postTimeslot
    xMusementVersion: (NSString*) xMusementVersion
    completionHandler: (void (^)(SWGTimeslot* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'postTimeslot' is set
    if (postTimeslot == nil) {
        NSParameterAssert(postTimeslot);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"postTimeslot"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}/timeslots"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = postTimeslot;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGTimeslot*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGTimeslot*)data, error);
                                }
                            }];
}

///
/// Get data for a timeslot
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param time String containing hours and minutes in the format 'HH-MM' | Example: 22-40 
///
///  @param xMusementVersion  (optional)
///
///  @returns SWGTimeslot*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    xMusementVersion: (NSString*) xMusementVersion
    completionHandler: (void (^)(SWGTimeslot* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'time' is set
    if (time == nil) {
        NSParameterAssert(time);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"time"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}/timeslots/{time}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }
    if (time != nil) {
        pathParams[@"time"] = time;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGTimeslot*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGTimeslot*)data, error);
                                }
                            }];
}

///
/// Update data for a timeslot
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param time String containing hours and minutes in the format 'HH-MM' | Example: 22-40 
///
///  @param patchTimeslot Timeslot patch request 
///
///  @param xMusementVersion  (optional)
///
///  @returns SWGTimeslot*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimePatchWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    patchTimeslot: (SWGPatchTimeslot*) patchTimeslot
    xMusementVersion: (NSString*) xMusementVersion
    completionHandler: (void (^)(SWGTimeslot* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'time' is set
    if (time == nil) {
        NSParameterAssert(time);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"time"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'patchTimeslot' is set
    if (patchTimeslot == nil) {
        NSParameterAssert(patchTimeslot);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"patchTimeslot"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}/timeslots/{time}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }
    if (time != nil) {
        pathParams[@"time"] = time;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = patchTimeslot;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PATCH"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGTimeslot*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGTimeslot*)data, error);
                                }
                            }];
}

///
/// Get data for a product of a timeslot
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param time String containing hours and minutes in the format 'HH-MM' | Example: 22-40 
///
///  @param pricetagCode Full pricetag code holder and feature separated by pipe | Example: adult|entrance-with-audioguide 
///
///  @param xMusementVersion  (optional)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @returns SWGTimeslotProduct*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodeGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    pricetagCode: (NSString*) pricetagCode
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
    completionHandler: (void (^)(SWGTimeslotProduct* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'time' is set
    if (time == nil) {
        NSParameterAssert(time);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"time"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'pricetagCode' is set
    if (pricetagCode == nil) {
        NSParameterAssert(pricetagCode);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"pricetagCode"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}/timeslots/{time}/products/{pricetagCode}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }
    if (time != nil) {
        pathParams[@"time"] = time;
    }
    if (pricetagCode != nil) {
        pathParams[@"pricetagCode"] = pricetagCode;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGTimeslotProduct*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGTimeslotProduct*)data, error);
                                }
                            }];
}

///
/// Update data of a product for a specific timeslot
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param time String containing hours and minutes in the format 'HH-MM' | Example: 22-40 
///
///  @param pricetagCode Full pricetag code holder and feature separated by pipe | Example: adult|entrance-with-audioguide 
///
///  @param putTimeslotProduct Timeslot product put request 
///
///  @param xMusementVersion  (optional)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @returns SWGTimeslotProduct*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodePutWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    pricetagCode: (NSString*) pricetagCode
    putTimeslotProduct: (SWGPutTimeslotProduct*) putTimeslotProduct
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
    completionHandler: (void (^)(SWGTimeslotProduct* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'time' is set
    if (time == nil) {
        NSParameterAssert(time);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"time"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'pricetagCode' is set
    if (pricetagCode == nil) {
        NSParameterAssert(pricetagCode);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"pricetagCode"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'putTimeslotProduct' is set
    if (putTimeslotProduct == nil) {
        NSParameterAssert(putTimeslotProduct);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"putTimeslotProduct"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}/timeslots/{time}/products/{pricetagCode}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }
    if (time != nil) {
        pathParams[@"time"] = time;
    }
    if (pricetagCode != nil) {
        pathParams[@"pricetagCode"] = pricetagCode;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = putTimeslotProduct;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PUT"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGTimeslotProduct*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGTimeslotProduct*)data, error);
                                }
                            }];
}

///
/// Update all the products for a specific timeslot
/// 
///  @param activityUuid Activity identifier 
///
///  @param day Day 
///
///  @param time String containing hours and minutes in the format 'HH-MM' | Example: 22-40 
///
///  @param putTimeslotProducts Timeslot products put request 
///
///  @param xMusementVersion  (optional)
///
///  @param xMusementCurrency  (optional, default to USD)
///
///  @returns SWGTimeslotProduct*
///
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeProductsPutWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    putTimeslotProducts: (SWGPutTimeslotProducts*) putTimeslotProducts
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
    completionHandler: (void (^)(SWGTimeslotProduct* output, NSError* error)) handler {
    // verify the required parameter 'activityUuid' is set
    if (activityUuid == nil) {
        NSParameterAssert(activityUuid);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"activityUuid"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'day' is set
    if (day == nil) {
        NSParameterAssert(day);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"day"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'time' is set
    if (time == nil) {
        NSParameterAssert(time);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"time"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'putTimeslotProducts' is set
    if (putTimeslotProducts == nil) {
        NSParameterAssert(putTimeslotProducts);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"putTimeslotProducts"] };
            NSError* error = [NSError errorWithDomain:kSWGTimeslotApiErrorDomain code:kSWGTimeslotApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/activities/{activityUuid}/dates/{day}/timeslots/{time}/products"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (activityUuid != nil) {
        pathParams[@"activityUuid"] = activityUuid;
    }
    if (day != nil) {
        pathParams[@"day"] = day;
    }
    if (time != nil) {
        pathParams[@"time"] = time;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (xMusementCurrency != nil) {
        headerParams[@"X-Musement-Currency"] = xMusementCurrency;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = putTimeslotProducts;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PUT"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGTimeslotProduct*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGTimeslotProduct*)data, error);
                                }
                            }];
}



@end
