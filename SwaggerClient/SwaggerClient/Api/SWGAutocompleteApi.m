#import "SWGAutocompleteApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGAutocompleteResult.h"


@interface SWGAutocompleteApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGAutocompleteApi

NSString* kSWGAutocompleteApiErrorDomain = @"SWGAutocompleteApiErrorDomain";
NSInteger kSWGAutocompleteApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Get autocompletion suggestions for textual search
/// 
///  @param xMusementVersion  (optional)
///
///  @param acceptLanguage  (optional, default to en-US)
///
///  @param sortBy Ordering criteria to apply, prepend `-` for descending order (optional)
///
///  @param coordinates Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
///
///  @param text Text to search (optional)
///
///  @param activityLimit Maximum number of activities to return (optional)
///
///  @param activityOffset Offset for activities to return (optional)
///
///  @param blogPostLimit Maximum number of blog posts to return (optional)
///
///  @param blogPostOffset Offset for blog posts to return (optional)
///
///  @param categoryLimit Maximum number of categories to return (optional)
///
///  @param categoryOffset Offset for categories to return (optional)
///
///  @param cityLimit Maximum number of cities to return (optional)
///
///  @param cityOffset Offset for cities to return (optional)
///
///  @param listLimit Maximum number of lists to return (optional)
///
///  @param listOffset Offset for lists to return (optional)
///
///  @param venueLimit Maximum number of venues to return (optional)
///
///  @param venueOffset Offset for venues to return (optional)
///
///  @returns NSArray<SWGAutocompleteResult>*
///
-(NSURLSessionTask*) autocompleteGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    sortBy: (NSArray<NSString*>*) sortBy
    coordinates: (NSString*) coordinates
    text: (NSString*) text
    activityLimit: (NSNumber*) activityLimit
    activityOffset: (NSNumber*) activityOffset
    blogPostLimit: (NSNumber*) blogPostLimit
    blogPostOffset: (NSNumber*) blogPostOffset
    categoryLimit: (NSNumber*) categoryLimit
    categoryOffset: (NSNumber*) categoryOffset
    cityLimit: (NSNumber*) cityLimit
    cityOffset: (NSNumber*) cityOffset
    listLimit: (NSNumber*) listLimit
    listOffset: (NSNumber*) listOffset
    venueLimit: (NSNumber*) venueLimit
    venueOffset: (NSNumber*) venueOffset
    completionHandler: (void (^)(NSArray<SWGAutocompleteResult>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/autocomplete"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (sortBy != nil) {
        queryParams[@"sort_by"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sortBy format: @"csv"];
    }
    if (coordinates != nil) {
        queryParams[@"coordinates"] = coordinates;
    }
    if (text != nil) {
        queryParams[@"text"] = text;
    }
    if (activityLimit != nil) {
        queryParams[@"activity_limit"] = activityLimit;
    }
    if (activityOffset != nil) {
        queryParams[@"activity_offset"] = activityOffset;
    }
    if (blogPostLimit != nil) {
        queryParams[@"blog_post_limit"] = blogPostLimit;
    }
    if (blogPostOffset != nil) {
        queryParams[@"blog_post_offset"] = blogPostOffset;
    }
    if (categoryLimit != nil) {
        queryParams[@"category_limit"] = categoryLimit;
    }
    if (categoryOffset != nil) {
        queryParams[@"category_offset"] = categoryOffset;
    }
    if (cityLimit != nil) {
        queryParams[@"city_limit"] = cityLimit;
    }
    if (cityOffset != nil) {
        queryParams[@"city_offset"] = cityOffset;
    }
    if (listLimit != nil) {
        queryParams[@"list_limit"] = listLimit;
    }
    if (listOffset != nil) {
        queryParams[@"list_offset"] = listOffset;
    }
    if (venueLimit != nil) {
        queryParams[@"venue_limit"] = venueLimit;
    }
    if (venueOffset != nil) {
        queryParams[@"venue_offset"] = venueOffset;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (xMusementVersion != nil) {
        headerParams[@"X-Musement-Version"] = xMusementVersion;
    }
    if (acceptLanguage != nil) {
        headerParams[@"Accept-Language"] = acceptLanguage;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"application/xml"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json", @"application/xml"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGAutocompleteResult>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGAutocompleteResult>*)data, error);
                                }
                            }];
}



@end
