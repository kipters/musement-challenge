# SWGPatchCustomerGuestCart

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**musementNewsletter** | **NSString*** | Subscribe to Musement&#39;s newsletter ? | 
**thirdpartyNewsletter** | **NSString*** | Subscribe to thirdparty&#39;s newsletter ? | 
**city** | **NSString*** | City | [optional] 
**address** | **NSString*** | Address | [optional] 
**zipcode** | **NSString*** | Zip code | [optional] 
**taxId** | **NSString*** | Tax identifier | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


