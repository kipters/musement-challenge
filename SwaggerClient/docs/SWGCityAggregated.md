# SWGCityAggregated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventCount** | **NSNumber*** |  | [optional] 
**listCount** | **NSNumber*** |  | [optional] 
**venueCount** | **NSNumber*** |  | [optional] 
**showInPopular** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 
**top** | **NSNumber*** | Indicate if the event is a Musement &#39;top&#39; event | [optional] 
**hasConcierge** | **NSNumber*** | True for cities with Musement&#39;s concierge service active | [optional] 
**name** | **NSString*** | City name | [optional] 
**code** | **NSString*** | City code. Do not depends on locale | [optional] 
**content** | **NSString*** | Description | [optional] 
**more** | **NSString*** |  | [optional] 
**weight** | **NSNumber*** |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**country** | [**SWGCountry***](SWGCountry.md) |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**eventsCount** | **NSNumber*** | Number of active events | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


