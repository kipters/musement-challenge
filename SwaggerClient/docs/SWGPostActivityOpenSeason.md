# SWGPostActivityOpenSeason

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSString*** | Activity title - Internal name not exposed to the customer | [optional] 
**featureCode** | **NSString*** | Price feature code | Get them using GET /suppliers/me/price-features | [optional] 
**validity** | **NSString*** | Validity period for the ticket in ISO-8601 format. Days, hours and minutes are accepted | [optional] 
**validUntil** | **NSString*** | Last day the ticket can be used | [optional] 
**prices** | [**NSArray&lt;SWGPostActivitySeasonSetupPrice&gt;***](SWGPostActivitySeasonSetupPrice.md) | Last day the ticket can be used | [optional] 
**languages** | **NSArray&lt;NSString*&gt;*** | A list of languages the activity is available in | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


