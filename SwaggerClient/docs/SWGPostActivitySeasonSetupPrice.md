# SWGPostActivitySeasonSetupPrice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**holderCode** | **NSString*** | Ticket holder code | Get them using GET /suppliers/me/ticket-holders | [optional] 
**ageInfo** | **NSString*** | Extra age info related to the holder | [optional] 
**supplierPrice** | **NSString*** | Activity price we pay to the supplier | [optional] 
**retailPriceSupplier** | **NSString*** | Activity retail price for the supplier | [optional] 
**currency** | **NSString*** | Currency | Use &#39;code&#39; field from GET /api/v3/currencies | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


