# SWGAutocompleteResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**NSArray&lt;SWGAutocompleteItem&gt;***](SWGAutocompleteItem.md) |  | [optional] 
**type** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


