# SWGContactFormApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidContactFormGet**](SWGContactFormApi.md#activitiesactivityuuidcontactformget) | **GET** /activities/{activityUuid}/contact-form | Get activity&#39;s contact form
[**activitiesActivityUuidContactRequestsPost**](SWGContactFormApi.md#activitiesactivityuuidcontactrequestspost) | **POST** /activities/{activityUuid}/contact-requests | Send a contact request for the activity.


# **activitiesActivityUuidContactFormGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidContactFormGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCategory* output, NSError* error)) handler;
```

Get activity's contact form

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGContactFormApi*apiInstance = [[SWGContactFormApi alloc] init];

// Get activity's contact form
[apiInstance activitiesActivityUuidContactFormGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGContactFormApi->activitiesActivityUuidContactFormGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCategory***](SWGCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidContactRequestsPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidContactRequestsPostWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Send a contact request for the activity.

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGContactFormApi*apiInstance = [[SWGContactFormApi alloc] init];

// Send a contact request for the activity.
[apiInstance activitiesActivityUuidContactRequestsPostWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGContactFormApi->activitiesActivityUuidContactRequestsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

