# SWGTicketCodeType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoding** | **NSString*** | Ticket encoding type | [optional] 
**status** | **NSString*** | Status of the ticket code | [optional] 
**customText** | **NSString*** | Custom text | [optional] 
**codes** | **NSArray&lt;NSString*&gt;*** | Array of ticket codes to be added | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


