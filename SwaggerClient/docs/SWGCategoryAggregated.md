# SWGCategoryAggregated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventCount** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**level** | **NSString*** |  | [optional] 
**code** | **NSString*** |  | [optional] 
**eventImageUrl** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


