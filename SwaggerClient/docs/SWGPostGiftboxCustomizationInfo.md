# SWGPostGiftboxCustomizationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keepSecret** | **NSNumber*** | Keep the giftbox secret | [optional] 
**donorName** | **NSString*** | Donor name | [optional] 
**message** | **NSString*** | Personal message for the receiver | [optional] 
**pictureUrl** | **NSString*** | Cover image url | [optional] 
**recipientName** | **NSString*** | Recipient name | [optional] 
**recipientEmail** | **NSString*** | Recipient email address | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


