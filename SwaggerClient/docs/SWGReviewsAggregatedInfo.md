# SWGReviewsAggregatedInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_1** | **NSNumber*** |  | [optional] 
**_2** | **NSNumber*** |  | [optional] 
**_3** | **NSNumber*** |  | [optional] 
**_4** | **NSNumber*** |  | [optional] 
**_5** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


