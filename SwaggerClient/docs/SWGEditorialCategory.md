# SWGEditorialCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**imageUrl** | **NSString*** |  | [optional] 
**active** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**subTitle** | **NSString*** |  | [optional] 
**content** | **NSString*** |  | [optional] 
**slug** | **NSString*** |  | [optional] 
**minEventsForFiltering** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


