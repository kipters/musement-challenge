# SWGFeedbackRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nonce** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 
**event** | [**SWGEvent***](SWGEvent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


