# SWGSupplierOrderApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersMeOrderItemsOrderItemUuidPatch**](SWGSupplierOrderApi.md#suppliersmeorderitemsorderitemuuidpatch) | **PATCH** /suppliers/me/order-items/{orderItemUuid} | Update order item for the logged in supplier
[**suppliersMeOrdersGet**](SWGSupplierOrderApi.md#suppliersmeordersget) | **GET** /suppliers/me/orders | Get orders for the logged in supplier
[**suppliersSupplierUuidOrderItemsOrderItemUuidPatch**](SWGSupplierOrderApi.md#supplierssupplieruuidorderitemsorderitemuuidpatch) | **PATCH** /suppliers/{supplierUuid}/order-items/{orderItemUuid} | Update order item for a particular supplier
[**suppliersSupplierUuidOrdersGet**](SWGSupplierOrderApi.md#supplierssupplieruuidordersget) | **GET** /suppliers/{supplierUuid}/orders | Get orders for a particular supplier


# **suppliersMeOrderItemsOrderItemUuidPatch**
```objc
-(NSURLSessionTask*) suppliersMeOrderItemsOrderItemUuidPatchWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler;
```

Update order item for the logged in supplier

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierOrderApi*apiInstance = [[SWGSupplierOrderApi alloc] init];

// Update order item for the logged in supplier
[apiInstance suppliersMeOrderItemsOrderItemUuidPatchWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGOrder>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierOrderApi->suppliersMeOrderItemsOrderItemUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGOrder>***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersMeOrdersGet**
```objc
-(NSURLSessionTask*) suppliersMeOrdersGetWithOffset: (NSNumber*) offset
    page: (NSNumber*) page
    limit: (NSNumber*) limit
    withTicketStatus: (NSArray<NSString*>*) withTicketStatus
        completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler;
```

Get orders for the logged in supplier

### Example 
```objc

NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSArray<NSString*>* withTicketStatus = @[@"withTicketStatus_example"]; // Filter, include only results with given ticket statuses (optional)

SWGSupplierOrderApi*apiInstance = [[SWGSupplierOrderApi alloc] init];

// Get orders for the logged in supplier
[apiInstance suppliersMeOrdersGetWithOffset:offset
              page:page
              limit:limit
              withTicketStatus:withTicketStatus
          completionHandler: ^(NSArray<SWGOrder>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierOrderApi->suppliersMeOrdersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **withTicketStatus** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results with given ticket statuses | [optional] 

### Return type

[**NSArray<SWGOrder>***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidOrderItemsOrderItemUuidPatch**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidOrderItemsOrderItemUuidPatchWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler;
```

Update order item for a particular supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierOrderApi*apiInstance = [[SWGSupplierOrderApi alloc] init];

// Update order item for a particular supplier
[apiInstance suppliersSupplierUuidOrderItemsOrderItemUuidPatchWithSupplierUuid:supplierUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGOrder>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierOrderApi->suppliersSupplierUuidOrderItemsOrderItemUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGOrder>***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidOrdersGet**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidOrdersGetWithSupplierUuid: (NSString*) supplierUuid
    offset: (NSNumber*) offset
    page: (NSNumber*) page
    limit: (NSNumber*) limit
    withTicketStatus: (NSArray<NSString*>*) withTicketStatus
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    identifier: (NSString*) identifier
    customer: (NSString*) customer
    activityTitle: (NSString*) activityTitle
    activityUuid: (NSString*) activityUuid
    activityDateFrom: (NSString*) activityDateFrom
    activityDateTo: (NSString*) activityDateTo
        completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler;
```

Get orders for a particular supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSArray<NSString*>* withTicketStatus = @[@"withTicketStatus_example"]; // Filter, include only results with given ticket statuses (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSString* identifier = @"identifier_example"; // Internal Musement order identifier | Use format: MUS12345 (optional)
NSString* customer = @"customer_example"; // Customer name (both first name & last name) (optional)
NSString* activityTitle = @"activityTitle_example"; // Activity title (optional)
NSString* activityUuid = @"activityUuid_example"; // Activity UUID (optional)
NSString* activityDateFrom = @"activityDateFrom_example"; // Start date | Use format: YYYY-MM-DD (optional)
NSString* activityDateTo = @"activityDateTo_example"; // End date | Use format: YYYY-MM-DD (optional)

SWGSupplierOrderApi*apiInstance = [[SWGSupplierOrderApi alloc] init];

// Get orders for a particular supplier
[apiInstance suppliersSupplierUuidOrdersGetWithSupplierUuid:supplierUuid
              offset:offset
              page:page
              limit:limit
              withTicketStatus:withTicketStatus
              dateFrom:dateFrom
              dateTo:dateTo
              identifier:identifier
              customer:customer
              activityTitle:activityTitle
              activityUuid:activityUuid
              activityDateFrom:activityDateFrom
              activityDateTo:activityDateTo
          completionHandler: ^(NSArray<SWGOrder>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierOrderApi->suppliersSupplierUuidOrdersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **withTicketStatus** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results with given ticket statuses | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **identifier** | **NSString***| Internal Musement order identifier | Use format: MUS12345 | [optional] 
 **customer** | **NSString***| Customer name (both first name &amp; last name) | [optional] 
 **activityTitle** | **NSString***| Activity title | [optional] 
 **activityUuid** | **NSString***| Activity UUID | [optional] 
 **activityDateFrom** | **NSString***| Start date | Use format: YYYY-MM-DD | [optional] 
 **activityDateTo** | **NSString***| End date | Use format: YYYY-MM-DD | [optional] 

### Return type

[**NSArray<SWGOrder>***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

