# SWGOrderApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customersMeOrdersGet**](SWGOrderApi.md#customersmeordersget) | **GET** /customers/me/orders | Get all orders for customer
[**customersMeOrdersOrderUuidItemsOrderItemUuidGet**](SWGOrderApi.md#customersmeordersorderuuiditemsorderitemuuidget) | **GET** /customers/me/orders/{orderUuid}/items/{orderItemUuid} | Get the details for a specific ticket
[**ordersOrderUuidGet**](SWGOrderApi.md#ordersorderuuidget) | **GET** /orders/{orderUuid} | Get details for an order
[**ordersPost**](SWGOrderApi.md#orderspost) | **POST** /orders | Create an order
[**paymentsStripeStoredSourceInfoGet**](SWGOrderApi.md#paymentsstripestoredsourceinfoget) | **GET** /payments/stripe/stored_source_info | Get stored Stripe Source info


# **customersMeOrdersGet**
```objc
-(NSURLSessionTask*) customersMeOrdersGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler;
```

Get all orders for customer

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGOrderApi*apiInstance = [[SWGOrderApi alloc] init];

// Get all orders for customer
[apiInstance customersMeOrdersGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(NSArray<SWGOrder>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGOrderApi->customersMeOrdersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**NSArray<SWGOrder>***](SWGOrder.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMeOrdersOrderUuidItemsOrderItemUuidGet**
```objc
-(NSURLSessionTask*) customersMeOrdersOrderUuidItemsOrderItemUuidGetWithOrderUuid: (NSString*) orderUuid
    orderItemUuid: (NSString*) orderItemUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGOrderItem* output, NSError* error)) handler;
```

Get the details for a specific ticket

### Example 
```objc

NSString* orderUuid = @"orderUuid_example"; // Order identifier
NSString* orderItemUuid = @"orderItemUuid_example"; // Order item identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGOrderApi*apiInstance = [[SWGOrderApi alloc] init];

// Get the details for a specific ticket
[apiInstance customersMeOrdersOrderUuidItemsOrderItemUuidGetWithOrderUuid:orderUuid
              orderItemUuid:orderItemUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGOrderItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGOrderApi->customersMeOrdersOrderUuidItemsOrderItemUuidGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderUuid** | **NSString***| Order identifier | 
 **orderItemUuid** | **NSString***| Order item identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGOrderItem***](SWGOrderItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ordersOrderUuidGet**
```objc
-(NSURLSessionTask*) ordersOrderUuidGetWithOrderUuid: (NSString*) orderUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    secureCode: (NSString*) secureCode
        completionHandler: (void (^)(SWGOrder* output, NSError* error)) handler;
```

Get details for an order

### Example 
```objc

NSString* orderUuid = @"orderUuid_example"; // Order Uuid
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* secureCode = @"secureCode_example"; // Used when you pass Order.id instead of uuid - is base64 encoded Customer lastname (optional)

SWGOrderApi*apiInstance = [[SWGOrderApi alloc] init];

// Get details for an order
[apiInstance ordersOrderUuidGetWithOrderUuid:orderUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              secureCode:secureCode
          completionHandler: ^(SWGOrder* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGOrderApi->ordersOrderUuidGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderUuid** | **NSString***| Order Uuid | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **secureCode** | **NSString***| Used when you pass Order.id instead of uuid - is base64 encoded Customer lastname | [optional] 

### Return type

[**SWGOrder***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ordersPost**
```objc
-(NSURLSessionTask*) ordersPostWithOrder: (SWGPostOrder*) order
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGOrder* output, NSError* error)) handler;
```

Create an order

### Example 
```objc

SWGPostOrder* order = [[SWGPostOrder alloc] init]; // Order information
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGOrderApi*apiInstance = [[SWGOrderApi alloc] init];

// Create an order
[apiInstance ordersPostWithOrder:order
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGOrder* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGOrderApi->ordersPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**SWGPostOrder***](SWGPostOrder.md)| Order information | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGOrder***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **paymentsStripeStoredSourceInfoGet**
```objc
-(NSURLSessionTask*) paymentsStripeStoredSourceInfoGetWithCompletionHandler: 
        (void (^)(NSError* error)) handler;
```

Get stored Stripe Source info

### Example 
```objc


SWGOrderApi*apiInstance = [[SWGOrderApi alloc] init];

// Get stored Stripe Source info
[apiInstance paymentsStripeStoredSourceInfoGetWithCompletionHandler: 
          ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGOrderApi->paymentsStripeStoredSourceInfoGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

