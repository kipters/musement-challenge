# SWGExternalLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**abstract** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**countryName** | **NSString*** |  | [optional] 
**authorName** | **NSString*** |  | [optional] 
**authorUrl** | **NSString*** |  | [optional] 
**authorAvatarImageUrl** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


