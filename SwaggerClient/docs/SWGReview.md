# SWGReview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **NSString*** |  | [optional] 
**rating** | **NSNumber*** |  | [optional] 
**picture** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**body** | **NSString*** |  | [optional] 
**sentAt** | **NSDate*** |  | [optional] 
**approvedAt** | **NSDate*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


