# SWGVerticalAggregated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**active** | **NSNumber*** |  | [optional] 
**code** | **NSString*** |  | [optional] 
**slug** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**metaTitle** | **NSString*** |  | [optional] 
**metaDescription** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**topEvents** | [**NSArray&lt;SWGEvent&gt;***](SWGEvent.md) |  | [optional] 
**relevance** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


