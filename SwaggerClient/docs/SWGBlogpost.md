# SWGBlogpost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**abstract** | **NSString*** |  | [optional] 
**publishDate** | **NSDate*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**backgroundImage** | **NSString*** |  | [optional] 
**backgroundImageCredits** | **NSString*** |  | [optional] 
**category** | [**SWGCategory***](SWGCategory.md) |  | [optional] 
**vertical** | [**SWGVertical***](SWGVertical.md) |  | [optional] 
**categories** | [**NSArray&lt;SWGBlogpostCategory&gt;***](SWGBlogpostCategory.md) |  | [optional] 
**list** | [**SWGMusementList***](SWGMusementList.md) |  | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**venue** | [**SWGVenue***](SWGVenue.md) |  | [optional] 
**author** | [**SWGBlogger***](SWGBlogger.md) |  | [optional] 
**shareImage** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


