# SWGPostTimeslot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **NSString*** | Set the timeslot opened or closed. | 
**availability** | **NSNumber*** | Number of available places. Use -1 if the date is free sales | 
**languages** | **NSArray&lt;NSString*&gt;*** | A list of languages the activity is available in ISO 639-1 format | 
**products** | [**NSArray&lt;SWGPostTimeslotProduct&gt;***](SWGPostTimeslotProduct.md) | A list of products to add | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


