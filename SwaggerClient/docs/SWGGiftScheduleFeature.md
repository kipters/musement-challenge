# SWGGiftScheduleFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**featureCode** | **NSString*** |  | [optional] 
**name** | **NSString*** | Name (label) of Feature. | [optional] 
**_description** | **NSString*** |  | [optional] 
**_default** | **NSNumber*** |  | [optional] 
**products** | [**NSArray&lt;SWGGiftScheduleProduct&gt;***](SWGGiftScheduleProduct.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


