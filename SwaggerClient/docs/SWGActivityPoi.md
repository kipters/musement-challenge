# SWGActivityPoi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**types** | [**NSArray&lt;SWGTranslatedMetadata&gt;***](SWGTranslatedMetadata.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


