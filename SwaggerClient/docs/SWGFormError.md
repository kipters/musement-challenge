# SWGFormError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | [optional] 
**globalErrorMessage** | **NSString*** |  | [optional] 
**errors** | [**NSArray&lt;SWGFormFieldError&gt;***](SWGFormFieldError.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


