# SWGFormTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**SWGExtraCustomerDataEvent***](SWGExtraCustomerDataEvent.md) |  | [optional] 
**passengersInfo** | [**NSArray&lt;SWGFormFieldDefinition&gt;***](SWGFormFieldDefinition.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


