# SWGScheduleSlot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | **NSString*** |  | [optional] 
**languages** | [**NSArray&lt;SWGTranslatedMetadata&gt;***](SWGTranslatedMetadata.md) |  | [optional] 
**products** | [**NSArray&lt;SWGScheduleProduct&gt;***](SWGScheduleProduct.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


