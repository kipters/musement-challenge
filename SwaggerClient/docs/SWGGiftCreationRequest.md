# SWGGiftCreationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventId** | **NSString*** | Uuid or id of Event (Activity) | [optional] 
**donorName** | **NSString*** |  | [optional] 
**message** | **NSString*** |  | [optional] 
**pictureUrl** | **NSString*** |  | [optional] 
**print** | **NSNumber*** |  | [optional] 
**recipientEmail** | **NSString*** | Optional if print is true. | [optional] 
**recipientName** | **NSString*** |  | [optional] 
**featureCode** | **NSString*** |  | [optional] 
**productsWithQuantities** | [**NSArray&lt;SWGGiftCreationProductWithQuantity&gt;***](SWGGiftCreationProductWithQuantity.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


