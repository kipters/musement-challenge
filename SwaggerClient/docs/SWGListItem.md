# SWGListItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**publicType** | **NSString*** | List item public type | 
**title** | **NSString*** | Item title | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


