# SWGOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **NSString*** | Order unique internal identifier | [optional] 
**uuid** | **NSString*** | Order unique identifier | [optional] 
**date** | **NSDate*** |  | [optional] 
**status** | **NSString*** |  | [optional] 
**tickets** | [**NSArray&lt;SWGTicket&gt;***](SWGTicket.md) |  | [optional] 
**trustpilotUrl** | **NSString*** |  | [optional] 
**customer** | [**SWGCustomer***](SWGCustomer.md) |  | [optional] 
**items** | [**NSArray&lt;SWGOrderItem&gt;***](SWGOrderItem.md) |  | [optional] 
**totalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**supplierTotalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**affiliateId** | **NSNumber*** |  | [optional] 
**promoCodes** | [**NSArray&lt;SWGPromoCode&gt;***](SWGPromoCode.md) |  | [optional] 
**extraData** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


