# SWGPaypalApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsPaypalPaymentPost**](SWGPaypalApi.md#paymentspaypalpaymentpost) | **POST** /payments/paypal/payment | Pay an Order.


# **paymentsPaypalPaymentPost**
```objc
-(NSURLSessionTask*) paymentsPaypalPaymentPostWithPaypalPayment: (SWGPaypalPaymentRequest*) paypalPayment
        completionHandler: (void (^)(SWGPaypalSuccessfulPayment* output, NSError* error)) handler;
```

Pay an Order.

Pay an Order using Paypal Payment Id.

### Example 
```objc

SWGPaypalPaymentRequest* paypalPayment = [[SWGPaypalPaymentRequest alloc] init]; // Paypal payment info request

SWGPaypalApi*apiInstance = [[SWGPaypalApi alloc] init];

// Pay an Order.
[apiInstance paymentsPaypalPaymentPostWithPaypalPayment:paypalPayment
          completionHandler: ^(SWGPaypalSuccessfulPayment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPaypalApi->paymentsPaypalPaymentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paypalPayment** | [**SWGPaypalPaymentRequest***](SWGPaypalPaymentRequest.md)| Paypal payment info request | 

### Return type

[**SWGPaypalSuccessfulPayment***](SWGPaypalSuccessfulPayment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

