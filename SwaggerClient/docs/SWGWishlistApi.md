# SWGWishlistApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**wishesCodeGet**](SWGWishlistApi.md#wishescodeget) | **GET** /wishes/{code} | Get a wish


# **wishesCodeGet**
```objc
-(NSURLSessionTask*) wishesCodeGetWithCode: (NSString*) code
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGWish* output, NSError* error)) handler;
```

Get a wish

### Example 
```objc

NSString* code = @"code_example"; // Wish identifier in the form '[wish-id]-[wishtype]'
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGWishlistApi*apiInstance = [[SWGWishlistApi alloc] init];

// Get a wish
[apiInstance wishesCodeGetWithCode:code
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGWish* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWishlistApi->wishesCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **NSString***| Wish identifier in the form &#39;[wish-id]-[wishtype]&#39; | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGWish***](SWGWish.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

