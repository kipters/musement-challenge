# SWGBlogpostBlockEmbeddedHtml

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**embeddedCode** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 
**_id** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


