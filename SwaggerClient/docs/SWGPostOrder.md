# SWGPostOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cartId** | **NSNumber*** | Cart ID - Deprecated, use uuid | [optional] 
**cartUuid** | **NSString*** | Cart UUID | [optional] 
**emailNotification** | **NSString*** | Define which notifications are sent by email. - ALL: All email are sent - TO-CUSTOMER: Only emails to the customer are sent - NONE: No emails are sent | [optional] 
**extraData** | **NSString*** | Property where the client application can save extra info about the order. The schema and type of data saved are decided by the client app. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


