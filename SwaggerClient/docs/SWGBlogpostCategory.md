# SWGBlogpostCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | [optional] 
**code** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**slug** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**metaTitle** | **NSString*** |  | [optional] 
**backgroundImage** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


