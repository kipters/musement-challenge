# SWGActivityRefundPoliciesApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidDelete**](SWGActivityRefundPoliciesApi.md#activitiesactivityuuidrefundpoliciesactivityrefundpolicyuuiddelete) | **DELETE** /activities/{activityUuid}/refund-policies/{activityRefundPolicyUuid} | Delete a refund policy for the activity
[**activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidPut**](SWGActivityRefundPoliciesApi.md#activitiesactivityuuidrefundpoliciesactivityrefundpolicyuuidput) | **PUT** /activities/{activityUuid}/refund-policies/{activityRefundPolicyUuid} | Update a refund policy for the activity
[**activitiesActivityUuidRefundPoliciesGet**](SWGActivityRefundPoliciesApi.md#activitiesactivityuuidrefundpoliciesget) | **GET** /activities/{{activityUuid}}/refund-policies | Return all of the activity refund policies
[**activitiesActivityUuidRefundPoliciesPost**](SWGActivityRefundPoliciesApi.md#activitiesactivityuuidrefundpoliciespost) | **POST** /activities/{activityUuid}/refund-policies | Create a refund policy for the activity


# **activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidDelete**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidDeleteWithActivityUuid: (NSString*) activityUuid
    activityRefundPolicyUuid: (NSString*) activityRefundPolicyUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a refund policy for the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* activityRefundPolicyUuid = @"activityRefundPolicyUuid_example"; // Activity refund policy identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGActivityRefundPoliciesApi*apiInstance = [[SWGActivityRefundPoliciesApi alloc] init];

// Delete a refund policy for the activity
[apiInstance activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidDeleteWithActivityUuid:activityUuid
              activityRefundPolicyUuid:activityRefundPolicyUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityRefundPoliciesApi->activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidDelete: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **activityRefundPolicyUuid** | **NSString***| Activity refund policy identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidPut**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidPutWithActivityUuid: (NSString*) activityUuid
    activityRefundPolicyUuid: (NSString*) activityRefundPolicyUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(NSError* error)) handler;
```

Update a refund policy for the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* activityRefundPolicyUuid = @"activityRefundPolicyUuid_example"; // Activity refund policy identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGActivityRefundPoliciesApi*apiInstance = [[SWGActivityRefundPoliciesApi alloc] init];

// Update a refund policy for the activity
[apiInstance activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidPutWithActivityUuid:activityUuid
              activityRefundPolicyUuid:activityRefundPolicyUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityRefundPoliciesApi->activitiesActivityUuidRefundPoliciesActivityRefundPolicyUuidPut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **activityRefundPolicyUuid** | **NSString***| Activity refund policy identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidRefundPoliciesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRefundPoliciesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGActivityRefundPolicy* output, NSError* error)) handler;
```

Return all of the activity refund policies

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGActivityRefundPoliciesApi*apiInstance = [[SWGActivityRefundPoliciesApi alloc] init];

// Return all of the activity refund policies
[apiInstance activitiesActivityUuidRefundPoliciesGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGActivityRefundPolicy* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityRefundPoliciesApi->activitiesActivityUuidRefundPoliciesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGActivityRefundPolicy***](SWGActivityRefundPolicy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidRefundPoliciesPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRefundPoliciesPostWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(NSError* error)) handler;
```

Create a refund policy for the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGActivityRefundPoliciesApi*apiInstance = [[SWGActivityRefundPoliciesApi alloc] init];

// Create a refund policy for the activity
[apiInstance activitiesActivityUuidRefundPoliciesPostWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityRefundPoliciesApi->activitiesActivityUuidRefundPoliciesPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

