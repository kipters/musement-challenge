# SWGPostActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **NSString*** | Activity type | Can be &#39;ACTIVITY&#39; or &#39;TOUR&#39; | [optional] 
**sellerGateway** | **NSString*** | Seller gateway. | Get them using GET /seller-gateways | [optional] 
**vertical** | **NSNumber*** | Vertical. | [optional] 
**categories** | **NSArray&lt;NSNumber*&gt;*** | Categories, related to the vertical, for the activity. | [optional] 
**highlights** | **NSArray&lt;NSString*&gt;*** | Highlights, for the activity | Get them using GET /activity-taxonomies/highlights. | [optional] 
**duration** | **NSString*** | Activity duration as ISO-8601 period. Only day, hours and minutes are accepted. | [optional] 
**inclusions** | **NSArray&lt;NSString*&gt;*** | What&#39;s included ? | Get them using GET /activity-taxonomies/features | [optional] 
**exclusions** | **NSArray&lt;NSString*&gt;*** | What&#39;s excluded | Get them using GET /activity-taxonomies/features | [optional] 
**features** | **NSArray&lt;NSString*&gt;*** | Features | Get them using GET /features | [optional] 
**services** | **NSArray&lt;NSString*&gt;*** | Services | Get them using GET /services | [optional] 
**mobileTicketIsAccepted** | **NSNumber*** | Is the mobile ticket accepted ? | [optional] 
**cutoffTime** | **NSString*** | Cutoff time as ISO-8601 period. Only days, hours and minutes are accepted. | [optional] 
**maxConfirmationTime** | **NSString*** | Max period of time the supplier need to confirm the reservation. Only days, hours and minutes are accepted as ISO-8601 period. | [optional] 
**partnerInternalCode** | **NSString*** | Internal code of the partner | [optional] 
**partner** | **NSString*** | Partner UUID | [optional] 
**reduction** | **NSString*** | A list of people who can have reduction. | [optional] 
**city** | **NSNumber*** | City identifier. Get the value of id from the call GET /api/v3/cities | [optional] 
**venues** | **NSArray&lt;NSNumber*&gt;*** | Venues identifier. Get the value of id from the call GET /api/v3/venues | [optional] 
**status** | **NSString*** |   * ONLINE: The activity is visible on musement.com and it can be booked  * ARCHIVED: The activity is visible on musement.com but it cannot be booked  * INACTIVE: The activity is not visible on musement.com | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


