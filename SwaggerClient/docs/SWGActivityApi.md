# SWGActivityApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidBundlesGet**](SWGActivityApi.md#activitiesactivityuuidbundlesget) | **GET** /activities/{activityUuid}/bundles | Get bundles related to the activity
[**activitiesActivityUuidCommentsGet**](SWGActivityApi.md#activitiesactivityuuidcommentsget) | **GET** /activities/{activityUuid}/comments | Get all comments for the activity
[**activitiesActivityUuidContentsLocaleCodeGet**](SWGActivityApi.md#activitiesactivityuuidcontentslocalecodeget) | **GET** /activities/{activityUuid}/contents/{localeCode} | Get activity content for a specific locale
[**activitiesActivityUuidGet**](SWGActivityApi.md#activitiesactivityuuidget) | **GET** /activities/{activityUuid} | Get an activity by unique identifier
[**activitiesActivityUuidPatch**](SWGActivityApi.md#activitiesactivityuuidpatch) | **PATCH** /activities/{activityUuid} | Update an activity
[**activitiesActivityUuidRegionsGet**](SWGActivityApi.md#activitiesactivityuuidregionsget) | **GET** /activities/{activityUuid}/regions | Get regions for the Activity
[**activitiesActivityUuidRelatedActivitiesGet**](SWGActivityApi.md#activitiesactivityuuidrelatedactivitiesget) | **GET** /activities/{activityUuid}/related-activities | Get related activities
[**activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderCodePatch**](SWGActivityApi.md#activitiesactivityuuidticketcodespricetagfeaturepricetagholdercodepatch) | **PATCH** /activities/{activityUuid}/ticket-codes/{priceTagFeature}/{priceTagHolder}/{code} | Update selected ticket code
[**activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderGet**](SWGActivityApi.md#activitiesactivityuuidticketcodespricetagfeaturepricetagholderget) | **GET** /activities/{activityUuid}/ticket-codes/{priceTagFeature}/{priceTagHolder} | Get available ticket code for an event
[**activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderPost**](SWGActivityApi.md#activitiesactivityuuidticketcodespricetagfeaturepricetagholderpost) | **POST** /activities/{activityUuid}/ticket-codes/{priceTagFeature}/{priceTagHolder} | Add new ticket codes to an event
[**activitiesGet**](SWGActivityApi.md#activitiesget) | **GET** /activities | Activities
[**activitiesPost**](SWGActivityApi.md#activitiespost) | **POST** /activities | Create an activity
[**activitiesRelatedGet**](SWGActivityApi.md#activitiesrelatedget) | **GET** /activities-related | Activities related search entrypoint
[**activityTaxonomiesActivityTaxonomyTypeGet**](SWGActivityApi.md#activitytaxonomiesactivitytaxonomytypeget) | **GET** /activity-taxonomies/{activityTaxonomyType} | Get all taxonomies for the required type
[**citiesCityIdActivitiesGet**](SWGActivityApi.md#citiescityidactivitiesget) | **GET** /cities/{cityId}/activities | Search activities within a given city
[**citiesCityIdActivitiesHappeningGet**](SWGActivityApi.md#citiescityidactivitieshappeningget) | **GET** /cities/{cityId}/activities/{happening} | Search activities within a given city happening today or tomorrow
[**countriesCountryIdActivitiesGet**](SWGActivityApi.md#countriescountryidactivitiesget) | **GET** /countries/{countryId}/activities | Get all activities for a country
[**editorialCategoriesEditorialcategoryIdActivitiesGet**](SWGActivityApi.md#editorialcategorieseditorialcategoryidactivitiesget) | **GET** /editorial-categories/{editorialcategoryId}/activities | Search activities within a given editorial category
[**eventsGet**](SWGActivityApi.md#eventsget) | **GET** /events | Get events
[**eventsSearchExtendedGet**](SWGActivityApi.md#eventssearchextendedget) | **GET** /events/search-extended | Deprecated API for search. Use GET /activities
[**eventsSearchGet**](SWGActivityApi.md#eventssearchget) | **GET** /events/search | Deprecated API for search - Use GET /activities
[**venuesVenueIdActivitiesGet**](SWGActivityApi.md#venuesvenueidactivitiesget) | **GET** /venues/{venueId}/activities | Search activities within a given venue


# **activitiesActivityUuidBundlesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidBundlesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGBundle* output, NSError* error)) handler;
```

Get bundles related to the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get bundles related to the activity
[apiInstance activitiesActivityUuidBundlesGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGBundle* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidBundlesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGBundle***](SWGBundle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidCommentsGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidCommentsGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    locale: (NSString*) locale
    minRating: (NSNumber*) minRating
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(NSArray<SWGComment>* output, NSError* error)) handler;
```

Get all comments for the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* locale = @"locale_example"; // Locale code (optional)
NSNumber* minRating = @56; // Minimum rating (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get all comments for the activity
[apiInstance activitiesActivityUuidCommentsGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              locale:locale
              minRating:minRating
              limit:limit
              offset:offset
          completionHandler: ^(NSArray<SWGComment>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidCommentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **locale** | **NSString***| Locale code | [optional] 
 **minRating** | **NSNumber***| Minimum rating | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

[**NSArray<SWGComment>***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidContentsLocaleCodeGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidContentsLocaleCodeGetWithActivityUuid: (NSString*) activityUuid
    localeCode: (NSString*) localeCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGActivityContent* output, NSError* error)) handler;
```

Get activity content for a specific locale

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* localeCode = @"localeCode_example"; // Locale code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get activity content for a specific locale
[apiInstance activitiesActivityUuidContentsLocaleCodeGetWithActivityUuid:activityUuid
              localeCode:localeCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGActivityContent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidContentsLocaleCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **localeCode** | **NSString***| Locale code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGActivityContent***](SWGActivityContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGEvent* output, NSError* error)) handler;
```

Get an activity by unique identifier

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get an activity by unique identifier
[apiInstance activitiesActivityUuidGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGEvent***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidPatch**
```objc
-(NSURLSessionTask*) activitiesActivityUuidPatchWithActivityUuid: (NSString*) activityUuid
    patchActivity: (SWGPatchActivity*) patchActivity
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGEvent* output, NSError* error)) handler;
```

Update an activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPatchActivity* patchActivity = [[SWGPatchActivity alloc] init]; // Activity patch request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Update an activity
[apiInstance activitiesActivityUuidPatchWithActivityUuid:activityUuid
              patchActivity:patchActivity
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **patchActivity** | [**SWGPatchActivity***](SWGPatchActivity.md)| Activity patch request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGEvent***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidRegionsGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRegionsGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get regions for the Activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get regions for the Activity
[apiInstance activitiesActivityUuidRegionsGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidRelatedActivitiesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRelatedActivitiesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Get related activities

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get related activities
[apiInstance activitiesActivityUuidRelatedActivitiesGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidRelatedActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderCodePatch**
```objc
-(NSURLSessionTask*) activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderCodePatchWithActivityUuid: (NSString*) activityUuid
    priceTagFeature: (NSString*) priceTagFeature
    priceTagHolder: (NSString*) priceTagHolder
    code: (NSString*) code
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Update selected ticket code

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* priceTagFeature = @"priceTagFeature_example"; // PriceTag feature code
NSString* priceTagHolder = @"priceTagHolder_example"; // PriceTag holder code
NSString* code = @"code_example"; // Ticket code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Update selected ticket code
[apiInstance activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderCodePatchWithActivityUuid:activityUuid
              priceTagFeature:priceTagFeature
              priceTagHolder:priceTagHolder
              code:code
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderCodePatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **priceTagFeature** | **NSString***| PriceTag feature code | 
 **priceTagHolder** | **NSString***| PriceTag holder code | 
 **code** | **NSString***| Ticket code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderGetWithActivityUuid: (NSString*) activityUuid
    priceTagFeature: (NSString*) priceTagFeature
    priceTagHolder: (NSString*) priceTagHolder
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Get available ticket code for an event

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* priceTagFeature = @"priceTagFeature_example"; // PriceTag feature code
NSString* priceTagHolder = @"priceTagHolder_example"; // PriceTag holder code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get available ticket code for an event
[apiInstance activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderGetWithActivityUuid:activityUuid
              priceTagFeature:priceTagFeature
              priceTagHolder:priceTagHolder
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **priceTagFeature** | **NSString***| PriceTag feature code | 
 **priceTagHolder** | **NSString***| PriceTag holder code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderPostWithActivityUuid: (NSString*) activityUuid
    priceTagFeature: (NSString*) priceTagFeature
    priceTagHolder: (NSString*) priceTagHolder
    ticketCodeProduct: (SWGTicketCodeType*) ticketCodeProduct
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Add new ticket codes to an event

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* priceTagFeature = @"priceTagFeature_example"; // PriceTag feature code
NSString* priceTagHolder = @"priceTagHolder_example"; // PriceTag holder code
SWGTicketCodeType* ticketCodeProduct = [[SWGTicketCodeType alloc] init]; // Ticket code information
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Add new ticket codes to an event
[apiInstance activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderPostWithActivityUuid:activityUuid
              priceTagFeature:priceTagFeature
              priceTagHolder:priceTagHolder
              ticketCodeProduct:ticketCodeProduct
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesActivityUuidTicketCodesPriceTagFeaturePriceTagHolderPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **priceTagFeature** | **NSString***| PriceTag feature code | 
 **priceTagHolder** | **NSString***| PriceTag holder code | 
 **ticketCodeProduct** | [**SWGTicketCodeType***](SWGTicketCodeType.md)| Ticket code information | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesGet**
```objc
-(NSURLSessionTask*) activitiesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    text: (NSString*) text
    textOperator: (NSString*) textOperator
    extendOtherLanguages: (NSString*) extendOtherLanguages
    extendContentFields: (NSString*) extendContentFields
    fuzzinessLevel: (NSString*) fuzzinessLevel
    zeroTermsQuery: (NSString*) zeroTermsQuery
    sortBy: (NSArray<NSString*>*) sortBy
    categoryIn: (NSArray<NSString*>*) categoryIn
    cityIn: (NSArray<NSNumber*>*) cityIn
    coordinates: (NSString*) coordinates
    countryIn: (NSArray<NSString*>*) countryIn
    defaultPriceRange: (NSString*) defaultPriceRange
    distance: (NSString*) distance
    featureIn: (NSArray<NSString*>*) featureIn
    temporary: (NSNumber*) temporary
    venueIn: (NSArray<NSNumber*>*) venueIn
    verticalIn: (NSArray<NSString*>*) verticalIn
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(SWGActivitySearchResult* output, NSError* error)) handler;
```

Activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* text = @"text_example"; // Text to search (optional)
NSString* textOperator = @"AUTO"; // Represents text operator. If `AND` results will contains all of the words, if `OR` at least one, if `AUTO` performs a `AND` search first and an `OR` then (optional) (default to AUTO)
NSString* extendOtherLanguages = @"AUTO"; // If set to `YES` will extend the search to other language than the one specified in `Accept-Language` header, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* extendContentFields = @"AUTO"; // If set to `YES` will extend the search to other textual field other than the title, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* fuzzinessLevel = @"LEVEL-0"; // Level of fuzziness (optional) (default to LEVEL-0)
NSString* zeroTermsQuery = @"NONE"; // If set to `ALL`, if all of the stop words have been removed, search will be performed, if set to 'NONE' will not (optional) (default to NONE)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Set sorting criteria. Add `-` to invert sorting direction. When `distance` is specified also `coordinates` parameter must be set. (optional)
NSArray<NSString*>* categoryIn = @[@"categoryIn_example"]; // Filter by category | Category code need to be passed (optional)
NSArray<NSNumber*>* cityIn = @[@56]; // Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
NSString* coordinates = @"coordinates_example"; // Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
NSArray<NSString*>* countryIn = @[@"countryIn_example"]; // Filter, include only results from given countries identified by a collection of 2 chars country code (optional)
NSString* defaultPriceRange = @"defaultPriceRange_example"; // Price range as comma separated values, accepts only floats positive or equals to 0, two points precision | Currency is inferred from X-Musement-Currency header | Example: 0.00,34.23 (optional)
NSString* distance = @"distance_example"; // Distance from given coordinates expressed with an integer followed by unit | `KM` for kilometers, `M` for miles | Requires the other parameters called `coordinates` to be set in the same request | Example: 334KM (optional)
NSArray<NSString*>* featureIn = @[@"featureIn_example"]; // Filter, include only results having at least one of the given features identified by a collection of codes (optional)
NSNumber* temporary = @true; // Filter, include results on an temporary flag basis. Accepted values: YES or NOT (optional)
NSArray<NSNumber*>* venueIn = @[@56]; // Filter, include only results from given verticals identified by a collection of ids (optional)
NSArray<NSString*>* verticalIn = @[@"verticalIn_example"]; // Filter, include only results from given verticals identified by a collection of literal codes (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Activities
[apiInstance activitiesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              text:text
              textOperator:textOperator
              extendOtherLanguages:extendOtherLanguages
              extendContentFields:extendContentFields
              fuzzinessLevel:fuzzinessLevel
              zeroTermsQuery:zeroTermsQuery
              sortBy:sortBy
              categoryIn:categoryIn
              cityIn:cityIn
              coordinates:coordinates
              countryIn:countryIn
              defaultPriceRange:defaultPriceRange
              distance:distance
              featureIn:featureIn
              temporary:temporary
              venueIn:venueIn
              verticalIn:verticalIn
              limit:limit
              offset:offset
          completionHandler: ^(SWGActivitySearchResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **text** | **NSString***| Text to search | [optional] 
 **textOperator** | **NSString***| Represents text operator. If &#x60;AND&#x60; results will contains all of the words, if &#x60;OR&#x60; at least one, if &#x60;AUTO&#x60; performs a &#x60;AND&#x60; search first and an &#x60;OR&#x60; then | [optional] [default to AUTO]
 **extendOtherLanguages** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other language than the one specified in &#x60;Accept-Language&#x60; header, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **extendContentFields** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other textual field other than the title, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **fuzzinessLevel** | **NSString***| Level of fuzziness | [optional] [default to LEVEL-0]
 **zeroTermsQuery** | **NSString***| If set to &#x60;ALL&#x60;, if all of the stop words have been removed, search will be performed, if set to &#39;NONE&#39; will not | [optional] [default to NONE]
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Set sorting criteria. Add &#x60;-&#x60; to invert sorting direction. When &#x60;distance&#x60; is specified also &#x60;coordinates&#x60; parameter must be set. | [optional] 
 **categoryIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter by category | Category code need to be passed | [optional] 
 **cityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given cities identified by a collection of ids | [optional] 
 **coordinates** | **NSString***| Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 | [optional] 
 **countryIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results from given countries identified by a collection of 2 chars country code | [optional] 
 **defaultPriceRange** | **NSString***| Price range as comma separated values, accepts only floats positive or equals to 0, two points precision | Currency is inferred from X-Musement-Currency header | Example: 0.00,34.23 | [optional] 
 **distance** | **NSString***| Distance from given coordinates expressed with an integer followed by unit | &#x60;KM&#x60; for kilometers, &#x60;M&#x60; for miles | Requires the other parameters called &#x60;coordinates&#x60; to be set in the same request | Example: 334KM | [optional] 
 **featureIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results having at least one of the given features identified by a collection of codes | [optional] 
 **temporary** | **NSNumber***| Filter, include results on an temporary flag basis. Accepted values: YES or NOT | [optional] 
 **venueIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given verticals identified by a collection of ids | [optional] 
 **verticalIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results from given verticals identified by a collection of literal codes | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

[**SWGActivitySearchResult***](SWGActivitySearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesPost**
```objc
-(NSURLSessionTask*) activitiesPostWithPostActivity: (SWGPostActivity*) postActivity
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGEvent* output, NSError* error)) handler;
```

Create an activity

### Example 
```objc

SWGPostActivity* postActivity = [[SWGPostActivity alloc] init]; // Activity post request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Create an activity
[apiInstance activitiesPostWithPostActivity:postActivity
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postActivity** | [**SWGPostActivity***](SWGPostActivity.md)| Activity post request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGEvent***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesRelatedGet**
```objc
-(NSURLSessionTask*) activitiesRelatedGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    text: (NSString*) text
    textOperator: (NSString*) textOperator
    extendOtherLanguages: (NSString*) extendOtherLanguages
    extendContentFields: (NSString*) extendContentFields
    fuzzinessLevel: (NSString*) fuzzinessLevel
    zeroTermsQuery: (NSString*) zeroTermsQuery
    sortBy: (NSArray<NSString*>*) sortBy
    coordinates: (NSString*) coordinates
    minimumEvents: (NSNumber*) minimumEvents
    cityLimit: (NSNumber*) cityLimit
    cityOffset: (NSNumber*) cityOffset
    listLimit: (NSNumber*) listLimit
    listOffset: (NSNumber*) listOffset
    venueLimit: (NSNumber*) venueLimit
    venueOffset: (NSNumber*) venueOffset
        completionHandler: (void (^)(NSArray<SWGActivityRelatedResult>* output, NSError* error)) handler;
```

Activities related search entrypoint

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* text = @"text_example"; // Text to search (optional)
NSString* textOperator = @"AUTO"; // Represents text operator. If `AND` results will contains all of the words, if `OR` at least one, if `AUTO` performs a `AND` search first and an `OR` then (optional) (default to AUTO)
NSString* extendOtherLanguages = @"AUTO"; // If set to `YES` will extend the search to other language than the one specified in `Accept-Language` header, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* extendContentFields = @"AUTO"; // If set to `YES` will extend the search to other textual field other than the title, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* fuzzinessLevel = @"LEVEL-0"; // Level of fuzziness (optional) (default to LEVEL-0)
NSString* zeroTermsQuery = @"NONE"; // If set to `ALL`, if all of the stop words have been removed, search will be performed, if set to 'NONE' will not (optional) (default to NONE)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria to apply, prepend `-` for descending order (optional)
NSString* coordinates = @"coordinates_example"; // Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
NSNumber* minimumEvents = @56; // Minimum number of active events that must belong to the container subject, default 1 (optional)
NSNumber* cityLimit = @56; // Maximum number of cities to return (optional)
NSNumber* cityOffset = @56; // Offset for cities to return (optional)
NSNumber* listLimit = @56; // Maximum number of lists to return (optional)
NSNumber* listOffset = @56; // Offset for lists to return (optional)
NSNumber* venueLimit = @56; // Maximum number of venues to return (optional)
NSNumber* venueOffset = @56; // Offset for venues to return (optional)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Activities related search entrypoint
[apiInstance activitiesRelatedGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              text:text
              textOperator:textOperator
              extendOtherLanguages:extendOtherLanguages
              extendContentFields:extendContentFields
              fuzzinessLevel:fuzzinessLevel
              zeroTermsQuery:zeroTermsQuery
              sortBy:sortBy
              coordinates:coordinates
              minimumEvents:minimumEvents
              cityLimit:cityLimit
              cityOffset:cityOffset
              listLimit:listLimit
              listOffset:listOffset
              venueLimit:venueLimit
              venueOffset:venueOffset
          completionHandler: ^(NSArray<SWGActivityRelatedResult>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activitiesRelatedGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **text** | **NSString***| Text to search | [optional] 
 **textOperator** | **NSString***| Represents text operator. If &#x60;AND&#x60; results will contains all of the words, if &#x60;OR&#x60; at least one, if &#x60;AUTO&#x60; performs a &#x60;AND&#x60; search first and an &#x60;OR&#x60; then | [optional] [default to AUTO]
 **extendOtherLanguages** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other language than the one specified in &#x60;Accept-Language&#x60; header, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **extendContentFields** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other textual field other than the title, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **fuzzinessLevel** | **NSString***| Level of fuzziness | [optional] [default to LEVEL-0]
 **zeroTermsQuery** | **NSString***| If set to &#x60;ALL&#x60;, if all of the stop words have been removed, search will be performed, if set to &#39;NONE&#39; will not | [optional] [default to NONE]
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria to apply, prepend &#x60;-&#x60; for descending order | [optional] 
 **coordinates** | **NSString***| Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 | [optional] 
 **minimumEvents** | **NSNumber***| Minimum number of active events that must belong to the container subject, default 1 | [optional] 
 **cityLimit** | **NSNumber***| Maximum number of cities to return | [optional] 
 **cityOffset** | **NSNumber***| Offset for cities to return | [optional] 
 **listLimit** | **NSNumber***| Maximum number of lists to return | [optional] 
 **listOffset** | **NSNumber***| Offset for lists to return | [optional] 
 **venueLimit** | **NSNumber***| Maximum number of venues to return | [optional] 
 **venueOffset** | **NSNumber***| Offset for venues to return | [optional] 

### Return type

[**NSArray<SWGActivityRelatedResult>***](SWGActivityRelatedResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activityTaxonomiesActivityTaxonomyTypeGet**
```objc
-(NSURLSessionTask*) activityTaxonomiesActivityTaxonomyTypeGetWithActivityTaxonomyType: (NSString*) activityTaxonomyType
    taxonomyCategoryCode: (NSString*) taxonomyCategoryCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGActivityTaxonomy* output, NSError* error)) handler;
```

Get all taxonomies for the required type

### Example 
```objc

NSString* activityTaxonomyType = @"activityTaxonomyType_example"; // Activity taxonomy type
NSString* taxonomyCategoryCode = @"taxonomyCategoryCode_example"; // Taxonomy category code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get all taxonomies for the required type
[apiInstance activityTaxonomiesActivityTaxonomyTypeGetWithActivityTaxonomyType:activityTaxonomyType
              taxonomyCategoryCode:taxonomyCategoryCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGActivityTaxonomy* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->activityTaxonomiesActivityTaxonomyTypeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityTaxonomyType** | **NSString***| Activity taxonomy type | 
 **taxonomyCategoryCode** | **NSString***| Taxonomy category code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGActivityTaxonomy***](SWGActivityTaxonomy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdActivitiesGet**
```objc
-(NSURLSessionTask*) citiesCityIdActivitiesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    category: (NSNumber*) category
    venue: (NSNumber*) venue
    editorialCategory: (NSNumber*) editorialCategory
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given city

Get all activities for an city. Only the activities that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* venue = @56; // Venue identifier (optional)
NSNumber* editorialCategory = @56; // Editorial category identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"venue-relevance"; // Sorting strategy (optional) (default to venue-relevance)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Search activities within a given city
[apiInstance citiesCityIdActivitiesGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              category:category
              venue:venue
              editorialCategory:editorialCategory
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->citiesCityIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **category** | **NSNumber***| Category identifier | [optional] 
 **venue** | **NSNumber***| Venue identifier | [optional] 
 **editorialCategory** | **NSNumber***| Editorial category identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to venue-relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdActivitiesHappeningGet**
```objc
-(NSURLSessionTask*) citiesCityIdActivitiesHappeningGetWithCityId: (NSNumber*) cityId
    happening: (NSString*) happening
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given city happening today or tomorrow

Some functionality could not have been preserved.

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* happening = @"happening_example"; // Events for today or tomorrow ?
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Search activities within a given city happening today or tomorrow
[apiInstance citiesCityIdActivitiesHappeningGetWithCityId:cityId
              happening:happening
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->citiesCityIdActivitiesHappeningGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **happening** | **NSString***| Events for today or tomorrow ? | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **countriesCountryIdActivitiesGet**
```objc
-(NSURLSessionTask*) countriesCountryIdActivitiesGetWithCountryId: (NSNumber*) countryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    priorityCity: (NSNumber*) priorityCity
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Get all activities for a country

Get all events for a country sorted by relevance. If priority_city is specified the event for that city are returned first'

### Example 
```objc

NSNumber* countryId = @56; // Country identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* priorityCity = @56; // Prioritize results that belong to this city (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get all activities for a country
[apiInstance countriesCountryIdActivitiesGetWithCountryId:countryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              priorityCity:priorityCity
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->countriesCountryIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **NSNumber***| Country identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **priorityCity** | **NSNumber***| Prioritize results that belong to this city | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editorialCategoriesEditorialcategoryIdActivitiesGet**
```objc
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdActivitiesGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    category: (NSNumber*) category
    venue: (NSNumber*) venue
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given editorial category

Get all activities for an editorial category sorted by relevance. Only the events that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* editorialcategoryId = @56; // Editorial category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* city = @56; // City identifier (optional)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* venue = @56; // Venue identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"relevance"; // Sorting strategy (optional) (default to relevance)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Search activities within a given editorial category
[apiInstance editorialCategoriesEditorialcategoryIdActivitiesGetWithEditorialcategoryId:editorialcategoryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              vertical:vertical
              city:city
              category:category
              venue:venue
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->editorialCategoriesEditorialcategoryIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **editorialcategoryId** | **NSNumber***| Editorial category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **city** | **NSNumber***| City identifier | [optional] 
 **category** | **NSNumber***| Category identifier | [optional] 
 **venue** | **NSNumber***| Venue identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsGet**
```objc
-(NSURLSessionTask*) eventsGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    ids: (NSArray<NSNumber*>*) ids
    idIn: (NSArray<NSNumber*>*) idIn
    countryIn: (NSArray<NSNumber*>*) countryIn
    notCountryIn: (NSArray<NSNumber*>*) notCountryIn
    cityIn: (NSArray<NSNumber*>*) cityIn
    notCityIn: (NSArray<NSNumber*>*) notCityIn
    categoryIn: (NSArray<NSNumber*>*) categoryIn
    verticalIn: (NSArray<NSNumber*>*) verticalIn
    discounted: (NSNumber*) discounted
    temporary: (NSString*) temporary
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
    page: (NSNumber*) page
    sortBy: (NSArray<NSString*>*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Get events

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSArray<NSNumber*>* ids = @[@56]; // Given events will always be returned (will not help achieve the limit quota) (optional)
NSArray<NSNumber*>* idIn = @[@56]; // Filter, include only results having the given ids (optional)
NSArray<NSNumber*>* countryIn = @[@56]; // Filter, include only results from given countries identified by a collection of ids (optional)
NSArray<NSNumber*>* notCountryIn = @[@56]; // Filter, exclude results from given countries identified by a collection of ids (optional)
NSArray<NSNumber*>* cityIn = @[@56]; // Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
NSArray<NSNumber*>* notCityIn = @[@56]; // Filter, exclude results from given cities identified by a collection of ids (optional)
NSArray<NSNumber*>* categoryIn = @[@56]; // Filter, include only results from at least one of the given categories identified by a collection of ids (optional)
NSArray<NSNumber*>* verticalIn = @[@56]; // Filter, include only results from given verticals identified by a collection of ids (optional)
NSNumber* discounted = @true; // Filter, include results that have a discount greater than 0 (optional)
NSString* temporary = @"temporary_example"; // When set to `YES` only temporary exhibitions are returned. (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria to apply, prepend `-` for descending order (optional)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Get events
[apiInstance eventsGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              ids:ids
              idIn:idIn
              countryIn:countryIn
              notCountryIn:notCountryIn
              cityIn:cityIn
              notCityIn:notCityIn
              categoryIn:categoryIn
              verticalIn:verticalIn
              discounted:discounted
              temporary:temporary
              limit:limit
              offset:offset
              page:page
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->eventsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **ids** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Given events will always be returned (will not help achieve the limit quota) | [optional] 
 **idIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results having the given ids | [optional] 
 **countryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given countries identified by a collection of ids | [optional] 
 **notCountryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, exclude results from given countries identified by a collection of ids | [optional] 
 **cityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given cities identified by a collection of ids | [optional] 
 **notCityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, exclude results from given cities identified by a collection of ids | [optional] 
 **categoryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given categories identified by a collection of ids | [optional] 
 **verticalIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given verticals identified by a collection of ids | [optional] 
 **discounted** | **NSNumber***| Filter, include results that have a discount greater than 0 | [optional] 
 **temporary** | **NSString***| When set to &#x60;YES&#x60; only temporary exhibitions are returned. | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria to apply, prepend &#x60;-&#x60; for descending order | [optional] 

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsSearchExtendedGet**
```objc
-(NSURLSessionTask*) eventsSearchExtendedGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    q: (NSString*) q
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    verticalCollection: (NSArray<NSString*>*) verticalCollection
    categoryCollection: (NSArray<NSString*>*) categoryCollection
    countryCollection: (NSArray<NSString*>*) countryCollection
    cityCollection: (NSArray<NSString*>*) cityCollection
    minPrice: (NSString*) minPrice
    maxPrice: (NSString*) maxPrice
    topFeature: (NSString*) topFeature
    sorting: (NSString*) sorting
        completionHandler: (void (^)(SWGSearchResponse* output, NSError* error)) handler;
```

Deprecated API for search. Use GET /activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* q = @"q_example"; // Query String (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSArray<NSString*>* verticalCollection = @[@"verticalCollection_example"]; // List of verticals (optional)
NSArray<NSString*>* categoryCollection = @[@"categoryCollection_example"]; // List of categories (optional)
NSArray<NSString*>* countryCollection = @[@"countryCollection_example"]; // List of countries (optional)
NSArray<NSString*>* cityCollection = @[@"cityCollection_example"]; // List of cities (optional)
NSString* minPrice = @"minPrice_example"; // Minimum price for an event to be considered (optional)
NSString* maxPrice = @"maxPrice_example"; // Maximum price for an event to be considered (optional)
NSString* topFeature = @"topFeature_example"; // Query String (optional)
NSString* sorting = @"relevance"; // Sorting strategy (optional) (default to relevance)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Deprecated API for search. Use GET /activities
[apiInstance eventsSearchExtendedGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              q:q
              offset:offset
              limit:limit
              dateFrom:dateFrom
              dateTo:dateTo
              verticalCollection:verticalCollection
              categoryCollection:categoryCollection
              countryCollection:countryCollection
              cityCollection:cityCollection
              minPrice:minPrice
              maxPrice:maxPrice
              topFeature:topFeature
              sorting:sorting
          completionHandler: ^(SWGSearchResponse* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->eventsSearchExtendedGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **q** | **NSString***| Query String | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **verticalCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of verticals | [optional] 
 **categoryCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of categories | [optional] 
 **countryCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of countries | [optional] 
 **cityCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of cities | [optional] 
 **minPrice** | **NSString***| Minimum price for an event to be considered | [optional] 
 **maxPrice** | **NSString***| Maximum price for an event to be considered | [optional] 
 **topFeature** | **NSString***| Query String | [optional] 
 **sorting** | **NSString***| Sorting strategy | [optional] [default to relevance]

### Return type

[**SWGSearchResponse***](SWGSearchResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsSearchGet**
```objc
-(NSURLSessionTask*) eventsSearchGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(NSError* error)) handler;
```

Deprecated API for search - Use GET /activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Deprecated API for search - Use GET /activities
[apiInstance eventsSearchGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              limit:limit
              offset:offset
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->eventsSearchGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesVenueIdActivitiesGet**
```objc
-(NSURLSessionTask*) venuesVenueIdActivitiesGetWithVenueId: (NSNumber*) venueId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    category: (NSNumber*) category
    editorialCategory: (NSNumber*) editorialCategory
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given venue

Get all activities for an venue sorted by relevance. Only the activities that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* venueId = @56; // Venue identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* city = @56; // City identifier (optional)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* editorialCategory = @56; // Editorial category identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"venue-relevance"; // Sorting strategy (optional) (default to venue-relevance)

SWGActivityApi*apiInstance = [[SWGActivityApi alloc] init];

// Search activities within a given venue
[apiInstance venuesVenueIdActivitiesGetWithVenueId:venueId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              city:city
              category:category
              editorialCategory:editorialCategory
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGActivityApi->venuesVenueIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venueId** | **NSNumber***| Venue identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **city** | **NSNumber***| City identifier | [optional] 
 **category** | **NSNumber***| Category identifier | [optional] 
 **editorialCategory** | **NSNumber***| Editorial category identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to venue-relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

