# SWGWidgetConfigurationStepCoverBox

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSNumber*** |  | [optional] 
**reviews** | **NSNumber*** |  | [optional] 
**price** | **NSNumber*** |  | [optional] 
**discount** | **NSNumber*** |  | [optional] 
**mobileVoucher** | **NSNumber*** |  | [optional] 
**customCtaText** | **NSString*** |  | [optional] 
**customTitle** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


