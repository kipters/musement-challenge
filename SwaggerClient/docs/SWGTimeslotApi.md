# SWGTimeslotApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidDatesDayTimeslotsPost**](SWGTimeslotApi.md#activitiesactivityuuiddatesdaytimeslotspost) | **POST** /activities/{activityUuid}/dates/{day}/timeslots | Add a timeslot
[**activitiesActivityUuidDatesDayTimeslotsTimeGet**](SWGTimeslotApi.md#activitiesactivityuuiddatesdaytimeslotstimeget) | **GET** /activities/{activityUuid}/dates/{day}/timeslots/{time} | Get data for a timeslot
[**activitiesActivityUuidDatesDayTimeslotsTimePatch**](SWGTimeslotApi.md#activitiesactivityuuiddatesdaytimeslotstimepatch) | **PATCH** /activities/{activityUuid}/dates/{day}/timeslots/{time} | Update data for a timeslot
[**activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodeGet**](SWGTimeslotApi.md#activitiesactivityuuiddatesdaytimeslotstimeproductspricetagcodeget) | **GET** /activities/{activityUuid}/dates/{day}/timeslots/{time}/products/{pricetagCode} | Get data for a product of a timeslot
[**activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodePut**](SWGTimeslotApi.md#activitiesactivityuuiddatesdaytimeslotstimeproductspricetagcodeput) | **PUT** /activities/{activityUuid}/dates/{day}/timeslots/{time}/products/{pricetagCode} | Update data of a product for a specific timeslot
[**activitiesActivityUuidDatesDayTimeslotsTimeProductsPut**](SWGTimeslotApi.md#activitiesactivityuuiddatesdaytimeslotstimeproductsput) | **PUT** /activities/{activityUuid}/dates/{day}/timeslots/{time}/products | Update all the products for a specific timeslot


# **activitiesActivityUuidDatesDayTimeslotsPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsPostWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    postTimeslot: (SWGPostTimeslot*) postTimeslot
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGTimeslot* output, NSError* error)) handler;
```

Add a timeslot

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
SWGPostTimeslot* postTimeslot = [[SWGPostTimeslot alloc] init]; // Timeslot post request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGTimeslotApi*apiInstance = [[SWGTimeslotApi alloc] init];

// Add a timeslot
[apiInstance activitiesActivityUuidDatesDayTimeslotsPostWithActivityUuid:activityUuid
              day:day
              postTimeslot:postTimeslot
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGTimeslot* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTimeslotApi->activitiesActivityUuidDatesDayTimeslotsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **postTimeslot** | [**SWGPostTimeslot***](SWGPostTimeslot.md)| Timeslot post request | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGTimeslot***](SWGTimeslot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesDayTimeslotsTimeGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGTimeslot* output, NSError* error)) handler;
```

Get data for a timeslot

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
NSString* time = @"time_example"; // String containing hours and minutes in the format 'HH-MM' | Example: 22-40
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGTimeslotApi*apiInstance = [[SWGTimeslotApi alloc] init];

// Get data for a timeslot
[apiInstance activitiesActivityUuidDatesDayTimeslotsTimeGetWithActivityUuid:activityUuid
              day:day
              time:time
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGTimeslot* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTimeslotApi->activitiesActivityUuidDatesDayTimeslotsTimeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **time** | **NSString***| String containing hours and minutes in the format &#39;HH-MM&#39; | Example: 22-40 | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGTimeslot***](SWGTimeslot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesDayTimeslotsTimePatch**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimePatchWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    patchTimeslot: (SWGPatchTimeslot*) patchTimeslot
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGTimeslot* output, NSError* error)) handler;
```

Update data for a timeslot

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
NSString* time = @"time_example"; // String containing hours and minutes in the format 'HH-MM' | Example: 22-40
SWGPatchTimeslot* patchTimeslot = [[SWGPatchTimeslot alloc] init]; // Timeslot patch request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGTimeslotApi*apiInstance = [[SWGTimeslotApi alloc] init];

// Update data for a timeslot
[apiInstance activitiesActivityUuidDatesDayTimeslotsTimePatchWithActivityUuid:activityUuid
              day:day
              time:time
              patchTimeslot:patchTimeslot
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGTimeslot* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTimeslotApi->activitiesActivityUuidDatesDayTimeslotsTimePatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **time** | **NSString***| String containing hours and minutes in the format &#39;HH-MM&#39; | Example: 22-40 | 
 **patchTimeslot** | [**SWGPatchTimeslot***](SWGPatchTimeslot.md)| Timeslot patch request | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGTimeslot***](SWGTimeslot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodeGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodeGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    pricetagCode: (NSString*) pricetagCode
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGTimeslotProduct* output, NSError* error)) handler;
```

Get data for a product of a timeslot

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
NSString* time = @"time_example"; // String containing hours and minutes in the format 'HH-MM' | Example: 22-40
NSString* pricetagCode = @"pricetagCode_example"; // Full pricetag code holder and feature separated by pipe | Example: adult|entrance-with-audioguide
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGTimeslotApi*apiInstance = [[SWGTimeslotApi alloc] init];

// Get data for a product of a timeslot
[apiInstance activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodeGetWithActivityUuid:activityUuid
              day:day
              time:time
              pricetagCode:pricetagCode
              xMusementVersion:xMusementVersion
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGTimeslotProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTimeslotApi->activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **time** | **NSString***| String containing hours and minutes in the format &#39;HH-MM&#39; | Example: 22-40 | 
 **pricetagCode** | **NSString***| Full pricetag code holder and feature separated by pipe | Example: adult|entrance-with-audioguide | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGTimeslotProduct***](SWGTimeslotProduct.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodePut**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodePutWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    pricetagCode: (NSString*) pricetagCode
    putTimeslotProduct: (SWGPutTimeslotProduct*) putTimeslotProduct
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGTimeslotProduct* output, NSError* error)) handler;
```

Update data of a product for a specific timeslot

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
NSString* time = @"time_example"; // String containing hours and minutes in the format 'HH-MM' | Example: 22-40
NSString* pricetagCode = @"pricetagCode_example"; // Full pricetag code holder and feature separated by pipe | Example: adult|entrance-with-audioguide
SWGPutTimeslotProduct* putTimeslotProduct = [[SWGPutTimeslotProduct alloc] init]; // Timeslot product put request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGTimeslotApi*apiInstance = [[SWGTimeslotApi alloc] init];

// Update data of a product for a specific timeslot
[apiInstance activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodePutWithActivityUuid:activityUuid
              day:day
              time:time
              pricetagCode:pricetagCode
              putTimeslotProduct:putTimeslotProduct
              xMusementVersion:xMusementVersion
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGTimeslotProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTimeslotApi->activitiesActivityUuidDatesDayTimeslotsTimeProductsPricetagCodePut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **time** | **NSString***| String containing hours and minutes in the format &#39;HH-MM&#39; | Example: 22-40 | 
 **pricetagCode** | **NSString***| Full pricetag code holder and feature separated by pipe | Example: adult|entrance-with-audioguide | 
 **putTimeslotProduct** | [**SWGPutTimeslotProduct***](SWGPutTimeslotProduct.md)| Timeslot product put request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGTimeslotProduct***](SWGTimeslotProduct.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesDayTimeslotsTimeProductsPut**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayTimeslotsTimeProductsPutWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    time: (NSString*) time
    putTimeslotProducts: (SWGPutTimeslotProducts*) putTimeslotProducts
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGTimeslotProduct* output, NSError* error)) handler;
```

Update all the products for a specific timeslot

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
NSString* time = @"time_example"; // String containing hours and minutes in the format 'HH-MM' | Example: 22-40
SWGPutTimeslotProducts* putTimeslotProducts = [[SWGPutTimeslotProducts alloc] init]; // Timeslot products put request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGTimeslotApi*apiInstance = [[SWGTimeslotApi alloc] init];

// Update all the products for a specific timeslot
[apiInstance activitiesActivityUuidDatesDayTimeslotsTimeProductsPutWithActivityUuid:activityUuid
              day:day
              time:time
              putTimeslotProducts:putTimeslotProducts
              xMusementVersion:xMusementVersion
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGTimeslotProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTimeslotApi->activitiesActivityUuidDatesDayTimeslotsTimeProductsPut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **time** | **NSString***| String containing hours and minutes in the format &#39;HH-MM&#39; | Example: 22-40 | 
 **putTimeslotProducts** | [**SWGPutTimeslotProducts***](SWGPutTimeslotProducts.md)| Timeslot products put request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGTimeslotProduct***](SWGTimeslotProduct.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

