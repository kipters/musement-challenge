# SWGSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groups** | [**NSArray&lt;SWGScheduleGroup&gt;***](SWGScheduleGroup.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


