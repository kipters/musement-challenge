# SWGPostActivityBlackoutDay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **NSDate*** | Day to add | YYYY-MM-DD | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


