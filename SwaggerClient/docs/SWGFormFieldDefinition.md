# SWGFormFieldDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**value** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 
**required** | **NSNumber*** |  | [optional] 
**label** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


