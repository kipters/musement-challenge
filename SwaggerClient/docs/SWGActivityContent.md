# SWGActivityContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**when** | **NSString*** |  | [optional] 
**operationalDays** | **NSString*** |  | [optional] 
**isDefault** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


