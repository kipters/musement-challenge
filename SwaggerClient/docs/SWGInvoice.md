# SWGInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **NSString*** |  | [optional] 
**grandTotal** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**createdAt** | **NSDate*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


