# SWGEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxConfirmationTime** | **NSString*** |  | [optional] 
**cutoffTime** | **NSString*** |  | [optional] 
**sellerGateway** | [**SWGSellerGateway***](SWGSellerGateway.md) |  | [optional] 
**partnerInternalCode** | **NSString*** |  | [optional] 
**uuid** | **NSString*** | Event unique identifier | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**saves** | **NSNumber*** | Number of customer who saved this city as favorite | [optional] 
**supplierCode** | **NSString*** | Supplier identification code. | [optional] 
**title** | **NSString*** |  | [optional] 
**relevance** | **NSNumber*** |  | [optional] 
**emergencyPhoneNumber** | **NSString*** |  | [optional] 
**relevanceVenue** | **NSNumber*** |  | [optional] 
**mustSee** | **NSNumber*** |  | [optional] 
**lastChance** | **NSNumber*** |  | [optional] 
**topSeller** | **NSNumber*** |  | [optional] 
**printVoucher** | **NSNumber*** | Voucher must be printed to access the activity | [optional] 
**temporary** | **NSNumber*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**about** | **NSString*** |  | [optional] 
**aboutMarkdown** | **NSString*** |  | [optional] 
**aboutHtml** | **NSString*** |  | [optional] 
**meetingPoint** | **NSString*** |  | [optional] 
**duration** | **NSString*** |  | [optional] 
**validity** | **NSString*** |  | [optional] 
**numberedSeats** | **NSNumber*** |  | [optional] 
**hasPriceInfoOnDate** | **NSNumber*** | If true the call GET /events/{id}/dates returns prices for each day. | [optional] 
**open** | **NSNumber*** | Define if the Event is with open dates entrance (aka: Event::isOpenTicket). | [optional] 
**ticketNotIncluded** | **NSNumber*** |  | [optional] 
**likelyToSellOut** | **NSNumber*** |  | [optional] 
**specialOffer** | **NSNumber*** |  | [optional] 
**exclusive** | **NSNumber*** |  | [optional] 
**includedHotelNightNumber** | **NSNumber*** |  | [optional] 
**greenFeeDays** | **NSNumber*** |  | [optional] 
**daily** | **NSNumber*** | Define if the Event is with \&quot;daily tickets\&quot; (aka: Event::getDailyTickets) This causes for example: to hide \&quot;time block\&quot; in Orderbox. | [optional] 
**languages** | [**NSArray&lt;SWGTranslatedMetadata&gt;***](SWGTranslatedMetadata.md) |  | [optional] 
**groupSize** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**food** | [**NSArray&lt;SWGTranslatedMetadata&gt;***](SWGTranslatedMetadata.md) |  | [optional] 
**services** | [**NSArray&lt;SWGTranslatedMetadata&gt;***](SWGTranslatedMetadata.md) |  | [optional] 
**translated** | **NSNumber*** |  | [optional] 
**automaticallyTranslated** | **NSNumber*** |  | [optional] 
**features** | [**NSArray&lt;SWGTranslatedMetadata&gt;***](SWGTranslatedMetadata.md) |  | [optional] 
**highlights** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**included** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**notIncluded** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**isAvailableToday** | **NSNumber*** |  | [optional] 
**isAvailableTomorrow** | **NSNumber*** |  | [optional] 
**hasMultipleOptions** | **NSNumber*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**extraMediaUrl** | **NSString*** |  | [optional] 
**serviceFee** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**retailPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**netPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**bundledPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**discount** | **NSNumber*** |  | [optional] 
**bucket** | [**SWGBucket***](SWGBucket.md) |  | [optional] 
**categories** | [**NSArray&lt;SWGCategory&gt;***](SWGCategory.md) |  | [optional] 
**reviewsNumber** | **NSNumber*** |  | [optional] 
**reviewsAvg** | **NSNumber*** |  | [optional] 
**reviewsAggregatedInfo** | [**SWGReviewsAggregatedInfo***](SWGReviewsAggregatedInfo.md) |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**agencyUrls** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**agencyPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**tags** | [**NSArray&lt;SWGTag&gt;***](SWGTag.md) |  | [optional] 
**flavours** | [**NSArray&lt;SWGFlavour&gt;***](SWGFlavour.md) |  | [optional] 
**verticals** | [**NSArray&lt;SWGVertical&gt;***](SWGVertical.md) |  | [optional] 
**orderBoxElements** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**giftable** | **NSNumber*** |  | [optional] 
**hasPassengerInfo** | **NSNumber*** |  | [optional] 
**hasExtraCustomerData** | **NSNumber*** |  | [optional] 
**buyMultiplier** | **NSNumber*** |  | [optional] 
**status** | **NSNumber*** |       * ONLINE: The actiity is visible on musement.com and it can be booked      * ARCHIVED: The actiity is visible on musement.com but it cannot be booked      * INACTIVE: The actiity is not visible on musement.com | [optional] 
**totalBookings** | **NSNumber*** |  | [optional] 
**createdAt** | **NSDate*** |  | [optional] 
**publishedAt** | **NSDate*** |  | [optional] 
**merchantPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**operationalDays** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


