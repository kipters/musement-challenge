# SWGPurchaseExperienceApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**feedbackRequestsFeedbackRequestNonceGet**](SWGPurchaseExperienceApi.md#feedbackrequestsfeedbackrequestnonceget) | **GET** /feedback-requests/{feedbackRequestNonce} | Get feedback request details


# **feedbackRequestsFeedbackRequestNonceGet**
```objc
-(NSURLSessionTask*) feedbackRequestsFeedbackRequestNonceGetWithFeedbackRequestNonce: (NSString*) feedbackRequestNonce
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGFeedbackRequest* output, NSError* error)) handler;
```

Get feedback request details

### Example 
```objc

NSString* feedbackRequestNonce = @"feedbackRequestNonce_example"; // Feedback request nonce
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGPurchaseExperienceApi*apiInstance = [[SWGPurchaseExperienceApi alloc] init];

// Get feedback request details
[apiInstance feedbackRequestsFeedbackRequestNonceGetWithFeedbackRequestNonce:feedbackRequestNonce
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGFeedbackRequest* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPurchaseExperienceApi->feedbackRequestsFeedbackRequestNonceGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feedbackRequestNonce** | **NSString***| Feedback request nonce | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGFeedbackRequest***](SWGFeedbackRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

