# SWGBraintreeApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsBraintreePaymentPost**](SWGBraintreeApi.md#paymentsbraintreepaymentpost) | **POST** /payments/braintree/payment | Pay an order.


# **paymentsBraintreePaymentPost**
```objc
-(NSURLSessionTask*) paymentsBraintreePaymentPostWithBraintreePayment: (SWGBraintreePaymentRequest*) braintreePayment
        completionHandler: (void (^)(SWGResponseWithMessage* output, NSError* error)) handler;
```

Pay an order.

Pay an order using braintree.

### Example 
```objc

SWGBraintreePaymentRequest* braintreePayment = [[SWGBraintreePaymentRequest alloc] init]; // Braintree payment info request

SWGBraintreeApi*apiInstance = [[SWGBraintreeApi alloc] init];

// Pay an order.
[apiInstance paymentsBraintreePaymentPostWithBraintreePayment:braintreePayment
          completionHandler: ^(SWGResponseWithMessage* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBraintreeApi->paymentsBraintreePaymentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **braintreePayment** | [**SWGBraintreePaymentRequest***](SWGBraintreePaymentRequest.md)| Braintree payment info request | 

### Return type

[**SWGResponseWithMessage***](SWGResponseWithMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

