# SWGActivityRelated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventsCount** | **NSNumber*** |  | [optional] 
**_id** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**coverImage** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**apiUrl** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


