# SWGMediaApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidLanguagesGet**](SWGMediaApi.md#activitiesactivityuuidlanguagesget) | **GET** /activities/{activityUuid}/languages | Get all languages for an activity.
[**activitiesActivityUuidMediaGet**](SWGMediaApi.md#activitiesactivityuuidmediaget) | **GET** /activities/{activityUuid}/media | Get all media for an activity. Media are both images and videos
[**activitiesActivityUuidMediaPost**](SWGMediaApi.md#activitiesactivityuuidmediapost) | **POST** /activities/{activityUuid}/media | Add a media to an activity


# **activitiesActivityUuidLanguagesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidLanguagesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all languages for an activity.

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGMediaApi*apiInstance = [[SWGMediaApi alloc] init];

// Get all languages for an activity.
[apiInstance activitiesActivityUuidLanguagesGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGMediaApi->activitiesActivityUuidLanguagesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidMediaGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidMediaGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGGalleryMedia>* output, NSError* error)) handler;
```

Get all media for an activity. Media are both images and videos

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGMediaApi*apiInstance = [[SWGMediaApi alloc] init];

// Get all media for an activity. Media are both images and videos
[apiInstance activitiesActivityUuidMediaGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGGalleryMedia>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGMediaApi->activitiesActivityUuidMediaGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGGalleryMedia>***](SWGGalleryMedia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidMediaPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidMediaPostWithActivityUuid: (NSString*) activityUuid
    postActivityMedia: (SWGPostActivityMedia*) postActivityMedia
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGGalleryMedia* output, NSError* error)) handler;
```

Add a media to an activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPostActivityMedia* postActivityMedia = [[SWGPostActivityMedia alloc] init]; // Media information
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGMediaApi*apiInstance = [[SWGMediaApi alloc] init];

// Add a media to an activity
[apiInstance activitiesActivityUuidMediaPostWithActivityUuid:activityUuid
              postActivityMedia:postActivityMedia
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGGalleryMedia* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGMediaApi->activitiesActivityUuidMediaPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **postActivityMedia** | [**SWGPostActivityMedia***](SWGPostActivityMedia.md)| Media information | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGGalleryMedia***](SWGGalleryMedia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

