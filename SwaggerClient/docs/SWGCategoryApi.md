# SWGCategoryApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesCategoryIdActivitiesGet**](SWGCategoryApi.md#categoriescategoryidactivitiesget) | **GET** /categories/{categoryId}/activities | Search activities within a given category
[**categoriesCategoryIdGet**](SWGCategoryApi.md#categoriescategoryidget) | **GET** /categories/{categoryId} | Get a category by identifier
[**categoriesGet**](SWGCategoryApi.md#categoriesget) | **GET** /categories | Get categories
[**citiesCityIdCategoriesGet**](SWGCategoryApi.md#citiescityidcategoriesget) | **GET** /cities/{cityId}/categories | Categories for the city. Sorted by the number of events for the category
[**citiesCityIdVerticalsVerticalIdCategoriesGet**](SWGCategoryApi.md#citiescityidverticalsverticalidcategoriesget) | **GET** /cities/{cityId}/verticals/{verticalId}/categories | Categories for the city for a specific vertical. Sorted by the number of activities for the category


# **categoriesCategoryIdActivitiesGet**
```objc
-(NSURLSessionTask*) categoriesCategoryIdActivitiesGetWithCategoryId: (NSNumber*) categoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    venue: (NSNumber*) venue
    editorialCategory: (NSNumber*) editorialCategory
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given category

Get all activities for a category sorted by category relevance. Only the activities that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* categoryId = @56; // Category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* city = @56; // City identifier (optional)
NSNumber* venue = @56; // Venue identifier (optional)
NSNumber* editorialCategory = @56; // Editorial category identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"venue-relevance"; // Sorting strategy (optional) (default to venue-relevance)

SWGCategoryApi*apiInstance = [[SWGCategoryApi alloc] init];

// Search activities within a given category
[apiInstance categoriesCategoryIdActivitiesGetWithCategoryId:categoryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              city:city
              venue:venue
              editorialCategory:editorialCategory
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCategoryApi->categoriesCategoryIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **NSNumber***| Category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **city** | **NSNumber***| City identifier | [optional] 
 **venue** | **NSNumber***| Venue identifier | [optional] 
 **editorialCategory** | **NSNumber***| Editorial category identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to venue-relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoriesCategoryIdGet**
```objc
-(NSURLSessionTask*) categoriesCategoryIdGetWithCategoryId: (NSNumber*) categoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCategory* output, NSError* error)) handler;
```

Get a category by identifier

### Example 
```objc

NSNumber* categoryId = @56; // Category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCategoryApi*apiInstance = [[SWGCategoryApi alloc] init];

// Get a category by identifier
[apiInstance categoriesCategoryIdGetWithCategoryId:categoryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCategoryApi->categoriesCategoryIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **NSNumber***| Category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCategory***](SWGCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoriesGet**
```objc
-(NSURLSessionTask*) categoriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    page: (NSNumber*) page
    sortBy: (NSArray<NSString*>*) sortBy
    filteringAware: (NSNumber*) filteringAware
        completionHandler: (void (^)(NSArray<SWGCategory>* output, NSError* error)) handler;
```

Get categories

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria: [relevance|city_relevance|discount], prepend `-` for descending order (optional)
NSNumber* filteringAware = @true; // If true the category is usable as filter (optional)

SWGCategoryApi*apiInstance = [[SWGCategoryApi alloc] init];

// Get categories
[apiInstance categoriesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
              page:page
              sortBy:sortBy
              filteringAware:filteringAware
          completionHandler: ^(NSArray<SWGCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCategoryApi->categoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria: [relevance|city_relevance|discount], prepend &#x60;-&#x60; for descending order | [optional] 
 **filteringAware** | **NSNumber***| If true the category is usable as filter | [optional] 

### Return type

[**NSArray<SWGCategory>***](SWGCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdCategoriesGet**
```objc
-(NSURLSessionTask*) citiesCityIdCategoriesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGCategory>* output, NSError* error)) handler;
```

Categories for the city. Sorted by the number of events for the category

Categories for the city. Sorted by the number of activities for the category

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCategoryApi*apiInstance = [[SWGCategoryApi alloc] init];

// Categories for the city. Sorted by the number of events for the category
[apiInstance citiesCityIdCategoriesGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCategoryApi->citiesCityIdCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGCategory>***](SWGCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdVerticalsVerticalIdCategoriesGet**
```objc
-(NSURLSessionTask*) citiesCityIdVerticalsVerticalIdCategoriesGetWithCityId: (NSNumber*) cityId
    verticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGCategoryAggregated>* output, NSError* error)) handler;
```

Categories for the city for a specific vertical. Sorted by the number of activities for the category

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCategoryApi*apiInstance = [[SWGCategoryApi alloc] init];

// Categories for the city for a specific vertical. Sorted by the number of activities for the category
[apiInstance citiesCityIdVerticalsVerticalIdCategoriesGetWithCityId:cityId
              verticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGCategoryAggregated>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCategoryApi->citiesCityIdVerticalsVerticalIdCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGCategoryAggregated>***](SWGCategoryAggregated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

