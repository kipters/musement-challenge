# SWGTicketCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | [optional] 
**status** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 
**internal** | **NSNumber*** |  | [optional] 
**createdAt** | **NSDate*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


