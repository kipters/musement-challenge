# SWGPatchCustomerPreferences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**varNewsletterFrequency** | **NSString*** | How often the customer want to receive a newsletter ?  * 3-TIMES-A-WEEK: Three newsletters a week,  * WEEKLY: A newsletter a week  * MONTHLY: A newsletter a month,  * NEVER: No newsletters are sent to the customer | [optional] 
**varNewsletterFromThirdparty** | **NSString*** | Does the customer want to receive newsletter from Musement&#39;s thirdparty ?  * YES: Yes  * NO: No | [optional] 
**interests** | **NSArray&lt;NSString*&gt;*** | Customer&#39;s interests ?  * ARTS-CULTURE : Arts and culture,  * SIGHTSEEING :  Sightseeing,  * FOOD-WINE : Food and wine,  * ENTERTAIMENT : Entertaiment,  * SPORTS : Sports,  * ADVENTURE : Adventure,  * NIGHTLIFE : Nightlife | [optional] 
**travelWith** | **NSString*** | The customer like to travel with:  * ALONE : Alone,  * PARTNER : Partner,  * FAMILY : Family,  * FRIENDS : Friends,  * GROUP : Group | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


