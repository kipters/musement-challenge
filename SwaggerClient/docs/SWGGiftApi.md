# SWGGiftApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventsEventIdGiftScheduleGet**](SWGGiftApi.md#eventseventidgiftscheduleget) | **GET** /events/{eventId}/gift-schedule | Get schedule for a &#39;Gift creation&#39; for an Event
[**eventsEventIdScheduleDayGiftCodeGet**](SWGGiftApi.md#eventseventidscheduledaygiftcodeget) | **GET** /events/{eventId}/schedule/{day}/{giftCode} | Get schedule for Gift and specific day
[**giftsPost**](SWGGiftApi.md#giftspost) | **POST** /gifts | Currently can be used only with &#39;FROM-GIFTBOX&#39; strategy - it starts Redeem process for given Giftbox (giftbox_code) and with chosen Activity (giftbox_type_item_id)


# **eventsEventIdGiftScheduleGet**
```objc
-(NSURLSessionTask*) eventsEventIdGiftScheduleGetWithEventId: (NSNumber*) eventId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGGiftSchedule* output, NSError* error)) handler;
```

Get schedule for a 'Gift creation' for an Event

### Example 
```objc

NSNumber* eventId = @56; // Event identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGGiftApi*apiInstance = [[SWGGiftApi alloc] init];

// Get schedule for a 'Gift creation' for an Event
[apiInstance eventsEventIdGiftScheduleGetWithEventId:eventId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGGiftSchedule* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftApi->eventsEventIdGiftScheduleGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **NSNumber***| Event identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGGiftSchedule***](SWGGiftSchedule.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsEventIdScheduleDayGiftCodeGet**
```objc
-(NSURLSessionTask*) eventsEventIdScheduleDayGiftCodeGetWithEventId: (NSNumber*) eventId
    day: (NSDate*) day
    giftCode: (NSString*) giftCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(NSArray<SWGSchedule>* output, NSError* error)) handler;
```

Get schedule for Gift and specific day

### Example 
```objc

NSNumber* eventId = @56; // Event identifier
NSDate* day = @"2013-10-20"; // Day
NSString* giftCode = @"giftCode_example"; // Gift code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGGiftApi*apiInstance = [[SWGGiftApi alloc] init];

// Get schedule for Gift and specific day
[apiInstance eventsEventIdScheduleDayGiftCodeGetWithEventId:eventId
              day:day
              giftCode:giftCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(NSArray<SWGSchedule>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftApi->eventsEventIdScheduleDayGiftCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **NSNumber***| Event identifier | 
 **day** | **NSDate***| Day | 
 **giftCode** | **NSString***| Gift code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**NSArray<SWGSchedule>***](SWGSchedule.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **giftsPost**
```objc
-(NSURLSessionTask*) giftsPostWithGiftProduct: (SWGPostGiftProduct*) giftProduct
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGGiftProduct* output, NSError* error)) handler;
```

Currently can be used only with 'FROM-GIFTBOX' strategy - it starts Redeem process for given Giftbox (giftbox_code) and with chosen Activity (giftbox_type_item_id)

### Example 
```objc

SWGPostGiftProduct* giftProduct = [[SWGPostGiftProduct alloc] init]; // Gift information
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGGiftApi*apiInstance = [[SWGGiftApi alloc] init];

// Currently can be used only with 'FROM-GIFTBOX' strategy - it starts Redeem process for given Giftbox (giftbox_code) and with chosen Activity (giftbox_type_item_id)
[apiInstance giftsPostWithGiftProduct:giftProduct
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGGiftProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftApi->giftsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftProduct** | [**SWGPostGiftProduct***](SWGPostGiftProduct.md)| Gift information | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGGiftProduct***](SWGGiftProduct.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

