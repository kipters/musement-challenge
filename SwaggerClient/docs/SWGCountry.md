# SWGCountry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**isoCode** | **NSString*** |  | [optional] 
**countryPrefix** | **NSString*** |  | [optional] 
**currencyCode** | **NSString*** | Our currency code for that Country - for United Stated its USD, for Great Britain its GBP etc | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


