# SWGPage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**pageCategory** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 
**parentId** | **NSNumber*** |  | [optional] 
**children** | [**NSArray&lt;SWGPageLink&gt;***](SWGPageLink.md) |  | [optional] 
**path** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**content** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**location** | **NSString*** |  | [optional] 
**active** | **NSNumber*** |  | [optional] 
**publishedAt** | **NSDate*** |  | [optional] 
**weight** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


