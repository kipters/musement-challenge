# SWGGenericEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**verticals** | [**NSArray&lt;SWGVertical&gt;***](SWGVertical.md) |  | [optional] 
**whereIs** | **NSString*** |  | [optional] 
**whenIs** | **NSString*** |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**date** | **NSString*** |  | [optional] 
**startAt** | **NSDate*** |  | [optional] 
**endAt** | **NSDate*** |  | [optional] 
**temporary** | **NSNumber*** |  | [optional] 
**price** | **NSString*** |  | [optional] 
**phone** | **NSString*** |  | [optional] 
**website** | **NSString*** |  | [optional] 
**rating** | **NSNumber*** |  | [optional] 
**votes** | **NSNumber*** |  | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**venue** | [**SWGVenue***](SWGVenue.md) |  | [optional] 
**images** | [**NSArray&lt;SWGGalleryMedia&gt;***](SWGGalleryMedia.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


