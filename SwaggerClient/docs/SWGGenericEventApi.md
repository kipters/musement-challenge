# SWGGenericEventApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**genericEventsGet**](SWGGenericEventApi.md#genericeventsget) | **GET** /generic-events | Get generic events


# **genericEventsGet**
```objc
-(NSURLSessionTask*) genericEventsGetWithCityIn: (NSArray<NSNumber*>*) cityIn
    maxDistance: (NSString*) maxDistance
    maxDistanceFromCity: (NSString*) maxDistanceFromCity
    nowOpen: (NSNumber*) nowOpen
    temporary: (NSString*) temporary
    verticalIn: (NSArray<NSNumber*>*) verticalIn
    sortBy: (NSArray<NSString*>*) sortBy
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
    page: (NSNumber*) page
        completionHandler: (void (^)(NSArray<SWGGenericEvent>* output, NSError* error)) handler;
```

Get generic events

### Example 
```objc

NSArray<NSNumber*>* cityIn = @[@56]; // Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
NSString* maxDistance = @"maxDistance_example"; // Maximum distance expressed with its unit from a given point coordinates expressed in decimal grades, accepts kilometers and miles. E.G.: 44.12233|12.23233|100KM or 44.12233|12.23233|62M (optional)
NSString* maxDistanceFromCity = @"maxDistanceFromCity_example"; // Maximum distance expressed with its unit from a given city id, accepts kilometers and miles. E.G.: 231|100KM or 51234|12.23233|62M (optional)
NSNumber* nowOpen = @8.14; // Is set to `YES` only activity open now are returned. (optional)
NSString* temporary = @"temporary_example"; // When set to `YES` only temporary exhibitions are returned. (optional)
NSArray<NSNumber*>* verticalIn = @[@56]; // Filter, include only results from given verticals identified by a collection of ids (optional)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria to apply, prepend `-` for descending order (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)

SWGGenericEventApi*apiInstance = [[SWGGenericEventApi alloc] init];

// Get generic events
[apiInstance genericEventsGetWithCityIn:cityIn
              maxDistance:maxDistance
              maxDistanceFromCity:maxDistanceFromCity
              nowOpen:nowOpen
              temporary:temporary
              verticalIn:verticalIn
              sortBy:sortBy
              limit:limit
              offset:offset
              page:page
          completionHandler: ^(NSArray<SWGGenericEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGenericEventApi->genericEventsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given cities identified by a collection of ids | [optional] 
 **maxDistance** | **NSString***| Maximum distance expressed with its unit from a given point coordinates expressed in decimal grades, accepts kilometers and miles. E.G.: 44.12233|12.23233|100KM or 44.12233|12.23233|62M | [optional] 
 **maxDistanceFromCity** | **NSString***| Maximum distance expressed with its unit from a given city id, accepts kilometers and miles. E.G.: 231|100KM or 51234|12.23233|62M | [optional] 
 **nowOpen** | **NSNumber***| Is set to &#x60;YES&#x60; only activity open now are returned. | [optional] 
 **temporary** | **NSString***| When set to &#x60;YES&#x60; only temporary exhibitions are returned. | [optional] 
 **verticalIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given verticals identified by a collection of ids | [optional] 
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria to apply, prepend &#x60;-&#x60; for descending order | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 

### Return type

[**NSArray<SWGGenericEvent>***](SWGGenericEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

