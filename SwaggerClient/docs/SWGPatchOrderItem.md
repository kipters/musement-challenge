# SWGPatchOrderItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **NSString*** |   * BOOKING-CONFIRMED: Booking was confirmed by supplier  * CONFIRM-REFUSED: Supplier refused confirmation  * NEEDS-CONFIRM: Supplier needs to confirm booking  * WAITING-CONFIRM: Booking awaits supplier confirmation | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


