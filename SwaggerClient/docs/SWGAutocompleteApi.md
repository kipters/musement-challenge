# SWGAutocompleteApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocompleteGet**](SWGAutocompleteApi.md#autocompleteget) | **GET** /autocomplete | Get autocompletion suggestions for textual search


# **autocompleteGet**
```objc
-(NSURLSessionTask*) autocompleteGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    sortBy: (NSArray<NSString*>*) sortBy
    coordinates: (NSString*) coordinates
    text: (NSString*) text
    activityLimit: (NSNumber*) activityLimit
    activityOffset: (NSNumber*) activityOffset
    blogPostLimit: (NSNumber*) blogPostLimit
    blogPostOffset: (NSNumber*) blogPostOffset
    categoryLimit: (NSNumber*) categoryLimit
    categoryOffset: (NSNumber*) categoryOffset
    cityLimit: (NSNumber*) cityLimit
    cityOffset: (NSNumber*) cityOffset
    listLimit: (NSNumber*) listLimit
    listOffset: (NSNumber*) listOffset
    venueLimit: (NSNumber*) venueLimit
    venueOffset: (NSNumber*) venueOffset
        completionHandler: (void (^)(NSArray<SWGAutocompleteResult>* output, NSError* error)) handler;
```

Get autocompletion suggestions for textual search

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria to apply, prepend `-` for descending order (optional)
NSString* coordinates = @"coordinates_example"; // Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
NSString* text = @"text_example"; // Text to search (optional)
NSNumber* activityLimit = @56; // Maximum number of activities to return (optional)
NSNumber* activityOffset = @56; // Offset for activities to return (optional)
NSNumber* blogPostLimit = @56; // Maximum number of blog posts to return (optional)
NSNumber* blogPostOffset = @56; // Offset for blog posts to return (optional)
NSNumber* categoryLimit = @56; // Maximum number of categories to return (optional)
NSNumber* categoryOffset = @56; // Offset for categories to return (optional)
NSNumber* cityLimit = @56; // Maximum number of cities to return (optional)
NSNumber* cityOffset = @56; // Offset for cities to return (optional)
NSNumber* listLimit = @56; // Maximum number of lists to return (optional)
NSNumber* listOffset = @56; // Offset for lists to return (optional)
NSNumber* venueLimit = @56; // Maximum number of venues to return (optional)
NSNumber* venueOffset = @56; // Offset for venues to return (optional)

SWGAutocompleteApi*apiInstance = [[SWGAutocompleteApi alloc] init];

// Get autocompletion suggestions for textual search
[apiInstance autocompleteGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              sortBy:sortBy
              coordinates:coordinates
              text:text
              activityLimit:activityLimit
              activityOffset:activityOffset
              blogPostLimit:blogPostLimit
              blogPostOffset:blogPostOffset
              categoryLimit:categoryLimit
              categoryOffset:categoryOffset
              cityLimit:cityLimit
              cityOffset:cityOffset
              listLimit:listLimit
              listOffset:listOffset
              venueLimit:venueLimit
              venueOffset:venueOffset
          completionHandler: ^(NSArray<SWGAutocompleteResult>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAutocompleteApi->autocompleteGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria to apply, prepend &#x60;-&#x60; for descending order | [optional] 
 **coordinates** | **NSString***| Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 | [optional] 
 **text** | **NSString***| Text to search | [optional] 
 **activityLimit** | **NSNumber***| Maximum number of activities to return | [optional] 
 **activityOffset** | **NSNumber***| Offset for activities to return | [optional] 
 **blogPostLimit** | **NSNumber***| Maximum number of blog posts to return | [optional] 
 **blogPostOffset** | **NSNumber***| Offset for blog posts to return | [optional] 
 **categoryLimit** | **NSNumber***| Maximum number of categories to return | [optional] 
 **categoryOffset** | **NSNumber***| Offset for categories to return | [optional] 
 **cityLimit** | **NSNumber***| Maximum number of cities to return | [optional] 
 **cityOffset** | **NSNumber***| Offset for cities to return | [optional] 
 **listLimit** | **NSNumber***| Maximum number of lists to return | [optional] 
 **listOffset** | **NSNumber***| Offset for lists to return | [optional] 
 **venueLimit** | **NSNumber***| Maximum number of venues to return | [optional] 
 **venueOffset** | **NSNumber***| Offset for venues to return | [optional] 

### Return type

[**NSArray<SWGAutocompleteResult>***](SWGAutocompleteResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

