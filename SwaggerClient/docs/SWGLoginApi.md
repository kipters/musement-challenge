# SWGLoginApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginGet**](SWGLoginApi.md#loginget) | **GET** /login | Login a user via OAuth 2
[**refreshGet**](SWGLoginApi.md#refreshget) | **GET** /refresh | Retrieve a fresh access token
[**resetPasswordPost**](SWGLoginApi.md#resetpasswordpost) | **POST** /reset_password | Send reset password email. This method always returns 204


# **loginGet**
```objc
-(NSURLSessionTask*) loginGetWithClientId: (NSString*) clientId
    grantType: (NSString*) grantType
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    clientSecret: (NSString*) clientSecret
    password: (NSString*) password
    username: (NSString*) username
    nonce: (NSString*) nonce
        completionHandler: (void (^)(SWGToken* output, NSError* error)) handler;
```

Login a user via OAuth 2

### Example 
```objc

NSString* clientId = @"clientId_example"; // 
NSString* grantType = @"grantType_example"; // Oauth2 grant type. We support standard flows: - authorization_code - client_credentials - password  and 2 custom:  - https://api.musement.com/grant/comment-nonce - https://api.musement.com/grant/purchase-experience-nonce  These last 2 must be used together with 'nonce' parameter and allow to login a user using a nonce.
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* clientSecret = @"clientSecret_example"; //  (optional)
NSString* password = @"password_example"; // Password of the user we want to login. Used when 'gran_type' is 'password'. (optional)
NSString* username = @"username_example"; // Username of the user we want to login. Used when 'gran_type' is 'password'. (optional)
NSString* nonce = @"nonce_example"; // Nonce token used to authenticate the customer using a nonce in a mail or in a notification. (optional)

SWGLoginApi*apiInstance = [[SWGLoginApi alloc] init];

// Login a user via OAuth 2
[apiInstance loginGetWithClientId:clientId
              grantType:grantType
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              clientSecret:clientSecret
              password:password
              username:username
              nonce:nonce
          completionHandler: ^(SWGToken* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGLoginApi->loginGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **grantType** | **NSString***| Oauth2 grant type. We support standard flows: - authorization_code - client_credentials - password  and 2 custom:  - https://api.musement.com/grant/comment-nonce - https://api.musement.com/grant/purchase-experience-nonce  These last 2 must be used together with &#39;nonce&#39; parameter and allow to login a user using a nonce. | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **clientSecret** | **NSString***|  | [optional] 
 **password** | **NSString***| Password of the user we want to login. Used when &#39;gran_type&#39; is &#39;password&#39;. | [optional] 
 **username** | **NSString***| Username of the user we want to login. Used when &#39;gran_type&#39; is &#39;password&#39;. | [optional] 
 **nonce** | **NSString***| Nonce token used to authenticate the customer using a nonce in a mail or in a notification. | [optional] 

### Return type

[**SWGToken***](SWGToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **refreshGet**
```objc
-(NSURLSessionTask*) refreshGetWithGrantType: (NSString*) grantType
    refreshToken: (NSString*) refreshToken
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    clientId: (NSString*) clientId
    clientSecret: (NSString*) clientSecret
        completionHandler: (void (^)(SWGToken* output, NSError* error)) handler;
```

Retrieve a fresh access token

### Example 
```objc

NSString* grantType = @"grantType_example"; // 
NSString* refreshToken = @"refreshToken_example"; // 
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* clientId = @"user"; //  (optional) (default to user)
NSString* clientSecret = @"clientSecret_example"; //  (optional)

SWGLoginApi*apiInstance = [[SWGLoginApi alloc] init];

// Retrieve a fresh access token
[apiInstance refreshGetWithGrantType:grantType
              refreshToken:refreshToken
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              clientId:clientId
              clientSecret:clientSecret
          completionHandler: ^(SWGToken* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGLoginApi->refreshGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grantType** | **NSString***|  | 
 **refreshToken** | **NSString***|  | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **clientId** | **NSString***|  | [optional] [default to user]
 **clientSecret** | **NSString***|  | [optional] 

### Return type

[**SWGToken***](SWGToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resetPasswordPost**
```objc
-(NSURLSessionTask*) resetPasswordPostWithRecoverPassword: (SWGRecoverPassword*) recoverPassword
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Send reset password email. This method always returns 204

### Example 
```objc

SWGRecoverPassword* recoverPassword = [[SWGRecoverPassword alloc] init]; // Data to recover user's password
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGLoginApi*apiInstance = [[SWGLoginApi alloc] init];

// Send reset password email. This method always returns 204
[apiInstance resetPasswordPostWithRecoverPassword:recoverPassword
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGLoginApi->resetPasswordPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recoverPassword** | [**SWGRecoverPassword***](SWGRecoverPassword.md)| Data to recover user&#39;s password | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

