# SWGGiftboxTypeItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**giftboxTypePrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**numberOfPeople** | **NSNumber*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**reviewsNumber** | **NSNumber*** |  | [optional] 
**reviewsAvg** | **NSNumber*** |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**verticals** | [**NSArray&lt;SWGVertical&gt;***](SWGVertical.md) |  | [optional] 
**categories** | [**NSArray&lt;SWGCategory&gt;***](SWGCategory.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


