# SWGCart

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **NSString*** |  | [optional] 
**items** | [**NSArray&lt;SWGCartItem&gt;***](SWGCartItem.md) |  | [optional] 
**tickets** | [**NSArray&lt;SWGTicket&gt;***](SWGTicket.md) |  | [optional] 
**discountCode** | **NSString*** | This is used only when the cart is posted/put/patched. This can be a discount code or the code of a giftcard. | [optional] 
**giftCard** | [**SWGGiftCard***](SWGGiftCard.md) |  | [optional] 
**promoCode** | [**SWGPromoCode***](SWGPromoCode.md) |  | [optional] 
**customer** | [**SWGCustomer***](SWGCustomer.md) |  | [optional] 
**customerId** | **NSNumber*** |  | [optional] 
**fullPrice** | [**SWGPrice***](SWGPrice.md) | Cart price. Discount is not considered | [optional] 
**discount** | [**SWGPrice***](SWGPrice.md) | Cart discount | [optional] 
**retailPrice** | [**SWGPrice***](SWGPrice.md) | Cart price. This is the price the customer must pay. Discount, if present, is applied.  | [optional] 
**serviceFee** | [**SWGPrice***](SWGPrice.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


