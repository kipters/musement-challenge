# SWGCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**email** | **NSString*** |  | [optional] 
**firstname** | **NSString*** |  | [optional] 
**lastname** | **NSString*** |  | [optional] 
**avatar** | **NSString*** |  | [optional] 
**country** | [**SWGCountry***](SWGCountry.md) |  | [optional] 
**currency** | [**SWGCurrency***](SWGCurrency.md) |  | [optional] 
**birthdate** | **NSDate*** |  | [optional] 
**gender** | [**SWGCustomerGender***](SWGCustomerGender.md) |  | [optional] 
**idNumber** | **NSString*** |  | [optional] 
**mobile** | **NSString*** |  | [optional] 
**address** | **NSString*** |  | [optional] 
**favouriteCity** | [**SWGCity***](SWGCity.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


