# SWGCartRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**NSArray&lt;SWGTicketRequest&gt;***](SWGTicketRequest.md) |  | [optional] 
**discountCode** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


