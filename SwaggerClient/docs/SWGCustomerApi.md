# SWGCustomerApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customersMeCommentsGet**](SWGCustomerApi.md#customersmecommentsget) | **GET** /customers/me/comments | Get all comments for the customer
[**customersMeGet**](SWGCustomerApi.md#customersmeget) | **GET** /customers/me | Get logged user data
[**customersMeOrdersGet**](SWGCustomerApi.md#customersmeordersget) | **GET** /customers/me/orders | Get all orders for customer
[**customersMePatch**](SWGCustomerApi.md#customersmepatch) | **PATCH** /customers/me | Update customer data
[**customersMePut**](SWGCustomerApi.md#customersmeput) | **PUT** /customers/me | Update customer data. Set to empty not specified data


# **customersMeCommentsGet**
```objc
-(NSURLSessionTask*) customersMeCommentsGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGComment* output, NSError* error)) handler;
```

Get all comments for the customer

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCustomerApi*apiInstance = [[SWGCustomerApi alloc] init];

// Get all comments for the customer
[apiInstance customersMeCommentsGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGComment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerApi->customersMeCommentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGComment***](SWGComment.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMeGet**
```objc
-(NSURLSessionTask*) customersMeGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCustomer* output, NSError* error)) handler;
```

Get logged user data

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCustomerApi*apiInstance = [[SWGCustomerApi alloc] init];

// Get logged user data
[apiInstance customersMeGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerApi->customersMeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCustomer***](SWGCustomer.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMeOrdersGet**
```objc
-(NSURLSessionTask*) customersMeOrdersGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(NSArray<SWGOrder>* output, NSError* error)) handler;
```

Get all orders for customer

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCustomerApi*apiInstance = [[SWGCustomerApi alloc] init];

// Get all orders for customer
[apiInstance customersMeOrdersGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(NSArray<SWGOrder>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerApi->customersMeOrdersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**NSArray<SWGOrder>***](SWGOrder.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMePatch**
```objc
-(NSURLSessionTask*) customersMePatchWithCustomer: (SWGPatchCustomer*) customer
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCustomer* output, NSError* error)) handler;
```

Update customer data

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


SWGPatchCustomer* customer = [[SWGPatchCustomer alloc] init]; // Customer patch request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCustomerApi*apiInstance = [[SWGCustomerApi alloc] init];

// Update customer data
[apiInstance customersMePatchWithCustomer:customer
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerApi->customersMePatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | [**SWGPatchCustomer***](SWGPatchCustomer.md)| Customer patch request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCustomer***](SWGCustomer.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMePut**
```objc
-(NSURLSessionTask*) customersMePutWithCustomer: (SWGPutCustomer*) customer
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCustomer* output, NSError* error)) handler;
```

Update customer data. Set to empty not specified data

All data must be sent. Empty field will be cleared

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


SWGPutCustomer* customer = [[SWGPutCustomer alloc] init]; // Customer put request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCustomerApi*apiInstance = [[SWGCustomerApi alloc] init];

// Update customer data. Set to empty not specified data
[apiInstance customersMePutWithCustomer:customer
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerApi->customersMePut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | [**SWGPutCustomer***](SWGPutCustomer.md)| Customer put request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCustomer***](SWGCustomer.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

