# SWGNoPaymentApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsNoPaymentPost**](SWGNoPaymentApi.md#paymentsnopaymentpost) | **POST** /payments/no/payment | Pay an order with no-payment strategy


# **paymentsNoPaymentPost**
```objc
-(NSURLSessionTask*) paymentsNoPaymentPostWithNoPaymentPostOrder: (SWGPostNoPayment*) noPaymentPostOrder
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGOrder* output, NSError* error)) handler;
```

Pay an order with no-payment strategy

Apply the no-payment strategy for an order. You can use this strategy if the cart amount is zero or if you have the special `NoPaymentStrategy` grant

### Example 
```objc

SWGPostNoPayment* noPaymentPostOrder = [[SWGPostNoPayment alloc] init]; // No payment info
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGNoPaymentApi*apiInstance = [[SWGNoPaymentApi alloc] init];

// Pay an order with no-payment strategy
[apiInstance paymentsNoPaymentPostWithNoPaymentPostOrder:noPaymentPostOrder
              xMusementVersion:xMusementVersion
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGOrder* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGNoPaymentApi->paymentsNoPaymentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **noPaymentPostOrder** | [**SWGPostNoPayment***](SWGPostNoPayment.md)| No payment info | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGOrder***](SWGOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

