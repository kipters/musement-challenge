# SWGSupplierProfileApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersMeGet**](SWGSupplierProfileApi.md#suppliersmeget) | **GET** /suppliers/me | Get logged supplier data
[**suppliersMePatch**](SWGSupplierProfileApi.md#suppliersmepatch) | **PATCH** /suppliers/me | Update logged supplier data


# **suppliersMeGet**
```objc
-(NSURLSessionTask*) suppliersMeGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;
```

Get logged supplier data

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierProfileApi*apiInstance = [[SWGSupplierProfileApi alloc] init];

// Get logged supplier data
[apiInstance suppliersMeGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGSupplier* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierProfileApi->suppliersMeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGSupplier***](SWGSupplier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersMePatch**
```objc
-(NSURLSessionTask*) suppliersMePatchWithPatchSupplierRegistration: (SWGPatchSupplierProfile*) patchSupplierRegistration
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;
```

Update logged supplier data

### Example 
```objc

SWGPatchSupplierProfile* patchSupplierRegistration = [[SWGPatchSupplierProfile alloc] init]; // Supplier data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierProfileApi*apiInstance = [[SWGSupplierProfileApi alloc] init];

// Update logged supplier data
[apiInstance suppliersMePatchWithPatchSupplierRegistration:patchSupplierRegistration
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGSupplier* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierProfileApi->suppliersMePatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patchSupplierRegistration** | [**SWGPatchSupplierProfile***](SWGPatchSupplierProfile.md)| Supplier data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGSupplier***](SWGSupplier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

