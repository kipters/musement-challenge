# SWGPostPromoIntent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promo** | **NSNumber*** | The promo id | [optional] 
**triggeringProduct** | **NSNumber*** | The current event id | [optional] 
**targetProduct** | **NSNumber*** | The chosen event id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


