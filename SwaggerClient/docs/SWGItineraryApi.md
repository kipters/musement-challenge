# SWGItineraryApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidItinerariesGet**](SWGItineraryApi.md#activitiesactivityuuiditinerariesget) | **GET** /activities/{activityUuid}/itineraries | Get all itineraries for an activity
[**activitiesActivityUuidItinerariesPost**](SWGItineraryApi.md#activitiesactivityuuiditinerariespost) | **POST** /activities/{activityUuid}/itineraries | Add an itinerary to an activity


# **activitiesActivityUuidItinerariesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidItinerariesGetWithActivityUuid: (NSString*) activityUuid
        completionHandler: (void (^)(NSArray<SWGActivityItinerary>* output, NSError* error)) handler;
```

Get all itineraries for an activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier

SWGItineraryApi*apiInstance = [[SWGItineraryApi alloc] init];

// Get all itineraries for an activity
[apiInstance activitiesActivityUuidItinerariesGetWithActivityUuid:activityUuid
          completionHandler: ^(NSArray<SWGActivityItinerary>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGItineraryApi->activitiesActivityUuidItinerariesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 

### Return type

[**NSArray<SWGActivityItinerary>***](SWGActivityItinerary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidItinerariesPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidItinerariesPostWithItinerary: (SWGPostActivityItinerary*) itinerary
    activityUuid: (NSString*) activityUuid
        completionHandler: (void (^)(SWGActivityItinerary* output, NSError* error)) handler;
```

Add an itinerary to an activity

### Example 
```objc

SWGPostActivityItinerary* itinerary = [[SWGPostActivityItinerary alloc] init]; // Activity itinerary post request
NSString* activityUuid = @"activityUuid_example"; // Activity identifier

SWGItineraryApi*apiInstance = [[SWGItineraryApi alloc] init];

// Add an itinerary to an activity
[apiInstance activitiesActivityUuidItinerariesPostWithItinerary:itinerary
              activityUuid:activityUuid
          completionHandler: ^(SWGActivityItinerary* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGItineraryApi->activitiesActivityUuidItinerariesPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itinerary** | [**SWGPostActivityItinerary***](SWGPostActivityItinerary.md)| Activity itinerary post request | 
 **activityUuid** | **NSString***| Activity identifier | 

### Return type

[**SWGActivityItinerary***](SWGActivityItinerary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

