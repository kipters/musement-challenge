# SWGBundle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount** | **NSNumber*** |  | [optional] 
**discountType** | **NSString*** |  | [optional] 
**bundleType** | **NSString*** | on_everything|from_second|no_discount | [optional] 
**ruleId** | **NSString*** |  | [optional] 
**activities** | [**NSArray&lt;SWGEvent&gt;***](SWGEvent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


