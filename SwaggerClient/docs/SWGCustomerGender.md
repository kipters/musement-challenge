# SWGCustomerGender

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** | Customer&#39;s gender code. Possible values are:  * MALE: Male  * FEMALE: Female  * OTHER: Other | [optional] 
**name** | **NSString*** | Customer&#39;s gender label. | This value depends on the value of the header Accept-Language | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


