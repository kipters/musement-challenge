# SWGPricetagApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pricetagsIdGet**](SWGPricetagApi.md#pricetagsidget) | **GET** /pricetags/{id} | Get the name for a pricetag


# **pricetagsIdGet**
```objc
-(NSURLSessionTask*) pricetagsIdGetWithId: (NSNumber*) _id
        completionHandler: (void (^)(SWGPricetag* output, NSError* error)) handler;
```

Get the name for a pricetag

### Example 
```objc

NSNumber* _id = @56; // 

SWGPricetagApi*apiInstance = [[SWGPricetagApi alloc] init];

// Get the name for a pricetag
[apiInstance pricetagsIdGetWithId:_id
          completionHandler: ^(SWGPricetag* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPricetagApi->pricetagsIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSNumber***|  | 

### Return type

[**SWGPricetag***](SWGPricetag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

