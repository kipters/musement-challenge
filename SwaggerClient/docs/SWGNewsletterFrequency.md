# SWGNewsletterFrequency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |   * 3-TIMES-A-WEEK: Three newsletters a week,  * WEEKLY: A newsletter a week  * MONTHLY: A newsletter a month,  * NEVER: No newsletters are sent to the customer | [optional] 
**name** | **NSString*** | Customer&#39;s newsletter frequency name. | This value depends on the value of the header Accept-Language | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


