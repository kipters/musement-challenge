# SWGPostGiftProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | **NSString*** | Strategy for creation of this GiftProduct. Currently only supported is: FROM-GIFTBOX | [optional] 
**giftboxCode** | **NSString*** | Giftbox code - from which Giftbox this GiftProduct will be created | [optional] 
**giftboxTypeItemId** | **NSNumber*** | GiftboxTypeItem id - describes Activity chosen by user | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


