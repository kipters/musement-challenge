# SWGPostActivityContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSString*** | Title for the activity | [optional] 
**_description** | **NSString*** | Description for the activity | [optional] 
**when** | **NSString*** | When the activity happen | [optional] 
**operationalDays** | **NSString*** | On which days doesn the activity happen ? | [optional] 
**duration** | **NSString*** | Activity duration as ISO-8601 period. Only hours and minutes are accepted. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


