# SWGPostActivityMedia

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **NSString*** | Image URL | 
**title** | **NSString*** | Image title | [optional] 
**isCover** | **NSNumber*** | The image will be set as the cover. If a cover image is already set this will its place. | [optional] [default to @(NO)]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


