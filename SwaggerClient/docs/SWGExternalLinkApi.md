# SWGExternalLinkApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**externalLinksExternalLinkIdGet**](SWGExternalLinkApi.md#externallinksexternallinkidget) | **GET** /external-links/{externalLinkId} | Get an external link by ID
[**externalLinksGet**](SWGExternalLinkApi.md#externallinksget) | **GET** /external-links | Get a collection of external links


# **externalLinksExternalLinkIdGet**
```objc
-(NSURLSessionTask*) externalLinksExternalLinkIdGetWithExternalLinkId: (NSNumber*) externalLinkId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGExternalLink* output, NSError* error)) handler;
```

Get an external link by ID

### Example 
```objc

NSNumber* externalLinkId = @56; // External link identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGExternalLinkApi*apiInstance = [[SWGExternalLinkApi alloc] init];

// Get an external link by ID
[apiInstance externalLinksExternalLinkIdGetWithExternalLinkId:externalLinkId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGExternalLink* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGExternalLinkApi->externalLinksExternalLinkIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externalLinkId** | **NSNumber***| External link identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGExternalLink***](SWGExternalLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **externalLinksGet**
```objc
-(NSURLSessionTask*) externalLinksGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    countryIso2charIn: (NSArray<NSNumber*>*) countryIso2charIn
        completionHandler: (void (^)(NSArray<SWGExternalLink>* output, NSError* error)) handler;
```

Get a collection of external links

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSArray<NSNumber*>* countryIso2charIn = @[@56]; // Filter, include only results from given countries identified by a collection of codes (optional)

SWGExternalLinkApi*apiInstance = [[SWGExternalLinkApi alloc] init];

// Get a collection of external links
[apiInstance externalLinksGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              countryIso2charIn:countryIso2charIn
          completionHandler: ^(NSArray<SWGExternalLink>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGExternalLinkApi->externalLinksGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **countryIso2charIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given countries identified by a collection of codes | [optional] 

### Return type

[**NSArray<SWGExternalLink>***](SWGExternalLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

