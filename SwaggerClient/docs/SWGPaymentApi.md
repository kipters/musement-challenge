# SWGPaymentApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsPaypalPaymentPost**](SWGPaymentApi.md#paymentspaypalpaymentpost) | **POST** /payments/paypal/payment | Pay an Order.
[**paymentsStripeStoredSourceInfoGet**](SWGPaymentApi.md#paymentsstripestoredsourceinfoget) | **GET** /payments/stripe/stored_source_info | Get stored Stripe Source info


# **paymentsPaypalPaymentPost**
```objc
-(NSURLSessionTask*) paymentsPaypalPaymentPostWithPaypalPayment: (SWGPaypalPaymentRequest*) paypalPayment
        completionHandler: (void (^)(SWGPaypalSuccessfulPayment* output, NSError* error)) handler;
```

Pay an Order.

Pay an Order using Paypal Payment Id.

### Example 
```objc

SWGPaypalPaymentRequest* paypalPayment = [[SWGPaypalPaymentRequest alloc] init]; // Paypal payment info request

SWGPaymentApi*apiInstance = [[SWGPaymentApi alloc] init];

// Pay an Order.
[apiInstance paymentsPaypalPaymentPostWithPaypalPayment:paypalPayment
          completionHandler: ^(SWGPaypalSuccessfulPayment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPaymentApi->paymentsPaypalPaymentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paypalPayment** | [**SWGPaypalPaymentRequest***](SWGPaypalPaymentRequest.md)| Paypal payment info request | 

### Return type

[**SWGPaypalSuccessfulPayment***](SWGPaypalSuccessfulPayment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **paymentsStripeStoredSourceInfoGet**
```objc
-(NSURLSessionTask*) paymentsStripeStoredSourceInfoGetWithCompletionHandler: 
        (void (^)(NSError* error)) handler;
```

Get stored Stripe Source info

### Example 
```objc


SWGPaymentApi*apiInstance = [[SWGPaymentApi alloc] init];

// Get stored Stripe Source info
[apiInstance paymentsStripeStoredSourceInfoGetWithCompletionHandler: 
          ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGPaymentApi->paymentsStripeStoredSourceInfoGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

