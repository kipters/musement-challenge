# SWGWish

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | [optional] 
**itemId** | **NSNumber*** |  | [optional] 
**itemType** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


