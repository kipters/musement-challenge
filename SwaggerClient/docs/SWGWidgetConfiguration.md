# SWGWidgetConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**widgetType** | **NSString*** |  | [optional] 
**modal** | **NSNumber*** |  | [optional] 
**entityIds** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**entityType** | **NSString*** |  | [optional] 
**customCss** | **NSString*** |  | [optional] 
**currency** | **NSString*** |  | [optional] 
**locale** | **NSString*** |  | [optional] 
**steps** | [**SWGWidgetConfigurationSteps***](SWGWidgetConfigurationSteps.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


