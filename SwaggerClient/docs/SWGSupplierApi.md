# SWGSupplierApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersGet**](SWGSupplierApi.md#suppliersget) | **GET** /suppliers | Get all active suppliers
[**suppliersSupplierUuidGet**](SWGSupplierApi.md#supplierssupplieruuidget) | **GET** /suppliers/{supplierUuid} | Get supplier data
[**suppliersSupplierUuidPatch**](SWGSupplierApi.md#supplierssupplieruuidpatch) | **PATCH** /suppliers/{supplierUuid} | Update supplier data


# **suppliersGet**
```objc
-(NSURLSessionTask*) suppliersGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all active suppliers

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierApi*apiInstance = [[SWGSupplierApi alloc] init];

// Get all active suppliers
[apiInstance suppliersGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierApi->suppliersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidGet**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;
```

Get supplier data

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierApi*apiInstance = [[SWGSupplierApi alloc] init];

// Get supplier data
[apiInstance suppliersSupplierUuidGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGSupplier* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierApi->suppliersSupplierUuidGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGSupplier***](SWGSupplier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidPatch**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidPatchWithPatchSupplierRegistration: (SWGPatchSupplierProfile*) patchSupplierRegistration
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;
```

Update supplier data

### Example 
```objc

SWGPatchSupplierProfile* patchSupplierRegistration = [[SWGPatchSupplierProfile alloc] init]; // Supplier registrations data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierApi*apiInstance = [[SWGSupplierApi alloc] init];

// Update supplier data
[apiInstance suppliersSupplierUuidPatchWithPatchSupplierRegistration:patchSupplierRegistration
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGSupplier* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierApi->suppliersSupplierUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patchSupplierRegistration** | [**SWGPatchSupplierProfile***](SWGPatchSupplierProfile.md)| Supplier registrations data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGSupplier***](SWGSupplier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

