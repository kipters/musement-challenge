# SWGTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **NSString*** | Ticket unique identifier | [optional] 
**cartId** | **NSNumber*** |  | [optional] 
**product** | [**SWGProduct***](SWGProduct.md) |  | [optional] 
**code** | **NSString*** |  | [optional] 
**quantity** | **NSNumber*** |  | [optional] 
**totalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**supplierTotalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**passengersInfo** | [**NSArray&lt;SWGPassengerInfo&gt;***](SWGPassengerInfo.md) |  | [optional] 
**metadata** | [**SWGTicketMetadata***](SWGTicketMetadata.md) |  | [optional] 
**status** | **NSString*** |  | [optional] 
**vouchers** | **NSArray&lt;NSString*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


