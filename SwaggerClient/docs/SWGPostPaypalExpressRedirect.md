# SWGPostPaypalExpressRedirect

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **NSString*** | Uuid or id of Order that payment will be for | [optional] 
**returnUrl** | **NSString*** | Absolute url. Here Paypal will redirect after user completes payment successfully on their site (paypal.com). You can add some query params if you want. Our Api action will add &#39;order_uuid&#39; to this url so when redirected back we know what Order is this about. Paypal will add &#39;token&#39; and &#39;PayerID&#39; query params to end of url: &#39;token&#x3D;EC-8UW872335L727771H&amp;PayerID&#x3D;CLRRGZHMA9LZJ&#39; | [optional] 
**cancelUrl** | **NSString*** | Absolute url. Similar as return_url but will be used if user clicks &#39;cancel&#39; on paypal site - so payment is not completed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


