# SWGPatchComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **NSString*** | Locale of the content of the message | [optional] 
**title** | **NSString*** | Title | [optional] 
**text** | **NSString*** | Text | [optional] 
**rating** | **NSNumber*** | Rating | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


