# SWGSupplierInvoicesApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersMeInvoicesGet**](SWGSupplierInvoicesApi.md#suppliersmeinvoicesget) | **GET** /suppliers/me/invoices | Get supplier invoices
[**suppliersSupplierUuidInvoicesGet**](SWGSupplierInvoicesApi.md#supplierssupplieruuidinvoicesget) | **GET** /suppliers/{supplierUuid}/invoices | Get invoices for a specific supplier


# **suppliersMeInvoicesGet**
```objc
-(NSURLSessionTask*) suppliersMeInvoicesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    page: (NSNumber*) page
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGInvoice>* output, NSError* error)) handler;
```

Get supplier invoices

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGSupplierInvoicesApi*apiInstance = [[SWGSupplierInvoicesApi alloc] init];

// Get supplier invoices
[apiInstance suppliersMeInvoicesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              page:page
              limit:limit
          completionHandler: ^(NSArray<SWGInvoice>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierInvoicesApi->suppliersMeInvoicesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGInvoice>***](SWGInvoice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidInvoicesGet**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidInvoicesGetWithSupplierUuid: (NSString*) supplierUuid
    page: (NSNumber*) page
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGInvoice>* output, NSError* error)) handler;
```

Get invoices for a specific supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGSupplierInvoicesApi*apiInstance = [[SWGSupplierInvoicesApi alloc] init];

// Get invoices for a specific supplier
[apiInstance suppliersSupplierUuidInvoicesGetWithSupplierUuid:supplierUuid
              page:page
              limit:limit
          completionHandler: ^(NSArray<SWGInvoice>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierInvoicesApi->suppliersSupplierUuidInvoicesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGInvoice>***](SWGInvoice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

