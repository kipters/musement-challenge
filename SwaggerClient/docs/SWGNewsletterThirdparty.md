# SWGNewsletterThirdparty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |   * YES: Yes  * NO: No | [optional] 
**name** | **NSString*** | Customer&#39;s newsletter from thirdparty name. | This value depends on the value of the header Accept-Language | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


