# SWGPostActivitySeasonSetup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **NSString*** | Time slot type. Can be &#39;WITH-TIMESLOTS&#39; or &#39;DAILY&#39;  | [optional] 
**title** | **NSString*** | Internal name that identify the timeslot | [optional] 
**days** | **NSArray&lt;NSString*&gt;*** | Days | [optional] 
**featureCode** | **NSString*** | Price features codes | Get them using GET /suppliers/me/price-features | [optional] 
**prices** | [**NSArray&lt;SWGPostActivitySeasonSetupPrice&gt;***](SWGPostActivitySeasonSetupPrice.md) | Prices | [optional] 
**timeslots** | [**NSArray&lt;SWGPostActivitySeasonSetupTimeslot&gt;***](SWGPostActivitySeasonSetupTimeslot.md) | Timeslots | [optional] 
**languages** | **NSArray&lt;NSString*&gt;*** | A list of languages the activity is available in | [optional] 
**minPurchasableQty** | **NSNumber*** | Min purchasable quantity | [optional] 
**maxPurchasableQty** | **NSNumber*** | Max purchasable quantity | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


