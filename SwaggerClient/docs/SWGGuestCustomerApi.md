# SWGGuestCustomerApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cartsCartUuidCustomerPatch**](SWGGuestCustomerApi.md#cartscartuuidcustomerpatch) | **PATCH** /carts/{cartUuid}/customer | Patch cart customer data


# **cartsCartUuidCustomerPatch**
```objc
-(NSURLSessionTask*) cartsCartUuidCustomerPatchWithCartUuid: (NSString*) cartUuid
    patchCustomerGuestCart: (SWGPatchCustomerGuestCart*) patchCustomerGuestCart
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGCustomer* output, NSError* error)) handler;
```

Patch cart customer data

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
SWGPatchCustomerGuestCart* patchCustomerGuestCart = [[SWGPatchCustomerGuestCart alloc] init]; // Customer data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGGuestCustomerApi*apiInstance = [[SWGGuestCustomerApi alloc] init];

// Patch cart customer data
[apiInstance cartsCartUuidCustomerPatchWithCartUuid:cartUuid
              patchCustomerGuestCart:patchCustomerGuestCart
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGuestCustomerApi->cartsCartUuidCustomerPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **patchCustomerGuestCart** | [**SWGPatchCustomerGuestCart***](SWGPatchCustomerGuestCart.md)| Customer data | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGCustomer***](SWGCustomer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

