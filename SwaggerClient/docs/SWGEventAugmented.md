# SWGEventAugmented

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isHero** | **NSNumber*** | Is hero ? | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


