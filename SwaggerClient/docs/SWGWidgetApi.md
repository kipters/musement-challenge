# SWGWidgetApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**affiliatesMeWidgetsGet**](SWGWidgetApi.md#affiliatesmewidgetsget) | **GET** /affiliates/me/widgets | Get a widget collection for the current user
[**affiliatesMeWidgetsPost**](SWGWidgetApi.md#affiliatesmewidgetspost) | **POST** /affiliates/me/widgets | Create a new widget
[**affiliatesMeWidgetsWidgetCodePatch**](SWGWidgetApi.md#affiliatesmewidgetswidgetcodepatch) | **PATCH** /affiliates/me/widgets/{widgetCode} | Update a widget
[**affiliatesSupplierUuidWidgetsGet**](SWGWidgetApi.md#affiliatessupplieruuidwidgetsget) | **GET** /affiliates/{supplierUuid}/widgets | Get a widget collection by supplier uuid
[**affiliatesSupplierUuidWidgetsPost**](SWGWidgetApi.md#affiliatessupplieruuidwidgetspost) | **POST** /affiliates/{supplierUuid}/widgets | Create a new widget for the specified supplier uuid
[**widgetsWidgetCodeGet**](SWGWidgetApi.md#widgetswidgetcodeget) | **GET** /widgets/{widgetCode} | Get a widget configuration by code


# **affiliatesMeWidgetsGet**
```objc
-(NSURLSessionTask*) affiliatesMeWidgetsGetWithXMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(NSArray<SWGWidget>* output, NSError* error)) handler;
```

Get a widget collection for the current user

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGWidgetApi*apiInstance = [[SWGWidgetApi alloc] init];

// Get a widget collection for the current user
[apiInstance affiliatesMeWidgetsGetWithXMusementVersion:xMusementVersion
          completionHandler: ^(NSArray<SWGWidget>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWidgetApi->affiliatesMeWidgetsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**NSArray<SWGWidget>***](SWGWidget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **affiliatesMeWidgetsPost**
```objc
-(NSURLSessionTask*) affiliatesMeWidgetsPostWithXMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGWidget* output, NSError* error)) handler;
```

Create a new widget

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGWidgetApi*apiInstance = [[SWGWidgetApi alloc] init];

// Create a new widget
[apiInstance affiliatesMeWidgetsPostWithXMusementVersion:xMusementVersion
          completionHandler: ^(SWGWidget* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWidgetApi->affiliatesMeWidgetsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGWidget***](SWGWidget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **affiliatesMeWidgetsWidgetCodePatch**
```objc
-(NSURLSessionTask*) affiliatesMeWidgetsWidgetCodePatchWithWidgetCode: (NSString*) widgetCode
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGWidget* output, NSError* error)) handler;
```

Update a widget

### Example 
```objc

NSString* widgetCode = @"widgetCode_example"; // Widget Code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGWidgetApi*apiInstance = [[SWGWidgetApi alloc] init];

// Update a widget
[apiInstance affiliatesMeWidgetsWidgetCodePatchWithWidgetCode:widgetCode
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGWidget* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWidgetApi->affiliatesMeWidgetsWidgetCodePatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widgetCode** | **NSString***| Widget Code | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGWidget***](SWGWidget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **affiliatesSupplierUuidWidgetsGet**
```objc
-(NSURLSessionTask*) affiliatesSupplierUuidWidgetsGetWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(NSArray<SWGWidget>* output, NSError* error)) handler;
```

Get a widget collection by supplier uuid

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGWidgetApi*apiInstance = [[SWGWidgetApi alloc] init];

// Get a widget collection by supplier uuid
[apiInstance affiliatesSupplierUuidWidgetsGetWithSupplierUuid:supplierUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(NSArray<SWGWidget>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWidgetApi->affiliatesSupplierUuidWidgetsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**NSArray<SWGWidget>***](SWGWidget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **affiliatesSupplierUuidWidgetsPost**
```objc
-(NSURLSessionTask*) affiliatesSupplierUuidWidgetsPostWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGWidget* output, NSError* error)) handler;
```

Create a new widget for the specified supplier uuid

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGWidgetApi*apiInstance = [[SWGWidgetApi alloc] init];

// Create a new widget for the specified supplier uuid
[apiInstance affiliatesSupplierUuidWidgetsPostWithSupplierUuid:supplierUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGWidget* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWidgetApi->affiliatesSupplierUuidWidgetsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGWidget***](SWGWidget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **widgetsWidgetCodeGet**
```objc
-(NSURLSessionTask*) widgetsWidgetCodeGetWithWidgetCode: (NSString*) widgetCode
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGWidget* output, NSError* error)) handler;
```

Get a widget configuration by code

### Example 
```objc

NSString* widgetCode = @"widgetCode_example"; // Widget Code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGWidgetApi*apiInstance = [[SWGWidgetApi alloc] init];

// Get a widget configuration by code
[apiInstance widgetsWidgetCodeGetWithWidgetCode:widgetCode
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGWidget* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGWidgetApi->widgetsWidgetCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widgetCode** | **NSString***| Widget Code | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGWidget***](SWGWidget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

