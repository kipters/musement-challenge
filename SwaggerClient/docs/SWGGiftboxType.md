# SWGGiftboxType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**relevance** | **NSNumber*** |  | [optional] 
**level** | [**SWGGiftboxTypeLevel***](SWGGiftboxTypeLevel.md) |  | [optional] 
**price** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**vertical** | [**SWGVertical***](SWGVertical.md) |  | [optional] 
**title** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**extendedDescription** | **NSString*** |  | [optional] 
**metaDescription** | **NSString*** |  | [optional] 
**seoTitle** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


