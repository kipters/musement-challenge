# SWGDeprecatedApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidRelatedActivitiesGet**](SWGDeprecatedApi.md#activitiesactivityuuidrelatedactivitiesget) | **GET** /activities/{activityUuid}/related-activities | Get related activities
[**eventsSearchExtendedGet**](SWGDeprecatedApi.md#eventssearchextendedget) | **GET** /events/search-extended | Deprecated API for search. Use GET /activities
[**eventsSearchGet**](SWGDeprecatedApi.md#eventssearchget) | **GET** /events/search | Deprecated API for search - Use GET /activities
[**suggestGet**](SWGDeprecatedApi.md#suggestget) | **GET** /suggest | Search relevant items in Musement DB


# **activitiesActivityUuidRelatedActivitiesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidRelatedActivitiesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Get related activities

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGDeprecatedApi*apiInstance = [[SWGDeprecatedApi alloc] init];

// Get related activities
[apiInstance activitiesActivityUuidRelatedActivitiesGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDeprecatedApi->activitiesActivityUuidRelatedActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsSearchExtendedGet**
```objc
-(NSURLSessionTask*) eventsSearchExtendedGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    q: (NSString*) q
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    verticalCollection: (NSArray<NSString*>*) verticalCollection
    categoryCollection: (NSArray<NSString*>*) categoryCollection
    countryCollection: (NSArray<NSString*>*) countryCollection
    cityCollection: (NSArray<NSString*>*) cityCollection
    minPrice: (NSString*) minPrice
    maxPrice: (NSString*) maxPrice
    topFeature: (NSString*) topFeature
    sorting: (NSString*) sorting
        completionHandler: (void (^)(SWGSearchResponse* output, NSError* error)) handler;
```

Deprecated API for search. Use GET /activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* q = @"q_example"; // Query String (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSArray<NSString*>* verticalCollection = @[@"verticalCollection_example"]; // List of verticals (optional)
NSArray<NSString*>* categoryCollection = @[@"categoryCollection_example"]; // List of categories (optional)
NSArray<NSString*>* countryCollection = @[@"countryCollection_example"]; // List of countries (optional)
NSArray<NSString*>* cityCollection = @[@"cityCollection_example"]; // List of cities (optional)
NSString* minPrice = @"minPrice_example"; // Minimum price for an event to be considered (optional)
NSString* maxPrice = @"maxPrice_example"; // Maximum price for an event to be considered (optional)
NSString* topFeature = @"topFeature_example"; // Query String (optional)
NSString* sorting = @"relevance"; // Sorting strategy (optional) (default to relevance)

SWGDeprecatedApi*apiInstance = [[SWGDeprecatedApi alloc] init];

// Deprecated API for search. Use GET /activities
[apiInstance eventsSearchExtendedGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              q:q
              offset:offset
              limit:limit
              dateFrom:dateFrom
              dateTo:dateTo
              verticalCollection:verticalCollection
              categoryCollection:categoryCollection
              countryCollection:countryCollection
              cityCollection:cityCollection
              minPrice:minPrice
              maxPrice:maxPrice
              topFeature:topFeature
              sorting:sorting
          completionHandler: ^(SWGSearchResponse* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDeprecatedApi->eventsSearchExtendedGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **q** | **NSString***| Query String | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **verticalCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of verticals | [optional] 
 **categoryCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of categories | [optional] 
 **countryCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of countries | [optional] 
 **cityCollection** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of cities | [optional] 
 **minPrice** | **NSString***| Minimum price for an event to be considered | [optional] 
 **maxPrice** | **NSString***| Maximum price for an event to be considered | [optional] 
 **topFeature** | **NSString***| Query String | [optional] 
 **sorting** | **NSString***| Sorting strategy | [optional] [default to relevance]

### Return type

[**SWGSearchResponse***](SWGSearchResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsSearchGet**
```objc
-(NSURLSessionTask*) eventsSearchGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(NSError* error)) handler;
```

Deprecated API for search - Use GET /activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGDeprecatedApi*apiInstance = [[SWGDeprecatedApi alloc] init];

// Deprecated API for search - Use GET /activities
[apiInstance eventsSearchGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              limit:limit
              offset:offset
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGDeprecatedApi->eventsSearchGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suggestGet**
```objc
-(NSURLSessionTask*) suggestGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    q: (NSString*) q
        completionHandler: (void (^)(NSError* error)) handler;
```

Search relevant items in Musement DB

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* q = @"q_example"; // Search string. Minimum 3 chars (optional)

SWGDeprecatedApi*apiInstance = [[SWGDeprecatedApi alloc] init];

// Search relevant items in Musement DB
[apiInstance suggestGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              q:q
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGDeprecatedApi->suggestGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **q** | **NSString***| Search string. Minimum 3 chars | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

