# SWGPromoIntent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **NSString*** |  | [optional] 
**cart** | **NSNumber*** |  | [optional] 
**promo** | **NSNumber*** |  | [optional] 
**triggeringProduct** | **NSString*** |  | [optional] 
**targetProduct** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


