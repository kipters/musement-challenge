# SWGSearchApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesGet**](SWGSearchApi.md#activitiesget) | **GET** /activities | Activities
[**activitiesRelatedGet**](SWGSearchApi.md#activitiesrelatedget) | **GET** /activities-related | Activities related search entrypoint


# **activitiesGet**
```objc
-(NSURLSessionTask*) activitiesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    text: (NSString*) text
    textOperator: (NSString*) textOperator
    extendOtherLanguages: (NSString*) extendOtherLanguages
    extendContentFields: (NSString*) extendContentFields
    fuzzinessLevel: (NSString*) fuzzinessLevel
    zeroTermsQuery: (NSString*) zeroTermsQuery
    sortBy: (NSArray<NSString*>*) sortBy
    categoryIn: (NSArray<NSString*>*) categoryIn
    cityIn: (NSArray<NSNumber*>*) cityIn
    coordinates: (NSString*) coordinates
    countryIn: (NSArray<NSString*>*) countryIn
    defaultPriceRange: (NSString*) defaultPriceRange
    distance: (NSString*) distance
    featureIn: (NSArray<NSString*>*) featureIn
    temporary: (NSNumber*) temporary
    venueIn: (NSArray<NSNumber*>*) venueIn
    verticalIn: (NSArray<NSString*>*) verticalIn
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(SWGActivitySearchResult* output, NSError* error)) handler;
```

Activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* text = @"text_example"; // Text to search (optional)
NSString* textOperator = @"AUTO"; // Represents text operator. If `AND` results will contains all of the words, if `OR` at least one, if `AUTO` performs a `AND` search first and an `OR` then (optional) (default to AUTO)
NSString* extendOtherLanguages = @"AUTO"; // If set to `YES` will extend the search to other language than the one specified in `Accept-Language` header, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* extendContentFields = @"AUTO"; // If set to `YES` will extend the search to other textual field other than the title, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* fuzzinessLevel = @"LEVEL-0"; // Level of fuzziness (optional) (default to LEVEL-0)
NSString* zeroTermsQuery = @"NONE"; // If set to `ALL`, if all of the stop words have been removed, search will be performed, if set to 'NONE' will not (optional) (default to NONE)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Set sorting criteria. Add `-` to invert sorting direction. When `distance` is specified also `coordinates` parameter must be set. (optional)
NSArray<NSString*>* categoryIn = @[@"categoryIn_example"]; // Filter by category | Category code need to be passed (optional)
NSArray<NSNumber*>* cityIn = @[@56]; // Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
NSString* coordinates = @"coordinates_example"; // Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
NSArray<NSString*>* countryIn = @[@"countryIn_example"]; // Filter, include only results from given countries identified by a collection of 2 chars country code (optional)
NSString* defaultPriceRange = @"defaultPriceRange_example"; // Price range as comma separated values, accepts only floats positive or equals to 0, two points precision | Currency is inferred from X-Musement-Currency header | Example: 0.00,34.23 (optional)
NSString* distance = @"distance_example"; // Distance from given coordinates expressed with an integer followed by unit | `KM` for kilometers, `M` for miles | Requires the other parameters called `coordinates` to be set in the same request | Example: 334KM (optional)
NSArray<NSString*>* featureIn = @[@"featureIn_example"]; // Filter, include only results having at least one of the given features identified by a collection of codes (optional)
NSNumber* temporary = @true; // Filter, include results on an temporary flag basis. Accepted values: YES or NOT (optional)
NSArray<NSNumber*>* venueIn = @[@56]; // Filter, include only results from given verticals identified by a collection of ids (optional)
NSArray<NSString*>* verticalIn = @[@"verticalIn_example"]; // Filter, include only results from given verticals identified by a collection of literal codes (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGSearchApi*apiInstance = [[SWGSearchApi alloc] init];

// Activities
[apiInstance activitiesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              text:text
              textOperator:textOperator
              extendOtherLanguages:extendOtherLanguages
              extendContentFields:extendContentFields
              fuzzinessLevel:fuzzinessLevel
              zeroTermsQuery:zeroTermsQuery
              sortBy:sortBy
              categoryIn:categoryIn
              cityIn:cityIn
              coordinates:coordinates
              countryIn:countryIn
              defaultPriceRange:defaultPriceRange
              distance:distance
              featureIn:featureIn
              temporary:temporary
              venueIn:venueIn
              verticalIn:verticalIn
              limit:limit
              offset:offset
          completionHandler: ^(SWGActivitySearchResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSearchApi->activitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **text** | **NSString***| Text to search | [optional] 
 **textOperator** | **NSString***| Represents text operator. If &#x60;AND&#x60; results will contains all of the words, if &#x60;OR&#x60; at least one, if &#x60;AUTO&#x60; performs a &#x60;AND&#x60; search first and an &#x60;OR&#x60; then | [optional] [default to AUTO]
 **extendOtherLanguages** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other language than the one specified in &#x60;Accept-Language&#x60; header, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **extendContentFields** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other textual field other than the title, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **fuzzinessLevel** | **NSString***| Level of fuzziness | [optional] [default to LEVEL-0]
 **zeroTermsQuery** | **NSString***| If set to &#x60;ALL&#x60;, if all of the stop words have been removed, search will be performed, if set to &#39;NONE&#39; will not | [optional] [default to NONE]
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Set sorting criteria. Add &#x60;-&#x60; to invert sorting direction. When &#x60;distance&#x60; is specified also &#x60;coordinates&#x60; parameter must be set. | [optional] 
 **categoryIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter by category | Category code need to be passed | [optional] 
 **cityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given cities identified by a collection of ids | [optional] 
 **coordinates** | **NSString***| Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 | [optional] 
 **countryIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results from given countries identified by a collection of 2 chars country code | [optional] 
 **defaultPriceRange** | **NSString***| Price range as comma separated values, accepts only floats positive or equals to 0, two points precision | Currency is inferred from X-Musement-Currency header | Example: 0.00,34.23 | [optional] 
 **distance** | **NSString***| Distance from given coordinates expressed with an integer followed by unit | &#x60;KM&#x60; for kilometers, &#x60;M&#x60; for miles | Requires the other parameters called &#x60;coordinates&#x60; to be set in the same request | Example: 334KM | [optional] 
 **featureIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results having at least one of the given features identified by a collection of codes | [optional] 
 **temporary** | **NSNumber***| Filter, include results on an temporary flag basis. Accepted values: YES or NOT | [optional] 
 **venueIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given verticals identified by a collection of ids | [optional] 
 **verticalIn** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Filter, include only results from given verticals identified by a collection of literal codes | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

[**SWGActivitySearchResult***](SWGActivitySearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesRelatedGet**
```objc
-(NSURLSessionTask*) activitiesRelatedGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    text: (NSString*) text
    textOperator: (NSString*) textOperator
    extendOtherLanguages: (NSString*) extendOtherLanguages
    extendContentFields: (NSString*) extendContentFields
    fuzzinessLevel: (NSString*) fuzzinessLevel
    zeroTermsQuery: (NSString*) zeroTermsQuery
    sortBy: (NSArray<NSString*>*) sortBy
    coordinates: (NSString*) coordinates
    minimumEvents: (NSNumber*) minimumEvents
    cityLimit: (NSNumber*) cityLimit
    cityOffset: (NSNumber*) cityOffset
    listLimit: (NSNumber*) listLimit
    listOffset: (NSNumber*) listOffset
    venueLimit: (NSNumber*) venueLimit
    venueOffset: (NSNumber*) venueOffset
        completionHandler: (void (^)(NSArray<SWGActivityRelatedResult>* output, NSError* error)) handler;
```

Activities related search entrypoint

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* text = @"text_example"; // Text to search (optional)
NSString* textOperator = @"AUTO"; // Represents text operator. If `AND` results will contains all of the words, if `OR` at least one, if `AUTO` performs a `AND` search first and an `OR` then (optional) (default to AUTO)
NSString* extendOtherLanguages = @"AUTO"; // If set to `YES` will extend the search to other language than the one specified in `Accept-Language` header, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* extendContentFields = @"AUTO"; // If set to `YES` will extend the search to other textual field other than the title, if set to `AUTO` will do this automatically on 0 results (optional) (default to AUTO)
NSString* fuzzinessLevel = @"LEVEL-0"; // Level of fuzziness (optional) (default to LEVEL-0)
NSString* zeroTermsQuery = @"NONE"; // If set to `ALL`, if all of the stop words have been removed, search will be performed, if set to 'NONE' will not (optional) (default to NONE)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria to apply, prepend `-` for descending order (optional)
NSString* coordinates = @"coordinates_example"; // Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 (optional)
NSNumber* minimumEvents = @56; // Minimum number of active events that must belong to the container subject, default 1 (optional)
NSNumber* cityLimit = @56; // Maximum number of cities to return (optional)
NSNumber* cityOffset = @56; // Offset for cities to return (optional)
NSNumber* listLimit = @56; // Maximum number of lists to return (optional)
NSNumber* listOffset = @56; // Offset for lists to return (optional)
NSNumber* venueLimit = @56; // Maximum number of venues to return (optional)
NSNumber* venueOffset = @56; // Offset for venues to return (optional)

SWGSearchApi*apiInstance = [[SWGSearchApi alloc] init];

// Activities related search entrypoint
[apiInstance activitiesRelatedGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              text:text
              textOperator:textOperator
              extendOtherLanguages:extendOtherLanguages
              extendContentFields:extendContentFields
              fuzzinessLevel:fuzzinessLevel
              zeroTermsQuery:zeroTermsQuery
              sortBy:sortBy
              coordinates:coordinates
              minimumEvents:minimumEvents
              cityLimit:cityLimit
              cityOffset:cityOffset
              listLimit:listLimit
              listOffset:listOffset
              venueLimit:venueLimit
              venueOffset:venueOffset
          completionHandler: ^(NSArray<SWGActivityRelatedResult>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSearchApi->activitiesRelatedGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **text** | **NSString***| Text to search | [optional] 
 **textOperator** | **NSString***| Represents text operator. If &#x60;AND&#x60; results will contains all of the words, if &#x60;OR&#x60; at least one, if &#x60;AUTO&#x60; performs a &#x60;AND&#x60; search first and an &#x60;OR&#x60; then | [optional] [default to AUTO]
 **extendOtherLanguages** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other language than the one specified in &#x60;Accept-Language&#x60; header, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **extendContentFields** | **NSString***| If set to &#x60;YES&#x60; will extend the search to other textual field other than the title, if set to &#x60;AUTO&#x60; will do this automatically on 0 results | [optional] [default to AUTO]
 **fuzzinessLevel** | **NSString***| Level of fuzziness | [optional] [default to LEVEL-0]
 **zeroTermsQuery** | **NSString***| If set to &#x60;ALL&#x60;, if all of the stop words have been removed, search will be performed, if set to &#39;NONE&#39; will not | [optional] [default to NONE]
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria to apply, prepend &#x60;-&#x60; for descending order | [optional] 
 **coordinates** | **NSString***| Coordinates as comma separated longitude and latitude | Example: 45.7386,-9.3641 | [optional] 
 **minimumEvents** | **NSNumber***| Minimum number of active events that must belong to the container subject, default 1 | [optional] 
 **cityLimit** | **NSNumber***| Maximum number of cities to return | [optional] 
 **cityOffset** | **NSNumber***| Offset for cities to return | [optional] 
 **listLimit** | **NSNumber***| Maximum number of lists to return | [optional] 
 **listOffset** | **NSNumber***| Offset for lists to return | [optional] 
 **venueLimit** | **NSNumber***| Maximum number of venues to return | [optional] 
 **venueOffset** | **NSNumber***| Offset for venues to return | [optional] 

### Return type

[**NSArray<SWGActivityRelatedResult>***](SWGActivityRelatedResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

