# SWGCityApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**citiesCityIdActivitiesGet**](SWGCityApi.md#citiescityidactivitiesget) | **GET** /cities/{cityId}/activities | Search activities within a given city
[**citiesCityIdActivitiesHappeningGet**](SWGCityApi.md#citiescityidactivitieshappeningget) | **GET** /cities/{cityId}/activities/{happening} | Search activities within a given city happening today or tomorrow
[**citiesCityIdBlogpostsGet**](SWGCityApi.md#citiescityidblogpostsget) | **GET** /cities/{cityId}/blogposts | Get the blogposts for the city
[**citiesCityIdCategoriesGet**](SWGCityApi.md#citiescityidcategoriesget) | **GET** /cities/{cityId}/categories | Categories for the city. Sorted by the number of events for the category
[**citiesCityIdGet**](SWGCityApi.md#citiescityidget) | **GET** /cities/{cityId} | Get city by unique identifier
[**citiesCityIdListsGet**](SWGCityApi.md#citiescityidlistsget) | **GET** /cities/{cityId}/lists | Get lists for a city
[**citiesCityIdVenuesGet**](SWGCityApi.md#citiescityidvenuesget) | **GET** /cities/{cityId}/venues | Get all venues for a city
[**citiesCityIdVerticalsGet**](SWGCityApi.md#citiescityidverticalsget) | **GET** /cities/{cityId}/verticals | Get all verticals for a city
[**citiesCityIdVerticalsVerticalIdCategoriesGet**](SWGCityApi.md#citiescityidverticalsverticalidcategoriesget) | **GET** /cities/{cityId}/verticals/{verticalId}/categories | Categories for the city for a specific vertical. Sorted by the number of activities for the category
[**citiesGet**](SWGCityApi.md#citiesget) | **GET** /cities | Get cities
[**countriesCountryIdCitiesGet**](SWGCityApi.md#countriescountryidcitiesget) | **GET** /countries/{countryId}/cities | Get all cities for a country


# **citiesCityIdActivitiesGet**
```objc
-(NSURLSessionTask*) citiesCityIdActivitiesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    category: (NSNumber*) category
    venue: (NSNumber*) venue
    editorialCategory: (NSNumber*) editorialCategory
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given city

Get all activities for an city. Only the activities that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* venue = @56; // Venue identifier (optional)
NSNumber* editorialCategory = @56; // Editorial category identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"venue-relevance"; // Sorting strategy (optional) (default to venue-relevance)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Search activities within a given city
[apiInstance citiesCityIdActivitiesGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              category:category
              venue:venue
              editorialCategory:editorialCategory
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **category** | **NSNumber***| Category identifier | [optional] 
 **venue** | **NSNumber***| Venue identifier | [optional] 
 **editorialCategory** | **NSNumber***| Editorial category identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to venue-relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdActivitiesHappeningGet**
```objc
-(NSURLSessionTask*) citiesCityIdActivitiesHappeningGetWithCityId: (NSNumber*) cityId
    happening: (NSString*) happening
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given city happening today or tomorrow

Some functionality could not have been preserved.

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* happening = @"happening_example"; // Events for today or tomorrow ?
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Search activities within a given city happening today or tomorrow
[apiInstance citiesCityIdActivitiesHappeningGetWithCityId:cityId
              happening:happening
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdActivitiesHappeningGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **happening** | **NSString***| Events for today or tomorrow ? | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdBlogpostsGet**
```objc
-(NSURLSessionTask*) citiesCityIdBlogpostsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGBlogpost>* output, NSError* error)) handler;
```

Get the blogposts for the city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get the blogposts for the city
[apiInstance citiesCityIdBlogpostsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGBlogpost>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdBlogpostsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGBlogpost>***](SWGBlogpost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdCategoriesGet**
```objc
-(NSURLSessionTask*) citiesCityIdCategoriesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGCategory>* output, NSError* error)) handler;
```

Categories for the city. Sorted by the number of events for the category

Categories for the city. Sorted by the number of activities for the category

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Categories for the city. Sorted by the number of events for the category
[apiInstance citiesCityIdCategoriesGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGCategory>***](SWGCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdGet**
```objc
-(NSURLSessionTask*) citiesCityIdGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCity* output, NSError* error)) handler;
```

Get city by unique identifier

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get city by unique identifier
[apiInstance citiesCityIdGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCity* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCity***](SWGCity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdListsGet**
```objc
-(NSURLSessionTask*) citiesCityIdListsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    xMusementDeviceType: (NSString*) xMusementDeviceType
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    excludeNotTagged: (NSString*) excludeNotTagged
    listtypes: (NSArray<NSString*>*) listtypes
    listtags: (NSArray<NSString*>*) listtags
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGMusementList>* output, NSError* error)) handler;
```

Get lists for a city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementDeviceType = @"xMusementDeviceType_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSString* excludeNotTagged = @"NO"; // If `YES` will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) (optional) (default to NO)
NSArray<NSString*>* listtypes = @[@"listtypes_example"]; // List type to filter by. A collection of list type (optional)
NSArray<NSString*>* listtags = @[@"listtags_example"]; // List of tags to filter by. A collection of tags (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get lists for a city
[apiInstance citiesCityIdListsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              xMusementDeviceType:xMusementDeviceType
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              excludeNotTagged:excludeNotTagged
              listtypes:listtypes
              listtags:listtags
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGMusementList>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdListsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementDeviceType** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **excludeNotTagged** | **NSString***| If &#x60;YES&#x60; will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) | [optional] [default to NO]
 **listtypes** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List type to filter by. A collection of list type | [optional] 
 **listtags** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of tags to filter by. A collection of tags | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGMusementList>***](SWGMusementList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdVenuesGet**
```objc
-(NSURLSessionTask*) citiesCityIdVenuesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    vertical: (NSNumber*) vertical
        completionHandler: (void (^)(NSArray<SWGVenue>* output, NSError* error)) handler;
```

Get all venues for a city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* vertical = @56; // Vertical identifier (optional)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get all venues for a city
[apiInstance citiesCityIdVenuesGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              offset:offset
              limit:limit
              vertical:vertical
          completionHandler: ^(NSArray<SWGVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdVenuesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 

### Return type

[**NSArray<SWGVenue>***](SWGVenue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdVerticalsGet**
```objc
-(NSURLSessionTask*) citiesCityIdVerticalsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGVertical>* output, NSError* error)) handler;
```

Get all verticals for a city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get all verticals for a city
[apiInstance citiesCityIdVerticalsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGVertical>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdVerticalsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGVertical>***](SWGVertical.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdVerticalsVerticalIdCategoriesGet**
```objc
-(NSURLSessionTask*) citiesCityIdVerticalsVerticalIdCategoriesGetWithCityId: (NSNumber*) cityId
    verticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGCategoryAggregated>* output, NSError* error)) handler;
```

Categories for the city for a specific vertical. Sorted by the number of activities for the category

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Categories for the city for a specific vertical. Sorted by the number of activities for the category
[apiInstance citiesCityIdVerticalsVerticalIdCategoriesGetWithCityId:cityId
              verticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGCategoryAggregated>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesCityIdVerticalsVerticalIdCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGCategoryAggregated>***](SWGCategoryAggregated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesGet**
```objc
-(NSURLSessionTask*) citiesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    prioritizedCountry: (NSNumber*) prioritizedCountry
    prioritizedCountryCitiesLimit: (NSNumber*) prioritizedCountryCitiesLimit
    sortBy: (NSString*) sortBy
    withoutEvents: (NSString*) withoutEvents
        completionHandler: (void (^)(NSArray<SWGCityAggregated>* output, NSError* error)) handler;
```

Get cities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* prioritizedCountry = @56; // Set the country to prioritize (id) (will affect results order) (optional)
NSNumber* prioritizedCountryCitiesLimit = @56; // Set the number of cities to select from the country given in prioritized_country, if not given means unlimited (optional)
NSString* sortBy = @"weight"; // Set sorting strategy (optional) (default to weight)
NSString* withoutEvents = @"no"; // Set to 'yes' return all cities otherwise only those with events (optional) (default to no)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get cities
[apiInstance citiesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
              prioritizedCountry:prioritizedCountry
              prioritizedCountryCitiesLimit:prioritizedCountryCitiesLimit
              sortBy:sortBy
              withoutEvents:withoutEvents
          completionHandler: ^(NSArray<SWGCityAggregated>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->citiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **prioritizedCountry** | **NSNumber***| Set the country to prioritize (id) (will affect results order) | [optional] 
 **prioritizedCountryCitiesLimit** | **NSNumber***| Set the number of cities to select from the country given in prioritized_country, if not given means unlimited | [optional] 
 **sortBy** | **NSString***| Set sorting strategy | [optional] [default to weight]
 **withoutEvents** | **NSString***| Set to &#39;yes&#39; return all cities otherwise only those with events | [optional] [default to no]

### Return type

[**NSArray<SWGCityAggregated>***](SWGCityAggregated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **countriesCountryIdCitiesGet**
```objc
-(NSURLSessionTask*) countriesCountryIdCitiesGetWithCountryId: (NSNumber*) countryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGCityAggregated>* output, NSError* error)) handler;
```

Get all cities for a country

### Example 
```objc

NSNumber* countryId = @56; // Country identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGCityApi*apiInstance = [[SWGCityApi alloc] init];

// Get all cities for a country
[apiInstance countriesCountryIdCitiesGetWithCountryId:countryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGCityAggregated>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCityApi->countriesCountryIdCitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **NSNumber***| Country identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGCityAggregated>***](SWGCityAggregated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

