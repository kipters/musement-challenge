# SWGActivitySearchResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**SWGSearchResultMetaData***](SWGSearchResultMetaData.md) |  | [optional] 
**data** | [**NSArray&lt;SWGEvent&gt;***](SWGEvent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


