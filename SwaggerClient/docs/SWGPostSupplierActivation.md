# SWGPostSupplierActivation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**confirmationToken** | **NSString*** | registration confirmation token | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


