# SWGServicesMetadataApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**servicesGet**](SWGServicesMetadataApi.md#servicesget) | **GET** /services | Get all available services


# **servicesGet**
```objc
-(NSURLSessionTask*) servicesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all available services

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGServicesMetadataApi*apiInstance = [[SWGServicesMetadataApi alloc] init];

// Get all available services
[apiInstance servicesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGServicesMetadataApi->servicesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

