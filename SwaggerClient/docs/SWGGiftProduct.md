# SWGGiftProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | [optional] 
**redeemUrl** | **NSString*** | for example: &#39;/us/some_city/some_event-123/?gift_hash&#x3D;abc123&amp;gift_code&#x3D;CDE432&#39; - when user is redirected to this Event page he will see \&quot;Free Gift\&quot; and \&quot;Gift redeem Orderbox\&quot; with 0 price etc page where he can pick Date for his Gift | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


