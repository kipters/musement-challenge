# SWGSupplierActivityApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersMeActivitiesActivityUuidPatch**](SWGSupplierActivityApi.md#suppliersmeactivitiesactivityuuidpatch) | **PATCH** /suppliers/me/activities/{activityUuid} | Update details for a supplier&#39;s activity
[**suppliersMeActivitiesGet**](SWGSupplierActivityApi.md#suppliersmeactivitiesget) | **GET** /suppliers/me/activities | Get activities for the logged in supplier
[**suppliersMePriceFeaturesGet**](SWGSupplierActivityApi.md#suppliersmepricefeaturesget) | **GET** /suppliers/me/price-features | Get all available price features for the logged in supplier
[**suppliersMeTicketHoldersGet**](SWGSupplierActivityApi.md#suppliersmeticketholdersget) | **GET** /suppliers/me/ticket-holders | Get all available ticket holders features for the logged in supplier
[**suppliersSupplierUuidActivitiesGet**](SWGSupplierActivityApi.md#supplierssupplieruuidactivitiesget) | **GET** /suppliers/{supplierUuid}/activities | Get all activities for a particular supplier
[**suppliersSupplierUuidPriceFeaturesGet**](SWGSupplierActivityApi.md#supplierssupplieruuidpricefeaturesget) | **GET** /suppliers/{supplierUuid}/price-features | Get all price features for a particular supplier
[**suppliersSupplierUuidTicketHoldersGet**](SWGSupplierActivityApi.md#supplierssupplieruuidticketholdersget) | **GET** /suppliers/{supplierUuid}/ticket-holders | Get all price ticket holders for a particular supplier


# **suppliersMeActivitiesActivityUuidPatch**
```objc
-(NSURLSessionTask*) suppliersMeActivitiesActivityUuidPatchWithActivityUuid: (NSString*) activityUuid
    patchActivity: (SWGPatchActivity*) patchActivity
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGEvent* output, NSError* error)) handler;
```

Update details for a supplier's activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPatchActivity* patchActivity = [[SWGPatchActivity alloc] init]; // Activity patch request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Update details for a supplier's activity
[apiInstance suppliersMeActivitiesActivityUuidPatchWithActivityUuid:activityUuid
              patchActivity:patchActivity
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersMeActivitiesActivityUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **patchActivity** | [**SWGPatchActivity***](SWGPatchActivity.md)| Activity patch request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGEvent***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersMeActivitiesGet**
```objc
-(NSURLSessionTask*) suppliersMeActivitiesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Get activities for the logged in supplier

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Get activities for the logged in supplier
[apiInstance suppliersMeActivitiesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersMeActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersMePriceFeaturesGet**
```objc
-(NSURLSessionTask*) suppliersMePriceFeaturesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all available price features for the logged in supplier

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Get all available price features for the logged in supplier
[apiInstance suppliersMePriceFeaturesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersMePriceFeaturesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersMeTicketHoldersGet**
```objc
-(NSURLSessionTask*) suppliersMeTicketHoldersGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all available ticket holders features for the logged in supplier

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Get all available ticket holders features for the logged in supplier
[apiInstance suppliersMeTicketHoldersGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersMeTicketHoldersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidActivitiesGet**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidActivitiesGetWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all activities for a particular supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Get all activities for a particular supplier
[apiInstance suppliersSupplierUuidActivitiesGetWithSupplierUuid:supplierUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersSupplierUuidActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidPriceFeaturesGet**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidPriceFeaturesGetWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all price features for a particular supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Get all price features for a particular supplier
[apiInstance suppliersSupplierUuidPriceFeaturesGetWithSupplierUuid:supplierUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersSupplierUuidPriceFeaturesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidTicketHoldersGet**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidTicketHoldersGetWithSupplierUuid: (NSString*) supplierUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all price ticket holders for a particular supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierActivityApi*apiInstance = [[SWGSupplierActivityApi alloc] init];

// Get all price ticket holders for a particular supplier
[apiInstance suppliersSupplierUuidTicketHoldersGetWithSupplierUuid:supplierUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierActivityApi->suppliersSupplierUuidTicketHoldersGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

