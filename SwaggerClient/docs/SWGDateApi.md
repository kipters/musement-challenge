# SWGDateApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidBlackoutDaysPost**](SWGDateApi.md#activitiesactivityuuidblackoutdayspost) | **POST** /activities/{activityUuid}/blackout-days | Add a blackout day
[**activitiesActivityUuidDatesDayGet**](SWGDateApi.md#activitiesactivityuuiddatesdayget) | **GET** /activities/{activityUuid}/dates/{day} | Get schedule for a day for an event
[**activitiesActivityUuidDatesGet**](SWGDateApi.md#activitiesactivityuuiddatesget) | **GET** /activities/{activityUuid}/dates | Get available dates for an event
[**activitiesActivityUuidOpenSeasonsPost**](SWGDateApi.md#activitiesactivityuuidopenseasonspost) | **POST** /activities/{activityUuid}/open-seasons | Add an open season to an activity
[**activitiesActivityUuidTimedSeasonsPost**](SWGDateApi.md#activitiesactivityuuidtimedseasonspost) | **POST** /activities/{activityUuid}/timed-seasons | Add a season to an activity
[**eventsEventIdGiftScheduleGet**](SWGDateApi.md#eventseventidgiftscheduleget) | **GET** /events/{eventId}/gift-schedule | Get schedule for a &#39;Gift creation&#39; for an Event


# **activitiesActivityUuidBlackoutDaysPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidBlackoutDaysPostWithActivityUuid: (NSString*) activityUuid
    blackoutDay: (SWGPostActivityBlackoutDay*) blackoutDay
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGBlackoutDay* output, NSError* error)) handler;
```

Add a blackout day

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPostActivityBlackoutDay* blackoutDay = [[SWGPostActivityBlackoutDay alloc] init]; // Day to add to the blackout one
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGDateApi*apiInstance = [[SWGDateApi alloc] init];

// Add a blackout day
[apiInstance activitiesActivityUuidBlackoutDaysPostWithActivityUuid:activityUuid
              blackoutDay:blackoutDay
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGBlackoutDay* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDateApi->activitiesActivityUuidBlackoutDaysPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **blackoutDay** | [**SWGPostActivityBlackoutDay***](SWGPostActivityBlackoutDay.md)| Day to add to the blackout one | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGBlackoutDay***](SWGBlackoutDay.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesDayGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesDayGetWithActivityUuid: (NSString*) activityUuid
    day: (NSDate*) day
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    ticketsNumber: (NSString*) ticketsNumber
        completionHandler: (void (^)(NSArray<SWGSchedule>* output, NSError* error)) handler;
```

Get schedule for a day for an event

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSDate* day = @"2013-10-20"; // Day
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSString* ticketsNumber = @"ticketsNumber_example"; // Number of ticket requested tickets | Only useful for real time activities (optional)

SWGDateApi*apiInstance = [[SWGDateApi alloc] init];

// Get schedule for a day for an event
[apiInstance activitiesActivityUuidDatesDayGetWithActivityUuid:activityUuid
              day:day
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              ticketsNumber:ticketsNumber
          completionHandler: ^(NSArray<SWGSchedule>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDateApi->activitiesActivityUuidDatesDayGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **day** | **NSDate***| Day | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **ticketsNumber** | **NSString***| Number of ticket requested tickets | Only useful for real time activities | [optional] 

### Return type

[**NSArray<SWGSchedule>***](SWGSchedule.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidDatesGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidDatesGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
        completionHandler: (void (^)(NSDate* output, NSError* error)) handler;
```

Get available dates for an event

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSDate* dateFrom = @"2013-10-20"; // Start date | If not specified set to today | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | If not specified set to next year | Use format: YYYY-MM-DD (optional)

SWGDateApi*apiInstance = [[SWGDateApi alloc] init];

// Get available dates for an event
[apiInstance activitiesActivityUuidDatesGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              dateFrom:dateFrom
              dateTo:dateTo
          completionHandler: ^(NSDate* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDateApi->activitiesActivityUuidDatesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **dateFrom** | **NSDate***| Start date | If not specified set to today | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | If not specified set to next year | Use format: YYYY-MM-DD | [optional] 

### Return type

**NSDate***

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidOpenSeasonsPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidOpenSeasonsPostWithOpenSeason: (SWGPostActivityOpenSeason*) openSeason
    activityUuid: (NSString*) activityUuid
        completionHandler: (void (^)(SWGActivityCalendarUpdateRequest* output, NSError* error)) handler;
```

Add an open season to an activity

### Example 
```objc

SWGPostActivityOpenSeason* openSeason = [[SWGPostActivityOpenSeason alloc] init]; // Open season
NSString* activityUuid = @"activityUuid_example"; // Activity identifier

SWGDateApi*apiInstance = [[SWGDateApi alloc] init];

// Add an open season to an activity
[apiInstance activitiesActivityUuidOpenSeasonsPostWithOpenSeason:openSeason
              activityUuid:activityUuid
          completionHandler: ^(SWGActivityCalendarUpdateRequest* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDateApi->activitiesActivityUuidOpenSeasonsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **openSeason** | [**SWGPostActivityOpenSeason***](SWGPostActivityOpenSeason.md)| Open season | 
 **activityUuid** | **NSString***| Activity identifier | 

### Return type

[**SWGActivityCalendarUpdateRequest***](SWGActivityCalendarUpdateRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidTimedSeasonsPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidTimedSeasonsPostWithSeason: (SWGPostTimedSeason*) season
    activityUuid: (NSString*) activityUuid
        completionHandler: (void (^)(SWGActivityCalendarUpdateRequest* output, NSError* error)) handler;
```

Add a season to an activity

### Example 
```objc

SWGPostTimedSeason* season = [[SWGPostTimedSeason alloc] init]; // Activity season
NSString* activityUuid = @"activityUuid_example"; // Activity identifier

SWGDateApi*apiInstance = [[SWGDateApi alloc] init];

// Add a season to an activity
[apiInstance activitiesActivityUuidTimedSeasonsPostWithSeason:season
              activityUuid:activityUuid
          completionHandler: ^(SWGActivityCalendarUpdateRequest* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDateApi->activitiesActivityUuidTimedSeasonsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **season** | [**SWGPostTimedSeason***](SWGPostTimedSeason.md)| Activity season | 
 **activityUuid** | **NSString***| Activity identifier | 

### Return type

[**SWGActivityCalendarUpdateRequest***](SWGActivityCalendarUpdateRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventsEventIdGiftScheduleGet**
```objc
-(NSURLSessionTask*) eventsEventIdGiftScheduleGetWithEventId: (NSNumber*) eventId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGGiftSchedule* output, NSError* error)) handler;
```

Get schedule for a 'Gift creation' for an Event

### Example 
```objc

NSNumber* eventId = @56; // Event identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGDateApi*apiInstance = [[SWGDateApi alloc] init];

// Get schedule for a 'Gift creation' for an Event
[apiInstance eventsEventIdGiftScheduleGetWithEventId:eventId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGGiftSchedule* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDateApi->eventsEventIdGiftScheduleGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **NSNumber***| Event identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGGiftSchedule***](SWGGiftSchedule.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

