# SWGPostStripePayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** | Order ID - Deprecated, use order_uuid | [optional] 
**orderUuid** | **NSString*** | Order UUID | [optional] 
**stripeToken** | **NSString*** | Stripe token. If not specified stripe_source_id must be specified. | [optional] 
**stripeSourceId** | **NSString*** | Stripe source id. If not specified stripe_token must be specified. | [optional] 
**isApplePay** | **NSNumber*** | Set to true if is an Apple Pay payment | [optional] 
**clientId** | **NSString*** | Client IP | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


