# SWGGiftRedeemRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **NSString*** | This is optional - for example when open ticket. | [optional] 
**time** | **NSString*** | This is optional - for example when open ticket. | [optional] 
**language** | **NSString*** | This is optional - when Event doesnt have languages to choose. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


