# SWGCommentApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidCommentsGet**](SWGCommentApi.md#activitiesactivityuuidcommentsget) | **GET** /activities/{activityUuid}/comments | Get all comments for the activity
[**activitiesActivityUuidCommentsPost**](SWGCommentApi.md#activitiesactivityuuidcommentspost) | **POST** /activities/{activityUuid}/comments | Add a comment to the activity
[**commentsCommentUuidGet**](SWGCommentApi.md#commentscommentuuidget) | **GET** /comments/{commentUuid} | Read a comment
[**commentsCommentUuidPatch**](SWGCommentApi.md#commentscommentuuidpatch) | **PATCH** /comments/{commentUuid} | Update a comment
[**customersMeCommentsGet**](SWGCommentApi.md#customersmecommentsget) | **GET** /customers/me/comments | Get all comments for the customer
[**feedbackRequestsFeedbackRequestNonceGet**](SWGCommentApi.md#feedbackrequestsfeedbackrequestnonceget) | **GET** /feedback-requests/{feedbackRequestNonce} | Get feedback request details
[**venuesVenueIdCommentsGet**](SWGCommentApi.md#venuesvenueidcommentsget) | **GET** /venues/{venueId}/comments | Get all comments for an venue


# **activitiesActivityUuidCommentsGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidCommentsGetWithActivityUuid: (NSString*) activityUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    locale: (NSString*) locale
    minRating: (NSNumber*) minRating
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(NSArray<SWGComment>* output, NSError* error)) handler;
```

Get all comments for the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* locale = @"locale_example"; // Locale code (optional)
NSNumber* minRating = @56; // Minimum rating (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Get all comments for the activity
[apiInstance activitiesActivityUuidCommentsGetWithActivityUuid:activityUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              locale:locale
              minRating:minRating
              limit:limit
              offset:offset
          completionHandler: ^(NSArray<SWGComment>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->activitiesActivityUuidCommentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **locale** | **NSString***| Locale code | [optional] 
 **minRating** | **NSNumber***| Minimum rating | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

[**NSArray<SWGComment>***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidCommentsPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidCommentsPostWithActivityUuid: (NSString*) activityUuid
    postComment: (SWGPostComment*) postComment
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGComment* output, NSError* error)) handler;
```

Add a comment to the activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPostComment* postComment = [[SWGPostComment alloc] init]; // Comment's data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Add a comment to the activity
[apiInstance activitiesActivityUuidCommentsPostWithActivityUuid:activityUuid
              postComment:postComment
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGComment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->activitiesActivityUuidCommentsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **postComment** | [**SWGPostComment***](SWGPostComment.md)| Comment&#39;s data | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGComment***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **commentsCommentUuidGet**
```objc
-(NSURLSessionTask*) commentsCommentUuidGetWithCommentUuid: (NSString*) commentUuid
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGComment* output, NSError* error)) handler;
```

Read a comment

### Example 
```objc

NSString* commentUuid = @"commentUuid_example"; // Comment identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Read a comment
[apiInstance commentsCommentUuidGetWithCommentUuid:commentUuid
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGComment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->commentsCommentUuidGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commentUuid** | **NSString***| Comment identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGComment***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **commentsCommentUuidPatch**
```objc
-(NSURLSessionTask*) commentsCommentUuidPatchWithCommentUuid: (NSString*) commentUuid
    patchComment: (SWGPatchComment*) patchComment
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGComment* output, NSError* error)) handler;
```

Update a comment

### Example 
```objc

NSString* commentUuid = @"commentUuid_example"; // Comment identifier
SWGPatchComment* patchComment = [[SWGPatchComment alloc] init]; // Comment's data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Update a comment
[apiInstance commentsCommentUuidPatchWithCommentUuid:commentUuid
              patchComment:patchComment
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGComment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->commentsCommentUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commentUuid** | **NSString***| Comment identifier | 
 **patchComment** | [**SWGPatchComment***](SWGPatchComment.md)| Comment&#39;s data | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGComment***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMeCommentsGet**
```objc
-(NSURLSessionTask*) customersMeCommentsGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGComment* output, NSError* error)) handler;
```

Get all comments for the customer

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Get all comments for the customer
[apiInstance customersMeCommentsGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGComment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->customersMeCommentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGComment***](SWGComment.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **feedbackRequestsFeedbackRequestNonceGet**
```objc
-(NSURLSessionTask*) feedbackRequestsFeedbackRequestNonceGetWithFeedbackRequestNonce: (NSString*) feedbackRequestNonce
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGFeedbackRequest* output, NSError* error)) handler;
```

Get feedback request details

### Example 
```objc

NSString* feedbackRequestNonce = @"feedbackRequestNonce_example"; // Feedback request nonce
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Get feedback request details
[apiInstance feedbackRequestsFeedbackRequestNonceGetWithFeedbackRequestNonce:feedbackRequestNonce
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGFeedbackRequest* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->feedbackRequestsFeedbackRequestNonceGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feedbackRequestNonce** | **NSString***| Feedback request nonce | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGFeedbackRequest***](SWGFeedbackRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesVenueIdCommentsGet**
```objc
-(NSURLSessionTask*) venuesVenueIdCommentsGetWithVenueId: (NSNumber*) venueId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    locale: (NSString*) locale
    minRating: (NSNumber*) minRating
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(NSArray<SWGComment>* output, NSError* error)) handler;
```

Get all comments for an venue

### Example 
```objc

NSNumber* venueId = @56; // Venue identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* locale = @"locale_example"; // Locale code (optional)
NSNumber* minRating = @56; // Minimum rating (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGCommentApi*apiInstance = [[SWGCommentApi alloc] init];

// Get all comments for an venue
[apiInstance venuesVenueIdCommentsGetWithVenueId:venueId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              locale:locale
              minRating:minRating
              limit:limit
              offset:offset
          completionHandler: ^(NSArray<SWGComment>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCommentApi->venuesVenueIdCommentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venueId** | **NSNumber***| Venue identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **locale** | **NSString***| Locale code | [optional] 
 **minRating** | **NSNumber***| Minimum rating | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

[**NSArray<SWGComment>***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

