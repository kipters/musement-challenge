# SWGGiftSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | [**NSArray&lt;SWGGiftScheduleFeature&gt;***](SWGGiftScheduleFeature.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


