# SWGGalleryMedia

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 
**externalIdentifier** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


