# SWGPaypalExpressApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsPaypalExpressPaymentPost**](SWGPaypalExpressApi.md#paymentspaypalexpresspaymentpost) | **POST** /payments/paypal-express/payment | Pay an Order.
[**paymentsPaypalExpressRedirectPost**](SWGPaypalExpressApi.md#paymentspaypalexpressredirectpost) | **POST** /payments/paypal-express/redirect | Generate Paypal Express redirect


# **paymentsPaypalExpressPaymentPost**
```objc
-(NSURLSessionTask*) paymentsPaypalExpressPaymentPostWithPostPaypalExpressComplete: (SWGPostPaypalExpressComplete*) postPaypalExpressComplete
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGPaypalPaymentRedirect* output, NSError* error)) handler;
```

Pay an Order.

Complete Paypal Express payment process after return from Paypal site.

### Example 
```objc

SWGPostPaypalExpressComplete* postPaypalExpressComplete = [[SWGPostPaypalExpressComplete alloc] init]; // Paypal Express complete data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGPaypalExpressApi*apiInstance = [[SWGPaypalExpressApi alloc] init];

// Pay an Order.
[apiInstance paymentsPaypalExpressPaymentPostWithPostPaypalExpressComplete:postPaypalExpressComplete
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGPaypalPaymentRedirect* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPaypalExpressApi->paymentsPaypalExpressPaymentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postPaypalExpressComplete** | [**SWGPostPaypalExpressComplete***](SWGPostPaypalExpressComplete.md)| Paypal Express complete data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGPaypalPaymentRedirect***](SWGPaypalPaymentRedirect.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **paymentsPaypalExpressRedirectPost**
```objc
-(NSURLSessionTask*) paymentsPaypalExpressRedirectPostWithPostPaypalExpressRedirect: (SWGPostPaypalExpressRedirect*) postPaypalExpressRedirect
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGPaypalPaymentRedirect* output, NSError* error)) handler;
```

Generate Paypal Express redirect

Generate redirect url for payment using Paypal Express.

### Example 
```objc

SWGPostPaypalExpressRedirect* postPaypalExpressRedirect = [[SWGPostPaypalExpressRedirect alloc] init]; // Paypal Express redirect data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGPaypalExpressApi*apiInstance = [[SWGPaypalExpressApi alloc] init];

// Generate Paypal Express redirect
[apiInstance paymentsPaypalExpressRedirectPostWithPostPaypalExpressRedirect:postPaypalExpressRedirect
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGPaypalPaymentRedirect* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPaypalExpressApi->paymentsPaypalExpressRedirectPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postPaypalExpressRedirect** | [**SWGPostPaypalExpressRedirect***](SWGPostPaypalExpressRedirect.md)| Paypal Express redirect data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGPaypalPaymentRedirect***](SWGPaypalPaymentRedirect.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

