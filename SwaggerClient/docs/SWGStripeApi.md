# SWGStripeApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsStripeComplete3dSecurePost**](SWGStripeApi.md#paymentsstripecomplete3dsecurepost) | **POST** /payments/stripe/complete_3d_secure | Complete 3d secure payment.
[**paymentsStripePaymentPost**](SWGStripeApi.md#paymentsstripepaymentpost) | **POST** /payments/stripe/payment | Pay an order.
[**paymentsStripeStoredSourceInfoGet**](SWGStripeApi.md#paymentsstripestoredsourceinfoget) | **GET** /payments/stripe/stored_source_info | Get stored Stripe Source info


# **paymentsStripeComplete3dSecurePost**
```objc
-(NSURLSessionTask*) paymentsStripeComplete3dSecurePostWithCompletionHandler: 
        (void (^)(SWGStripeSuccessfulPayment* output, NSError* error)) handler;
```

Complete 3d secure payment.

Complete 3d secure payment after confirming it on card issuer page.

### Example 
```objc


SWGStripeApi*apiInstance = [[SWGStripeApi alloc] init];

// Complete 3d secure payment.
[apiInstance paymentsStripeComplete3dSecurePostWithCompletionHandler: 
          ^(SWGStripeSuccessfulPayment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGStripeApi->paymentsStripeComplete3dSecurePost: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGStripeSuccessfulPayment***](SWGStripeSuccessfulPayment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **paymentsStripePaymentPost**
```objc
-(NSURLSessionTask*) paymentsStripePaymentPostWithStripePayment: (SWGPostStripePayment*) stripePayment
        completionHandler: (void (^)(SWGStripeSuccessfulPayment* output, NSError* error)) handler;
```

Pay an order.

Pay an order using stripe.

### Example 
```objc

SWGPostStripePayment* stripePayment = [[SWGPostStripePayment alloc] init]; // Stripe payment info request

SWGStripeApi*apiInstance = [[SWGStripeApi alloc] init];

// Pay an order.
[apiInstance paymentsStripePaymentPostWithStripePayment:stripePayment
          completionHandler: ^(SWGStripeSuccessfulPayment* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGStripeApi->paymentsStripePaymentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stripePayment** | [**SWGPostStripePayment***](SWGPostStripePayment.md)| Stripe payment info request | 

### Return type

[**SWGStripeSuccessfulPayment***](SWGStripeSuccessfulPayment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **paymentsStripeStoredSourceInfoGet**
```objc
-(NSURLSessionTask*) paymentsStripeStoredSourceInfoGetWithCompletionHandler: 
        (void (^)(NSError* error)) handler;
```

Get stored Stripe Source info

### Example 
```objc


SWGStripeApi*apiInstance = [[SWGStripeApi alloc] init];

// Get stored Stripe Source info
[apiInstance paymentsStripeStoredSourceInfoGetWithCompletionHandler: 
          ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGStripeApi->paymentsStripeStoredSourceInfoGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

