# SWGStoredSourceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceId** | **NSString*** | Id of stored Customer&#39;s Source - for example Card or Android Pay. | [optional] 
**type** | **NSString*** | Type of Source. For now only \&quot;card\&quot; is supported to be stored. | [optional] 
**creditCardBrand** | **NSString*** | In case of Credit Cards its brand name, for example: \&quot;Visa\&quot;, \&quot;MasterCard\&quot;, \&quot;American Express\&quot;, \&quot;Unknown\&quot;. | [optional] 
**name** | **NSString*** | Name of Source. For example last 4 digits of card: \&quot;1234\&quot;. | [optional] 
**cardholderName** | **NSString*** | Name of credit card holder. For example: \&quot;John Yolo\&quot;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


