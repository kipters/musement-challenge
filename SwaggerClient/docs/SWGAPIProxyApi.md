# SWGAPIProxyApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**proxyAuthClientPost**](SWGAPIProxyApi.md#proxyauthclientpost) | **POST** /proxy/auth/client | POST Login using client_credentials grant type
[**proxyAuthUserPost**](SWGAPIProxyApi.md#proxyauthuserpost) | **POST** /proxy/auth/user | POST Login using password grant type


# **proxyAuthClientPost**
```objc
-(NSURLSessionTask*) proxyAuthClientPostWithCompletionHandler: 
        (void (^)(NSError* error)) handler;
```

POST Login using client_credentials grant type

### Example 
```objc


SWGAPIProxyApi*apiInstance = [[SWGAPIProxyApi alloc] init];

// POST Login using client_credentials grant type
[apiInstance proxyAuthClientPostWithCompletionHandler: 
          ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGAPIProxyApi->proxyAuthClientPost: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **proxyAuthUserPost**
```objc
-(NSURLSessionTask*) proxyAuthUserPostWithCompletionHandler: 
        (void (^)(NSError* error)) handler;
```

POST Login using password grant type

### Example 
```objc


SWGAPIProxyApi*apiInstance = [[SWGAPIProxyApi alloc] init];

// POST Login using password grant type
[apiInstance proxyAuthUserPostWithCompletionHandler: 
          ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGAPIProxyApi->proxyAuthUserPost: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

