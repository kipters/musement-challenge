# SWGProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **NSString*** |  | [optional] 
**image** | **NSString*** |  | [optional] 
**finalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**discountAmount** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**serviceFee** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**_id** | **NSString*** | This is a unique identifier of the product. For Musement&#39;s product this is the SeatPriceId for 3rd party product is the unique identifier of the partner&#39;s product.  Please note that this is a string | [optional] 
**title** | **NSString*** |  | [optional] 
**datetime** | **NSDate*** |  | [optional] 
**ticketHolder** | **NSString*** |  | [optional] 
**priceFeature** | **NSString*** |  | [optional] 
**activity** | [**SWGEvent***](SWGEvent.md) |  | [optional] 
**supplierPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**touristCategory** | **NSString*** |  | [optional] 
**activityName** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


