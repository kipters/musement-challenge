# SWGGiftboxApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**giftboxesGiftboxCodeGet**](SWGGiftboxApi.md#giftboxesgiftboxcodeget) | **GET** /giftboxes/{giftboxCode} | Get a giftbox by code


# **giftboxesGiftboxCodeGet**
```objc
-(NSURLSessionTask*) giftboxesGiftboxCodeGetWithGiftboxCode: (NSString*) giftboxCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGGiftbox* output, NSError* error)) handler;
```

Get a giftbox by code

### Example 
```objc

NSString* giftboxCode = @"giftboxCode_example"; // Giftbox redeem code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGGiftboxApi*apiInstance = [[SWGGiftboxApi alloc] init];

// Get a giftbox by code
[apiInstance giftboxesGiftboxCodeGetWithGiftboxCode:giftboxCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGGiftbox* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxApi->giftboxesGiftboxCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftboxCode** | **NSString***| Giftbox redeem code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGGiftbox***](SWGGiftbox.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

