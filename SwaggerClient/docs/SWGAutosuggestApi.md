# SWGAutosuggestApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suggestGet**](SWGAutosuggestApi.md#suggestget) | **GET** /suggest | Search relevant items in Musement DB


# **suggestGet**
```objc
-(NSURLSessionTask*) suggestGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    q: (NSString*) q
        completionHandler: (void (^)(NSError* error)) handler;
```

Search relevant items in Musement DB

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* q = @"q_example"; // Search string. Minimum 3 chars (optional)

SWGAutosuggestApi*apiInstance = [[SWGAutosuggestApi alloc] init];

// Search relevant items in Musement DB
[apiInstance suggestGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              q:q
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGAutosuggestApi->suggestGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **q** | **NSString***| Search string. Minimum 3 chars | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

