# SWGDestinationComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**SWGDestinationCommentAuthor***](SWGDestinationCommentAuthor.md) |  | [optional] 
**title** | **NSString*** |  | [optional] 
**text** | **NSString*** |  | [optional] 
**date** | **NSString*** | This value is saved as ISO 8601 string. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


