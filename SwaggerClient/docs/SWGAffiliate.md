# SWGAffiliate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | 
**name** | **NSString*** |  | 
**logoUrl** | **NSString*** |  | 
**secondaryLogoUrl** | **NSString*** |  | [optional] 
**whitelabel** | **NSNumber*** |  | [optional] 
**showCobrandedHeader** | **NSNumber*** |  | [optional] 
**showCobrandedVoucher** | **NSNumber*** |  | [optional] 
**showCobrandedItemConfirmationEmail** | **NSNumber*** |  | [optional] 
**setupCookieAfterFirstVisit** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


