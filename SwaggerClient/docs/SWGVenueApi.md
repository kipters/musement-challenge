# SWGVenueApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**citiesCityIdVenuesGet**](SWGVenueApi.md#citiescityidvenuesget) | **GET** /cities/{cityId}/venues | Get all venues for a city
[**venuesGet**](SWGVenueApi.md#venuesget) | **GET** /venues | List venues | For non autenticated requests only venues that have active activities for the region you request are returned. If the request has &#x60;CONTENTMANAGEMENT&#x60; grant activity restriction is not applied.
[**venuesVenueIdActivitiesGet**](SWGVenueApi.md#venuesvenueidactivitiesget) | **GET** /venues/{venueId}/activities | Search activities within a given venue
[**venuesVenueIdCommentsGet**](SWGVenueApi.md#venuesvenueidcommentsget) | **GET** /venues/{venueId}/comments | Get all comments for an venue
[**venuesVenueIdGet**](SWGVenueApi.md#venuesvenueidget) | **GET** /venues/{venueId} | Get a venue
[**venuesVenueIdRegionsGet**](SWGVenueApi.md#venuesvenueidregionsget) | **GET** /venues/{venueId}/regions | Get all available regions for the venue


# **citiesCityIdVenuesGet**
```objc
-(NSURLSessionTask*) citiesCityIdVenuesGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    vertical: (NSNumber*) vertical
        completionHandler: (void (^)(NSArray<SWGVenue>* output, NSError* error)) handler;
```

Get all venues for a city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* vertical = @56; // Vertical identifier (optional)

SWGVenueApi*apiInstance = [[SWGVenueApi alloc] init];

// Get all venues for a city
[apiInstance citiesCityIdVenuesGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              offset:offset
              limit:limit
              vertical:vertical
          completionHandler: ^(NSArray<SWGVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVenueApi->citiesCityIdVenuesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 

### Return type

[**NSArray<SWGVenue>***](SWGVenue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesGet**
```objc
-(NSURLSessionTask*) venuesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    status: (NSString*) status
    visible: (NSNumber*) visible
    countryIn: (NSArray<NSNumber*>*) countryIn
    limit: (NSNumber*) limit
    page: (NSNumber*) page
    offset: (NSNumber*) offset
    sortBy: (NSArray<NSString*>*) sortBy
        completionHandler: (void (^)(NSArray<SWGVenue>* output, NSError* error)) handler;
```

List venues | For non autenticated requests only venues that have active activities for the region you request are returned. If the request has `CONTENTMANAGEMENT` grant activity restriction is not applied.

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* status = @"ACTIVE"; // Filter venues by status | `NOT-ACTIVE` and `ALL` can be set only when the endpoint is called by an application with `CONTENTMANAGEMENT` grant. (optional) (default to ACTIVE)
NSNumber* visible = @true; // Filter, include results on a visibility status basis (optional)
NSArray<NSNumber*>* countryIn = @[@56]; // Filter, include only results from given countries identified by a collection of ids (optional)
NSNumber* limit = @56; // Limit quota of venues to return (optional)
NSNumber* page = @56; // Page from which starting to return found venues (mandatory if limit is given) (optional)
NSNumber* offset = @56; // Offset from which starting to return found venues (mandatory if limit is given cannot be used within the page parameter) (optional)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria: [relevance], prepend `-` for descending order (optional)

SWGVenueApi*apiInstance = [[SWGVenueApi alloc] init];

// List venues | For non autenticated requests only venues that have active activities for the region you request are returned. If the request has `CONTENTMANAGEMENT` grant activity restriction is not applied.
[apiInstance venuesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              status:status
              visible:visible
              countryIn:countryIn
              limit:limit
              page:page
              offset:offset
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVenueApi->venuesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **status** | **NSString***| Filter venues by status | &#x60;NOT-ACTIVE&#x60; and &#x60;ALL&#x60; can be set only when the endpoint is called by an application with &#x60;CONTENTMANAGEMENT&#x60; grant. | [optional] [default to ACTIVE]
 **visible** | **NSNumber***| Filter, include results on a visibility status basis | [optional] 
 **countryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from given countries identified by a collection of ids | [optional] 
 **limit** | **NSNumber***| Limit quota of venues to return | [optional] 
 **page** | **NSNumber***| Page from which starting to return found venues (mandatory if limit is given) | [optional] 
 **offset** | **NSNumber***| Offset from which starting to return found venues (mandatory if limit is given cannot be used within the page parameter) | [optional] 
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria: [relevance], prepend &#x60;-&#x60; for descending order | [optional] 

### Return type

[**NSArray<SWGVenue>***](SWGVenue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesVenueIdActivitiesGet**
```objc
-(NSURLSessionTask*) venuesVenueIdActivitiesGetWithVenueId: (NSNumber*) venueId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    category: (NSNumber*) category
    editorialCategory: (NSNumber*) editorialCategory
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given venue

Get all activities for an venue sorted by relevance. Only the activities that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* venueId = @56; // Venue identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* city = @56; // City identifier (optional)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* editorialCategory = @56; // Editorial category identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"venue-relevance"; // Sorting strategy (optional) (default to venue-relevance)

SWGVenueApi*apiInstance = [[SWGVenueApi alloc] init];

// Search activities within a given venue
[apiInstance venuesVenueIdActivitiesGetWithVenueId:venueId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              city:city
              category:category
              editorialCategory:editorialCategory
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVenueApi->venuesVenueIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venueId** | **NSNumber***| Venue identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **city** | **NSNumber***| City identifier | [optional] 
 **category** | **NSNumber***| Category identifier | [optional] 
 **editorialCategory** | **NSNumber***| Editorial category identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to venue-relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesVenueIdCommentsGet**
```objc
-(NSURLSessionTask*) venuesVenueIdCommentsGetWithVenueId: (NSNumber*) venueId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    locale: (NSString*) locale
    minRating: (NSNumber*) minRating
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
        completionHandler: (void (^)(NSArray<SWGComment>* output, NSError* error)) handler;
```

Get all comments for an venue

### Example 
```objc

NSNumber* venueId = @56; // Venue identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* locale = @"locale_example"; // Locale code (optional)
NSNumber* minRating = @56; // Minimum rating (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)

SWGVenueApi*apiInstance = [[SWGVenueApi alloc] init];

// Get all comments for an venue
[apiInstance venuesVenueIdCommentsGetWithVenueId:venueId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              locale:locale
              minRating:minRating
              limit:limit
              offset:offset
          completionHandler: ^(NSArray<SWGComment>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVenueApi->venuesVenueIdCommentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venueId** | **NSNumber***| Venue identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **locale** | **NSString***| Locale code | [optional] 
 **minRating** | **NSNumber***| Minimum rating | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]

### Return type

[**NSArray<SWGComment>***](SWGComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesVenueIdGet**
```objc
-(NSURLSessionTask*) venuesVenueIdGetWithVenueId: (NSNumber*) venueId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGVenue* output, NSError* error)) handler;
```

Get a venue

### Example 
```objc

NSNumber* venueId = @56; // Venue identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGVenueApi*apiInstance = [[SWGVenueApi alloc] init];

// Get a venue
[apiInstance venuesVenueIdGetWithVenueId:venueId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGVenue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVenueApi->venuesVenueIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venueId** | **NSNumber***| Venue identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGVenue***](SWGVenue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **venuesVenueIdRegionsGet**
```objc
-(NSURLSessionTask*) venuesVenueIdRegionsGetWithVenueId: (NSNumber*) venueId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get all available regions for the venue

### Example 
```objc

NSNumber* venueId = @56; // Venue identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGVenueApi*apiInstance = [[SWGVenueApi alloc] init];

// Get all available regions for the venue
[apiInstance venuesVenueIdRegionsGetWithVenueId:venueId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVenueApi->venuesVenueIdRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venueId** | **NSNumber***| Venue identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

