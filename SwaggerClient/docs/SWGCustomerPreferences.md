# SWGCustomerPreferences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**varNewsletterFrequency** | [**SWGNewsletterFrequency***](SWGNewsletterFrequency.md) | How often the customer want to receive a newsletter ? | [optional] 
**varNewsletterFromThirdparty** | [**SWGNewsletterThirdparty***](SWGNewsletterThirdparty.md) | Does the customer wants newsletters from thirdparty ? | [optional] 
**interests** | [**NSArray&lt;SWGCustomerInterest&gt;***](SWGCustomerInterest.md) | Customer&#39;s interests | [optional] 
**travelWith** | [**SWGCustomerTravelWith***](SWGCustomerTravelWith.md) | The customer like to travel with: | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


