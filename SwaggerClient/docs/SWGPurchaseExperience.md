# SWGPurchaseExperience

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**text** | **NSString*** |  | [optional] 
**rating** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


