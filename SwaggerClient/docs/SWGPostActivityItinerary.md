# SWGPostActivityItinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** | Itinerary code | [optional] 
**pois** | [**NSArray&lt;SWGActivityPoi&gt;***](SWGActivityPoi.md) | POI type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


