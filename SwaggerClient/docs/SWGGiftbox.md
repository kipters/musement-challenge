# SWGGiftbox

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redeemCode** | **NSString*** |  | [optional] 
**finiteState** | **NSString*** |  | [optional] 
**apiUrl** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**coverImage** | **NSString*** |  | [optional] 
**finalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**discountAmount** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**serviceFee** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**giftboxType** | [**SWGGiftboxType***](SWGGiftboxType.md) |  | [optional] 
**customizationInfo** | [**SWGGiftboxCustomizationInfo***](SWGGiftboxCustomizationInfo.md) |  | [optional] 
**type** | **NSString*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**_id** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


