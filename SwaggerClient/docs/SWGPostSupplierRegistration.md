# SWGPostSupplierRegistration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstname** | **NSString*** | Firstname of the contact person | [optional] 
**lastname** | **NSString*** | Lastname of the contact person | [optional] 
**companyLegalName** | **NSString*** | Company legal name | [optional] 
**companyName** | **NSString*** | Customer friendly name. It appears on the voucher | [optional] 
**contactName** | **NSString*** | Contact name | [optional] 
**contactPhoneNumber** | **NSString*** | Contact phone number | [optional] 
**website** | **NSString*** | Supplier&#39;s website | [optional] 
**supportEmail** | **NSString*** | Supplier&#39;s customer care email. | [optional] 
**countryCode** | **NSString*** | Supplier&#39;s country | 2 chars code | [optional] 
**address** | **NSString*** | Supplier&#39;s address | [optional] 
**zipCode** | **NSString*** | Address zip code | [optional] 
**city** | **NSString*** | Supplier&#39;s city | [optional] 
**taxId** | **NSString*** | Supplier&#39;s tax identifier. | [optional] 
**defaultCurrency** | **NSString*** | Default currency used to upload products | Values from GET /api/v3/currencies | [optional] 
**logoUrl** | **NSString*** | Logo URL | [optional] 
**password** | **NSString*** | Login password | [optional] 
**email** | **NSString*** | Email used to login and recover password. It&#39;s not shown to customers | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


