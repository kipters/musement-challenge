# SWGCommission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **NSString*** | Commission type | Can be &#39;ABSOLUTE&#39; or &#39;PERCENTAGE&#39;. | 
**value** | **NSNumber*** | Commission value. Max 2 decimals. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


