# SWGActivityRelatedResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matched** | **NSNumber*** |  | [optional] 
**items** | [**NSArray&lt;SWGActivityRelated&gt;***](SWGActivityRelated.md) |  | [optional] 
**type** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


