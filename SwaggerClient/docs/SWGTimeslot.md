# SWGTimeslot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datetime** | **NSDate*** |  | [optional] 
**availability** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


