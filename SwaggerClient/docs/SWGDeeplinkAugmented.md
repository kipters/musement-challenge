# SWGDeeplinkAugmented

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resource** | **NSString*** | Resource data serialized | [optional] 
**regions** | [**NSArray&lt;SWGRegion&gt;***](SWGRegion.md) | Active regions for the resource | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


