# SWGCustomerInterest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |   * ARTS-CULTURE : Arts and culture,  * SIGHTSEEING :  Sightseeing,  * FOOD-WINE : Food and wine,  * ENTERTAIMENT : Entertaiment,  * SPORTS : Sports,  * ADVENTURE : Adventure,  * NIGHTLIFE : Nightlife | [optional] 
**name** | **NSString*** | Customer&#39;s interest name. | This value depends on the value of the header Accept-Language | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


