# SWGPassengerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**salutation** | **NSString*** |  | [optional] 
**firstname** | **NSString*** |  | [optional] 
**lastname** | **NSString*** |  | [optional] 
**dateOfBirth** | **NSDate*** |  | [optional] 
**passport** | **NSString*** |  | [optional] 
**email** | **NSString*** |  | [optional] 
**passportExpiryDate** | **NSDate*** |  | [optional] 
**nationality** | **NSString*** |  | [optional] 
**medicalNotes** | **NSString*** |  | [optional] 
**address** | **NSString*** |  | [optional] 
**fanCard** | **NSString*** | /_** | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


