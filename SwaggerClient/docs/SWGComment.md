# SWGComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **NSString*** |  | [optional] 
**uuid** | **NSString*** |  | [optional] 
**author** | [**SWGCustomer***](SWGCustomer.md) |  | [optional] 
**locale** | **NSString*** |  | [optional] 
**pictures** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**body** | **NSString*** |  | [optional] 
**rating** | **NSNumber*** |  | [optional] 
**sentAt** | **NSDate*** |  | [optional] 
**event** | [**SWGEvent***](SWGEvent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


