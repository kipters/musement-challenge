# SWGWidgetConfigurationStepCalendarBox

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calendarType** | **NSString*** |  | [optional] 
**flow** | **NSString*** |  | [optional] 
**customTitle** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


