# SWGVerticalApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**citiesCityIdVerticalsGet**](SWGVerticalApi.md#citiescityidverticalsget) | **GET** /cities/{cityId}/verticals | Get all verticals for a city
[**verticalsGet**](SWGVerticalApi.md#verticalsget) | **GET** /verticals | Get all verticals
[**verticalsVerticalIdActivitiesGet**](SWGVerticalApi.md#verticalsverticalidactivitiesget) | **GET** /verticals/{verticalId}/activities | Search activities within a given vertical
[**verticalsVerticalIdCategoriesGet**](SWGVerticalApi.md#verticalsverticalidcategoriesget) | **GET** /verticals/{verticalId}/categories | Get categories by vertical ID
[**verticalsVerticalIdGet**](SWGVerticalApi.md#verticalsverticalidget) | **GET** /verticals/{verticalId} | Get verticals by unique identifier
[**verticalsVerticalIdRegionsGet**](SWGVerticalApi.md#verticalsverticalidregionsget) | **GET** /verticals/{verticalId}/regions | Get all available regions for the vertical


# **citiesCityIdVerticalsGet**
```objc
-(NSURLSessionTask*) citiesCityIdVerticalsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGVertical>* output, NSError* error)) handler;
```

Get all verticals for a city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGVerticalApi*apiInstance = [[SWGVerticalApi alloc] init];

// Get all verticals for a city
[apiInstance citiesCityIdVerticalsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGVertical>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVerticalApi->citiesCityIdVerticalsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGVertical>***](SWGVertical.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verticalsGet**
```objc
-(NSURLSessionTask*) verticalsGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    categoryIn: (NSArray<NSNumber*>*) categoryIn
        completionHandler: (void (^)(NSArray<SWGVertical>* output, NSError* error)) handler;
```

Get all verticals

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSArray<NSNumber*>* categoryIn = @[@56]; // Filter, include only results from at least one of the given categories identified by a collection of ids (optional)

SWGVerticalApi*apiInstance = [[SWGVerticalApi alloc] init];

// Get all verticals
[apiInstance verticalsGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
              categoryIn:categoryIn
          completionHandler: ^(NSArray<SWGVertical>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVerticalApi->verticalsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **categoryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given categories identified by a collection of ids | [optional] 

### Return type

[**NSArray<SWGVertical>***](SWGVertical.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verticalsVerticalIdActivitiesGet**
```objc
-(NSURLSessionTask*) verticalsVerticalIdActivitiesGetWithVerticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given vertical

### Example 
```objc

NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGVerticalApi*apiInstance = [[SWGVerticalApi alloc] init];

// Search activities within a given vertical
[apiInstance verticalsVerticalIdActivitiesGetWithVerticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVerticalApi->verticalsVerticalIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verticalsVerticalIdCategoriesGet**
```objc
-(NSURLSessionTask*) verticalsVerticalIdCategoriesGetWithVerticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    filteringAware: (NSNumber*) filteringAware
        completionHandler: (void (^)(NSArray<SWGCategory>* output, NSError* error)) handler;
```

Get categories by vertical ID

### Example 
```objc

NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* filteringAware = @true; // If true the category is usable as filter (optional)

SWGVerticalApi*apiInstance = [[SWGVerticalApi alloc] init];

// Get categories by vertical ID
[apiInstance verticalsVerticalIdCategoriesGetWithVerticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
              filteringAware:filteringAware
          completionHandler: ^(NSArray<SWGCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVerticalApi->verticalsVerticalIdCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **filteringAware** | **NSNumber***| If true the category is usable as filter | [optional] 

### Return type

[**NSArray<SWGCategory>***](SWGCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verticalsVerticalIdGet**
```objc
-(NSURLSessionTask*) verticalsVerticalIdGetWithVerticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGVertical>* output, NSError* error)) handler;
```

Get verticals by unique identifier

### Example 
```objc

NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGVerticalApi*apiInstance = [[SWGVerticalApi alloc] init];

// Get verticals by unique identifier
[apiInstance verticalsVerticalIdGetWithVerticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGVertical>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVerticalApi->verticalsVerticalIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGVertical>***](SWGVertical.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verticalsVerticalIdRegionsGet**
```objc
-(NSURLSessionTask*) verticalsVerticalIdRegionsGetWithVerticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get all available regions for the vertical

### Example 
```objc

NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGVerticalApi*apiInstance = [[SWGVerticalApi alloc] init];

// Get all available regions for the vertical
[apiInstance verticalsVerticalIdRegionsGetWithVerticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGVerticalApi->verticalsVerticalIdRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

