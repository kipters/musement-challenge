# SWGSearchResultMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **NSNumber*** | Total search results items | [optional] 
**matchType** | **NSString*** | If the search query string correspond to an exact match, indicates the exact match type: city, country or venue | [optional] 
**matchNames** | **NSArray&lt;NSString*&gt;*** | If the search query string correspond to an exact match, this will contain the name of the exactly matched country, city or venue | [optional] 
**matchIds** | **NSArray&lt;NSString*&gt;*** | If the search query string correspond to an exact match, this will contain the identifier of the exactly matched country, city or venue | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


