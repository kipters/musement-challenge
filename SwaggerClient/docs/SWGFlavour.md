# SWGFlavour

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**active** | **NSNumber*** |  | [optional] 
**img** | **NSString*** |  | [optional] 
**slug** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


