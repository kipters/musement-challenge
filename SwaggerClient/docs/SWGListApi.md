# SWGListApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**citiesCityIdListsGet**](SWGListApi.md#citiescityidlistsget) | **GET** /cities/{cityId}/lists | Get lists for a city
[**listTypesGet**](SWGListApi.md#listtypesget) | **GET** /list-types | Get all List types
[**listsGet**](SWGListApi.md#listsget) | **GET** /lists | Get all lists
[**listsListIdGet**](SWGListApi.md#listslistidget) | **GET** /lists/{listId} | Get list by ID
[**listsListIdRegionsGet**](SWGListApi.md#listslistidregionsget) | **GET** /lists/{listId}/regions | Get all available regions for the list


# **citiesCityIdListsGet**
```objc
-(NSURLSessionTask*) citiesCityIdListsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    xMusementDeviceType: (NSString*) xMusementDeviceType
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    excludeNotTagged: (NSString*) excludeNotTagged
    listtypes: (NSArray<NSString*>*) listtypes
    listtags: (NSArray<NSString*>*) listtags
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGMusementList>* output, NSError* error)) handler;
```

Get lists for a city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementDeviceType = @"xMusementDeviceType_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSString* excludeNotTagged = @"NO"; // If `YES` will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) (optional) (default to NO)
NSArray<NSString*>* listtypes = @[@"listtypes_example"]; // List type to filter by. A collection of list type (optional)
NSArray<NSString*>* listtags = @[@"listtags_example"]; // List of tags to filter by. A collection of tags (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGListApi*apiInstance = [[SWGListApi alloc] init];

// Get lists for a city
[apiInstance citiesCityIdListsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              xMusementDeviceType:xMusementDeviceType
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              excludeNotTagged:excludeNotTagged
              listtypes:listtypes
              listtags:listtags
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGMusementList>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGListApi->citiesCityIdListsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementDeviceType** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **excludeNotTagged** | **NSString***| If &#x60;YES&#x60; will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) | [optional] [default to NO]
 **listtypes** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List type to filter by. A collection of list type | [optional] 
 **listtags** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of tags to filter by. A collection of tags | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGMusementList>***](SWGMusementList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **listTypesGet**
```objc
-(NSURLSessionTask*) listTypesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGListType>* output, NSError* error)) handler;
```

Get all List types

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGListApi*apiInstance = [[SWGListApi alloc] init];

// Get all List types
[apiInstance listTypesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGListType>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGListApi->listTypesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGListType>***](SWGListType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **listsGet**
```objc
-(NSURLSessionTask*) listsGetWithXMusementVersion: (NSString*) xMusementVersion
    xMusementDeviceType: (NSString*) xMusementDeviceType
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    cityIn: (NSArray<NSNumber*>*) cityIn
    countryIn: (NSArray<NSNumber*>*) countryIn
    categoryIn: (NSArray<NSNumber*>*) categoryIn
    vertical: (NSNumber*) vertical
    excludeNotTagged: (NSString*) excludeNotTagged
    temporary: (NSString*) temporary
    listtypes: (NSArray<NSString*>*) listtypes
    listtags: (NSArray<NSString*>*) listtags
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGMusementList>* output, NSError* error)) handler;
```

Get all lists

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementDeviceType = @"xMusementDeviceType_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSArray<NSNumber*>* cityIn = @[@56]; // Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
NSArray<NSNumber*>* countryIn = @[@56]; // Filter, include only results from at least one of the given countries identified by a collection of ids (optional)
NSArray<NSNumber*>* categoryIn = @[@56]; // Filter, include only results from at least one of the given categories identified by a collection of ids (optional)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSString* excludeNotTagged = @"NO"; // If `YES` will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) (optional) (default to NO)
NSString* temporary = @"temporary_example"; // When set to `YES` only temporary exhibitions are returned. (optional)
NSArray<NSString*>* listtypes = @[@"listtypes_example"]; // List type to filter by. A collection of list type (optional)
NSArray<NSString*>* listtags = @[@"listtags_example"]; // List of tags to filter by. A collection of tags (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGListApi*apiInstance = [[SWGListApi alloc] init];

// Get all lists
[apiInstance listsGetWithXMusementVersion:xMusementVersion
              xMusementDeviceType:xMusementDeviceType
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              cityIn:cityIn
              countryIn:countryIn
              categoryIn:categoryIn
              vertical:vertical
              excludeNotTagged:excludeNotTagged
              temporary:temporary
              listtypes:listtypes
              listtags:listtags
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGMusementList>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGListApi->listsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementDeviceType** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **cityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given cities identified by a collection of ids | [optional] 
 **countryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given countries identified by a collection of ids | [optional] 
 **categoryIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given categories identified by a collection of ids | [optional] 
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **excludeNotTagged** | **NSString***| If &#x60;YES&#x60; will return only tagged lists (with at least one tag), otherwise will return also untagged lists (with no tags) | [optional] [default to NO]
 **temporary** | **NSString***| When set to &#x60;YES&#x60; only temporary exhibitions are returned. | [optional] 
 **listtypes** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List type to filter by. A collection of list type | [optional] 
 **listtags** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| List of tags to filter by. A collection of tags | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGMusementList>***](SWGMusementList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **listsListIdGet**
```objc
-(NSURLSessionTask*) listsListIdGetWithListId: (NSNumber*) listId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGMusementList* output, NSError* error)) handler;
```

Get list by ID

### Example 
```objc

NSNumber* listId = @56; // List identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGListApi*apiInstance = [[SWGListApi alloc] init];

// Get list by ID
[apiInstance listsListIdGetWithListId:listId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGMusementList* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGListApi->listsListIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listId** | **NSNumber***| List identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGMusementList***](SWGMusementList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **listsListIdRegionsGet**
```objc
-(NSURLSessionTask*) listsListIdRegionsGetWithListId: (NSNumber*) listId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get all available regions for the list

### Example 
```objc

NSNumber* listId = @56; // List identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGListApi*apiInstance = [[SWGListApi alloc] init];

// Get all available regions for the list
[apiInstance listsListIdRegionsGetWithListId:listId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGListApi->listsListIdRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listId** | **NSNumber***| List identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

