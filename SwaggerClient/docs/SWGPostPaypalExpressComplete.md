# SWGPostPaypalExpressComplete

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderUuid** | **NSString*** | Uuid of Order for which payment will be completed | [optional] 
**token** | **NSString*** | Returned from Paypal in return url | [optional] 
**payerId** | **NSString*** | It is &#39;PayerID&#39; returned from Paypal in return url | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


