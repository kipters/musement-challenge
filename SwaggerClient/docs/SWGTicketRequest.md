# SWGTicketRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**SWGProductRequest***](SWGProductRequest.md) |  | [optional] 
**quantity** | **NSNumber*** |  | [optional] 
**passengersInfo** | [**NSArray&lt;SWGPassengerInfoRequest&gt;***](SWGPassengerInfoRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


