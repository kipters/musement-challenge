# SWGPatchCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstname** | **NSString*** | Customer&#39;s first name | [optional] 
**lastname** | **NSString*** | Customer&#39;s last name | [optional] 
**mobile** | **NSString*** | Customer mobile number with country code. | [optional] 
**city** | **NSString*** | Customer&#39;s city (free text) | [optional] 
**address** | **NSString*** | Customer&#39;s street address | [optional] 
**birthdate** | **NSDate*** | Date of birth of the customer, ICU format: &#39;YYYY-MM-DD&#39; | [optional] 
**gender** | **NSString*** | Customer&#39;s gender. Possible values are:  * MALE: Male  * FEMALE: Female  * OTHER: Other | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


