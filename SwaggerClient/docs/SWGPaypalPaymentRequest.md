# SWGPaypalPaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **NSString*** | Can be Order.id or Order.uuid. | [optional] 
**paypalPaymentId** | **NSString*** | Paypal \&quot;payment id\&quot; obtained on \&quot;client side\&quot;. | [optional] 
**clientIp** | **NSString*** | [optional] User IP address for logging purposes. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


