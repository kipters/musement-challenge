# SWGGiftCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**code** | **NSString*** |  | [optional] 
**validFrom** | **NSDate*** |  | [optional] 
**validUntil** | **NSDate*** |  | [optional] 
**created** | **NSDate*** |  | [optional] 
**updated** | **NSDate*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


