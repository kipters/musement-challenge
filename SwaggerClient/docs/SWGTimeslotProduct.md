# SWGTimeslotProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**event** | [**SWGEvent***](SWGEvent.md) |  | [optional] 
**priceTag** | [**SWGPricetag***](SWGPricetag.md) |  | [optional] 
**maxBuy** | **NSNumber*** | Maximum purchasable quantity. | [optional] 
**minBuy** | **NSNumber*** | Minimum purchasable quantity. | [optional] 
**retailPrice** | [**SWGPrice***](SWGPrice.md) | Price paid by the customer. RetailPriceSupplier - Discount. | [optional] 
**supplierPrice** | [**SWGPrice***](SWGPrice.md) | Price we pay to the supplier. | [optional] 
**discountAmount** | [**SWGPrice***](SWGPrice.md) | Discount amount. | [optional] 
**serviceFee** | [**SWGPrice***](SWGPrice.md) | Service fee. Always included in the retailPrice. Show it to the user is a decisione of the client application. | [optional] 
**datetime** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


