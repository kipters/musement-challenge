# SWGPostTimeslotProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierPrice** | **NSString*** | Activity supplier price | [optional] 
**retailPriceSupplier** | **NSString*** | Activity retail price | [optional] 
**currency** | **NSString*** | Currency | Use &#39;code&#39; field from GET /api/v3/currencies | [optional] 
**holderCode** | **NSString*** | Holder code | [optional] 
**featureCode** | **NSString*** | Feature code | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


