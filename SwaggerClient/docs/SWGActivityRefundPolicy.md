# SWGActivityRefundPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **NSString*** | The unique identifier of the refund policy | [optional] 
**period** | **NSString*** | Follow the ISO_8601 format specification for duration. | [optional] 
**type** | **NSString*** |       *         ABSOLUTE: means it is an absolute amount for the refund quota, if given currency_code is returned      *         PERCENTAGE: means it is a percentage amount for the refund quota, if given currency_code will not be returned      *      | [optional] 
**value** | **NSNumber*** | The amount of the refund quota | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


