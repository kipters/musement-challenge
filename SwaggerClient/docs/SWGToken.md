# SWGToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **NSString*** | The new generated access token | [optional] 
**refreshToken** | **NSString*** | The refresh token | [optional] 
**expiresIn** | **NSNumber*** | Seconds the token will expire in | [optional] 
**tokenType** | **NSString*** | The type of the token | [optional] 
**scope** | **NSString*** | The authentication request scope | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


