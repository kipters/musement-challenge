# SWGSupplierRegistrationApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierRegistrationsPost**](SWGSupplierRegistrationApi.md#supplierregistrationspost) | **POST** /supplier-registrations | Register a new supplier
[**suppliersSupplierUuidActivatePatch**](SWGSupplierRegistrationApi.md#supplierssupplieruuidactivatepatch) | **PATCH** /suppliers/{supplierUuid}/activate | Activate a supplier


# **supplierRegistrationsPost**
```objc
-(NSURLSessionTask*) supplierRegistrationsPostWithPostSupplierRegistration: (SWGPostSupplierRegistration*) postSupplierRegistration
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;
```

Register a new supplier

### Example 
```objc

SWGPostSupplierRegistration* postSupplierRegistration = [[SWGPostSupplierRegistration alloc] init]; // Supplier registrations data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierRegistrationApi*apiInstance = [[SWGSupplierRegistrationApi alloc] init];

// Register a new supplier
[apiInstance supplierRegistrationsPostWithPostSupplierRegistration:postSupplierRegistration
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGSupplier* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierRegistrationApi->supplierRegistrationsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postSupplierRegistration** | [**SWGPostSupplierRegistration***](SWGPostSupplierRegistration.md)| Supplier registrations data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGSupplier***](SWGSupplier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **suppliersSupplierUuidActivatePatch**
```objc
-(NSURLSessionTask*) suppliersSupplierUuidActivatePatchWithSupplierUuid: (NSString*) supplierUuid
    postSupplierActivation: (SWGPostSupplierActivation*) postSupplierActivation
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGSupplier* output, NSError* error)) handler;
```

Activate a supplier

### Example 
```objc

NSString* supplierUuid = @"supplierUuid_example"; // Supplier UUID
SWGPostSupplierActivation* postSupplierActivation = [[SWGPostSupplierActivation alloc] init]; // Supplier activation data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGSupplierRegistrationApi*apiInstance = [[SWGSupplierRegistrationApi alloc] init];

// Activate a supplier
[apiInstance suppliersSupplierUuidActivatePatchWithSupplierUuid:supplierUuid
              postSupplierActivation:postSupplierActivation
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGSupplier* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSupplierRegistrationApi->suppliersSupplierUuidActivatePatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierUuid** | **NSString***| Supplier UUID | 
 **postSupplierActivation** | [**SWGPostSupplierActivation***](SWGPostSupplierActivation.md)| Supplier activation data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGSupplier***](SWGSupplier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

