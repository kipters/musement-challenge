# SWGGiftCreationProductWithQuantity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**holderCode** | **NSString*** |  | [optional] 
**quantity** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


