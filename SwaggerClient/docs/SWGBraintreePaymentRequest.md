# SWGBraintreePaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **NSNumber*** |  | [optional] 
**clientIp** | **NSString*** | [optional] User IP address for logging purposes. | [optional] 
**cardholderName** | **NSString*** | [optional] Credit card holder name. Required for iOS integration because its missing in \&quot;tokenizeCard\&quot; function there. Include it only if you pass \&quot;nonce\&quot; parameter. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


