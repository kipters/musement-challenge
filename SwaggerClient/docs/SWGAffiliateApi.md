# SWGAffiliateApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**affiliatesAffiliateCodeGet**](SWGAffiliateApi.md#affiliatesaffiliatecodeget) | **GET** /affiliates/{affiliateCode} | Get an affiliate by code


# **affiliatesAffiliateCodeGet**
```objc
-(NSURLSessionTask*) affiliatesAffiliateCodeGetWithAffiliateCode: (NSString*) affiliateCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGAffiliate* output, NSError* error)) handler;
```

Get an affiliate by code

### Example 
```objc

NSString* affiliateCode = @"affiliateCode_example"; // Affiliate code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGAffiliateApi*apiInstance = [[SWGAffiliateApi alloc] init];

// Get an affiliate by code
[apiInstance affiliatesAffiliateCodeGetWithAffiliateCode:affiliateCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGAffiliate* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAffiliateApi->affiliatesAffiliateCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **affiliateCode** | **NSString***| Affiliate code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGAffiliate***](SWGAffiliate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

