# SWGBlogpostBlockImage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **NSString*** |  | [optional] 
**caption** | **NSString*** |  | [optional] 
**shareable** | **NSNumber*** |  | [optional] 
**_id** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


