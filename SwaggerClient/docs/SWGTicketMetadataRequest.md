# SWGTicketMetadataRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **NSString*** |  | [optional] 
**note** | **NSString*** |  | [optional] 
**extendedFields** | [**NSArray&lt;SWGExtendedField&gt;***](SWGExtendedField.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


