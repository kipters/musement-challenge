# SWGDefaultApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**citiesCityIdRegionsGet**](SWGDefaultApi.md#citiescityidregionsget) | **GET** /cities/{cityId}/regions | Get the regions for the city
[**signupExternalProviderPost**](SWGDefaultApi.md#signupexternalproviderpost) | **POST** /signup/external_provider | Register a new customer using external provider data
[**signupPost**](SWGDefaultApi.md#signuppost) | **POST** /signup | Begin a new signup process
[**signupSoftPost**](SWGDefaultApi.md#signupsoftpost) | **POST** /signup/soft | Register a new customer setting the softpassword


# **citiesCityIdRegionsGet**
```objc
-(NSURLSessionTask*) citiesCityIdRegionsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get the regions for the city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGDefaultApi*apiInstance = [[SWGDefaultApi alloc] init];

// Get the regions for the city
[apiInstance citiesCityIdRegionsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDefaultApi->citiesCityIdRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **signupExternalProviderPost**
```objc
-(NSURLSessionTask*) signupExternalProviderPostWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCustomer* output, NSError* error)) handler;
```

Register a new customer using external provider data

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGDefaultApi*apiInstance = [[SWGDefaultApi alloc] init];

// Register a new customer using external provider data
[apiInstance signupExternalProviderPostWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDefaultApi->signupExternalProviderPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCustomer***](SWGCustomer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **signupPost**
```objc
-(NSURLSessionTask*) signupPostWithSignup: (SWGSignupByEmailRequest*) signup
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Begin a new signup process

### Example 
```objc

SWGSignupByEmailRequest* signup = [[SWGSignupByEmailRequest alloc] init]; // Signup information
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGDefaultApi*apiInstance = [[SWGDefaultApi alloc] init];

// Begin a new signup process
[apiInstance signupPostWithSignup:signup
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGDefaultApi->signupPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **signup** | [**SWGSignupByEmailRequest***](SWGSignupByEmailRequest.md)| Signup information | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **signupSoftPost**
```objc
-(NSURLSessionTask*) signupSoftPostWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSError* error)) handler;
```

Register a new customer setting the softpassword

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGDefaultApi*apiInstance = [[SWGDefaultApi alloc] init];

// Register a new customer setting the softpassword
[apiInstance signupSoftPostWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGDefaultApi->signupSoftPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

