# SWGPutTimeslotProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierPrice** | **NSString*** | Price we pay to the supplier | [optional] 
**retailPriceSupplier** | **NSString*** | Retail price for the supplier | [optional] 
**currency** | **NSString*** | Currency | Use &#39;code&#39; field from GET /api/v3/currencies | [optional] 
**holderCode** | **NSString*** | Holder code | [optional] 
**featureCode** | **NSString*** | Feature code | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


