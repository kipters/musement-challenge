# SWGBlogger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**bio** | **NSString*** |  | [optional] 
**avatar** | **NSString*** |  | [optional] 
**socialUrl** | **NSString*** |  | [optional] 
**website** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


