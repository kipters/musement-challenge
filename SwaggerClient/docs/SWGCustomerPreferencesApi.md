# SWGCustomerPreferencesApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customersMePreferencesGet**](SWGCustomerPreferencesApi.md#customersmepreferencesget) | **GET** /customers/me/preferences | Get preferences for logged in customer
[**customersMePreferencesPatch**](SWGCustomerPreferencesApi.md#customersmepreferencespatch) | **PATCH** /customers/me/preferences | Update customer preferences data


# **customersMePreferencesGet**
```objc
-(NSURLSessionTask*) customersMePreferencesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCustomerPreferences* output, NSError* error)) handler;
```

Get preferences for logged in customer

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCustomerPreferencesApi*apiInstance = [[SWGCustomerPreferencesApi alloc] init];

// Get preferences for logged in customer
[apiInstance customersMePreferencesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCustomerPreferences* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerPreferencesApi->customersMePreferencesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCustomerPreferences***](SWGCustomerPreferences.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customersMePreferencesPatch**
```objc
-(NSURLSessionTask*) customersMePreferencesPatchWithCustomerPreference: (SWGPatchCustomerPreferences*) customerPreference
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCustomerPreferences* output, NSError* error)) handler;
```

Update customer preferences data

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure OAuth2 access token for authorization: (authentication scheme: customer_oauth)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


SWGPatchCustomerPreferences* customerPreference = [[SWGPatchCustomerPreferences alloc] init]; // Customer preference data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCustomerPreferencesApi*apiInstance = [[SWGCustomerPreferencesApi alloc] init];

// Update customer preferences data
[apiInstance customersMePreferencesPatchWithCustomerPreference:customerPreference
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCustomerPreferences* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCustomerPreferencesApi->customersMePreferencesPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerPreference** | [**SWGPatchCustomerPreferences***](SWGPatchCustomerPreferences.md)| Customer preference data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCustomerPreferences***](SWGCustomerPreferences.md)

### Authorization

[customer_oauth](../README.md#customer_oauth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

