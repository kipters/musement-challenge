# SWGPostActivityPoi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **NSNumber*** | POI latitude | [optional] 
**longitude** | **NSNumber*** | POI longitude | [optional] 
**type** | **NSArray&lt;NSString*&gt;*** | POI type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


