# SWGPromoCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |  | [optional] 
**active** | **NSNumber*** |  | [optional] 
**percentage** | **NSNumber*** |  | [optional] 
**discount** | **NSNumber*** |  | [optional] 
**maxUsage** | **NSNumber*** |  | [optional] 
**validFrom** | **NSDate*** |  | [optional] 
**validUntil** | **NSDate*** |  | [optional] 
**minimumAmount** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


