# SWGCustomerTravelWith

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** |   * ALONE : Alone,  * PARTNER : Partner,  * FAMILY : Family,  * FRIENDS : Friends,  * GROUP : Group | [optional] 
**name** | **NSString*** | Customer&#39;s travel with name. | This value depends on the value of the header Accept-Language | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


