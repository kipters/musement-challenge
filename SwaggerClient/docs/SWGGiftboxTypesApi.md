# SWGGiftboxTypesApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**giftboxTypesGet**](SWGGiftboxTypesApi.md#giftboxtypesget) | **GET** /giftbox-types | Get all giftbox types sorted by relevance
[**giftboxTypesGiftboxCodeRegionsGet**](SWGGiftboxTypesApi.md#giftboxtypesgiftboxcoderegionsget) | **GET** /giftbox-types/{giftboxCode}/regions | Get all regions for a giftbox
[**giftboxTypesGiftboxTypeCodeCityOptionsGet**](SWGGiftboxTypesApi.md#giftboxtypesgiftboxtypecodecityoptionsget) | **GET** /giftbox-types/{giftboxTypeCode}/city-options | Get giftbox related city in an option list format
[**giftboxTypesGiftboxTypeCodeGet**](SWGGiftboxTypesApi.md#giftboxtypesgiftboxtypecodeget) | **GET** /giftbox-types/{giftboxTypeCode} | Get a giftbox type by code
[**giftboxTypesGiftboxTypeCodeItemsGet**](SWGGiftboxTypesApi.md#giftboxtypesgiftboxtypecodeitemsget) | **GET** /giftbox-types/{giftboxTypeCode}/items | Get items related to a giftboxe type
[**giftboxTypesGiftboxTypeCodeRelatedGet**](SWGGiftboxTypesApi.md#giftboxtypesgiftboxtypecoderelatedget) | **GET** /giftbox-types/{giftboxTypeCode}/related | Get related giftboxes


# **giftboxTypesGet**
```objc
-(NSURLSessionTask*) giftboxTypesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGGiftboxType>* output, NSError* error)) handler;
```

Get all giftbox types sorted by relevance

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGGiftboxTypesApi*apiInstance = [[SWGGiftboxTypesApi alloc] init];

// Get all giftbox types sorted by relevance
[apiInstance giftboxTypesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGGiftboxType>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxTypesApi->giftboxTypesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGGiftboxType>***](SWGGiftboxType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **giftboxTypesGiftboxCodeRegionsGet**
```objc
-(NSURLSessionTask*) giftboxTypesGiftboxCodeRegionsGetWithGiftboxCode: (NSString*) giftboxCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get all regions for a giftbox

### Example 
```objc

NSString* giftboxCode = @"giftboxCode_example"; // Giftbox redeem code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGGiftboxTypesApi*apiInstance = [[SWGGiftboxTypesApi alloc] init];

// Get all regions for a giftbox
[apiInstance giftboxTypesGiftboxCodeRegionsGetWithGiftboxCode:giftboxCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxTypesApi->giftboxTypesGiftboxCodeRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftboxCode** | **NSString***| Giftbox redeem code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **giftboxTypesGiftboxTypeCodeCityOptionsGet**
```objc
-(NSURLSessionTask*) giftboxTypesGiftboxTypeCodeCityOptionsGetWithGiftboxTypeCode: (NSString*) giftboxTypeCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGSelectOption>* output, NSError* error)) handler;
```

Get giftbox related city in an option list format

### Example 
```objc

NSString* giftboxTypeCode = @"giftboxTypeCode_example"; // Giftbox identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGGiftboxTypesApi*apiInstance = [[SWGGiftboxTypesApi alloc] init];

// Get giftbox related city in an option list format
[apiInstance giftboxTypesGiftboxTypeCodeCityOptionsGetWithGiftboxTypeCode:giftboxTypeCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGSelectOption>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxTypesApi->giftboxTypesGiftboxTypeCodeCityOptionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftboxTypeCode** | **NSString***| Giftbox identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGSelectOption>***](SWGSelectOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **giftboxTypesGiftboxTypeCodeGet**
```objc
-(NSURLSessionTask*) giftboxTypesGiftboxTypeCodeGetWithGiftboxTypeCode: (NSString*) giftboxTypeCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGGiftboxType* output, NSError* error)) handler;
```

Get a giftbox type by code

### Example 
```objc

NSString* giftboxTypeCode = @"giftboxTypeCode_example"; // Giftbox identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGGiftboxTypesApi*apiInstance = [[SWGGiftboxTypesApi alloc] init];

// Get a giftbox type by code
[apiInstance giftboxTypesGiftboxTypeCodeGetWithGiftboxTypeCode:giftboxTypeCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGGiftboxType* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxTypesApi->giftboxTypesGiftboxTypeCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftboxTypeCode** | **NSString***| Giftbox identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGGiftboxType***](SWGGiftboxType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **giftboxTypesGiftboxTypeCodeItemsGet**
```objc
-(NSURLSessionTask*) giftboxTypesGiftboxTypeCodeItemsGetWithGiftboxTypeCode: (NSString*) giftboxTypeCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    text: (NSString*) text
    numberOfPeople: (NSNumber*) numberOfPeople
    cityIn: (NSArray<NSNumber*>*) cityIn
    limit: (NSNumber*) limit
    offset: (NSNumber*) offset
    page: (NSNumber*) page
        completionHandler: (void (^)(NSArray<SWGGiftboxTypeItem>* output, NSError* error)) handler;
```

Get items related to a giftboxe type

### Example 
```objc

NSString* giftboxTypeCode = @"giftboxTypeCode_example"; // Giftbox identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* text = @"text_example"; // Text to search (optional)
NSNumber* numberOfPeople = @56; // Number of people (optional)
NSArray<NSNumber*>* cityIn = @[@56]; // Filter, include only results from at least one of the given cities identified by a collection of ids (optional)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)

SWGGiftboxTypesApi*apiInstance = [[SWGGiftboxTypesApi alloc] init];

// Get items related to a giftboxe type
[apiInstance giftboxTypesGiftboxTypeCodeItemsGetWithGiftboxTypeCode:giftboxTypeCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              text:text
              numberOfPeople:numberOfPeople
              cityIn:cityIn
              limit:limit
              offset:offset
              page:page
          completionHandler: ^(NSArray<SWGGiftboxTypeItem>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxTypesApi->giftboxTypesGiftboxTypeCodeItemsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftboxTypeCode** | **NSString***| Giftbox identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **text** | **NSString***| Text to search | [optional] 
 **numberOfPeople** | **NSNumber***| Number of people | [optional] 
 **cityIn** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Filter, include only results from at least one of the given cities identified by a collection of ids | [optional] 
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 

### Return type

[**NSArray<SWGGiftboxTypeItem>***](SWGGiftboxTypeItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **giftboxTypesGiftboxTypeCodeRelatedGet**
```objc
-(NSURLSessionTask*) giftboxTypesGiftboxTypeCodeRelatedGetWithGiftboxTypeCode: (NSString*) giftboxTypeCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGGiftboxType>* output, NSError* error)) handler;
```

Get related giftboxes

### Example 
```objc

NSString* giftboxTypeCode = @"giftboxTypeCode_example"; // Giftbox identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGGiftboxTypesApi*apiInstance = [[SWGGiftboxTypesApi alloc] init];

// Get related giftboxes
[apiInstance giftboxTypesGiftboxTypeCodeRelatedGetWithGiftboxTypeCode:giftboxTypeCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGGiftboxType>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftboxTypesApi->giftboxTypesGiftboxTypeCodeRelatedGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftboxTypeCode** | **NSString***| Giftbox identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGGiftboxType>***](SWGGiftboxType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

