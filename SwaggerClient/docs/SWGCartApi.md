# SWGCartApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cartsCartIdBundlesGet**](SWGCartApi.md#cartscartidbundlesget) | **GET** /carts/{cartId}/bundles | Get the bundle in a cart
[**cartsCartUuidFormGet**](SWGCartApi.md#cartscartuuidformget) | **GET** /carts/{cartUuid}/form | Get the form associated to a cart
[**cartsCartUuidGet**](SWGCartApi.md#cartscartuuidget) | **GET** /carts/{cartUuid} | Get a cart by unique identifier
[**cartsCartUuidGiftsGiftCodePut**](SWGCartApi.md#cartscartuuidgiftsgiftcodeput) | **PUT** /carts/{cartUuid}/gifts/{giftCode} | Redeem gift code with chosen date+time+language. Appropriate Tickets with price 0 will be added to Cart
[**cartsCartUuidGiftsPost**](SWGCartApi.md#cartscartuuidgiftspost) | **POST** /carts/{cartUuid}/gifts | Create tailored Gift and add it to Cart
[**cartsCartUuidItemsPost**](SWGCartApi.md#cartscartuuiditemspost) | **POST** /carts/{cartUuid}/items | Add an items to a cart
[**cartsCartUuidPatch**](SWGCartApi.md#cartscartuuidpatch) | **PATCH** /carts/{cartUuid} | Update a cart
[**cartsCartUuidPromoIntentPost**](SWGCartApi.md#cartscartuuidpromointentpost) | **POST** /carts/{cartUuid}/promo-intent | Generate a promo intent
[**cartsCartUuidPut**](SWGCartApi.md#cartscartuuidput) | **PUT** /carts/{cartUuid} | Replace a Cart
[**cartsCartUuidTicketsPost**](SWGCartApi.md#cartscartuuidticketspost) | **POST** /carts/{cartUuid}/tickets | Add tickets to a cart | Deprecated infavour of POST /carts/{cartUuid}/items
[**cartsPost**](SWGCartApi.md#cartspost) | **POST** /carts | Create a cart


# **cartsCartIdBundlesGet**
```objc
-(NSURLSessionTask*) cartsCartIdBundlesGetWithCartId: (NSNumber*) cartId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGBundle* output, NSError* error)) handler;
```

Get the bundle in a cart

### Example 
```objc

NSNumber* cartId = @56; // Cart identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Get the bundle in a cart
[apiInstance cartsCartIdBundlesGetWithCartId:cartId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGBundle* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartIdBundlesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartId** | **NSNumber***| Cart identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGBundle***](SWGBundle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidFormGet**
```objc
-(NSURLSessionTask*) cartsCartUuidFormGetWithCartUuid: (NSString*) cartUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCheckoutForm* output, NSError* error)) handler;
```

Get the form associated to a cart

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Get the form associated to a cart
[apiInstance cartsCartUuidFormGetWithCartUuid:cartUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCheckoutForm* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidFormGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCheckoutForm***](SWGCheckoutForm.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidGet**
```objc
-(NSURLSessionTask*) cartsCartUuidGetWithCartUuid: (NSString*) cartUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGCart* output, NSError* error)) handler;
```

Get a cart by unique identifier

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Get a cart by unique identifier
[apiInstance cartsCartUuidGetWithCartUuid:cartUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGCart* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGCart***](SWGCart.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidGiftsGiftCodePut**
```objc
-(NSURLSessionTask*) cartsCartUuidGiftsGiftCodePutWithCartUuid: (NSString*) cartUuid
    giftCode: (NSString*) giftCode
    giftRedeemRequest: (SWGGiftRedeemRequest*) giftRedeemRequest
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGResponseWithMessage* output, NSError* error)) handler;
```

Redeem gift code with chosen date+time+language. Appropriate Tickets with price 0 will be added to Cart

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
NSString* giftCode = @"giftCode_example"; // Gift code
SWGGiftRedeemRequest* giftRedeemRequest = [[SWGGiftRedeemRequest alloc] init]; // Gift redeem request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Redeem gift code with chosen date+time+language. Appropriate Tickets with price 0 will be added to Cart
[apiInstance cartsCartUuidGiftsGiftCodePutWithCartUuid:cartUuid
              giftCode:giftCode
              giftRedeemRequest:giftRedeemRequest
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGResponseWithMessage* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidGiftsGiftCodePut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **giftCode** | **NSString***| Gift code | 
 **giftRedeemRequest** | [**SWGGiftRedeemRequest***](SWGGiftRedeemRequest.md)| Gift redeem request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGResponseWithMessage***](SWGResponseWithMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidGiftsPost**
```objc
-(NSURLSessionTask*) cartsCartUuidGiftsPostWithCartUuid: (NSString*) cartUuid
    giftCreationRequest: (SWGGiftCreationRequest*) giftCreationRequest
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGResponseWithMessage* output, NSError* error)) handler;
```

Create tailored Gift and add it to Cart

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
SWGGiftCreationRequest* giftCreationRequest = [[SWGGiftCreationRequest alloc] init]; // Gift creation request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Create tailored Gift and add it to Cart
[apiInstance cartsCartUuidGiftsPostWithCartUuid:cartUuid
              giftCreationRequest:giftCreationRequest
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGResponseWithMessage* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidGiftsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **giftCreationRequest** | [**SWGGiftCreationRequest***](SWGGiftCreationRequest.md)| Gift creation request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGResponseWithMessage***](SWGResponseWithMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidItemsPost**
```objc
-(NSURLSessionTask*) cartsCartUuidItemsPostWithCartUuid: (NSString*) cartUuid
    cart: (SWGPostCartItem*) cart
    xMusementVersion: (NSString*) xMusementVersion
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGCart* output, NSError* error)) handler;
```

Add an items to a cart

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
SWGPostCartItem* cart = [[SWGPostCartItem alloc] init]; // Product request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Add an items to a cart
[apiInstance cartsCartUuidItemsPostWithCartUuid:cartUuid
              cart:cart
              xMusementVersion:xMusementVersion
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGCart* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidItemsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **cart** | [**SWGPostCartItem***](SWGPostCartItem.md)| Product request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGCart***](SWGCart.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidPatch**
```objc
-(NSURLSessionTask*) cartsCartUuidPatchWithCartUuid: (NSString*) cartUuid
    cart: (SWGCartRequest*) cart
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGCart* output, NSError* error)) handler;
```

Update a cart

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
SWGCartRequest* cart = [[SWGCartRequest alloc] init]; // Cart request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Update a cart
[apiInstance cartsCartUuidPatchWithCartUuid:cartUuid
              cart:cart
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGCart* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidPatch: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **cart** | [**SWGCartRequest***](SWGCartRequest.md)| Cart request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGCart***](SWGCart.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidPromoIntentPost**
```objc
-(NSURLSessionTask*) cartsCartUuidPromoIntentPostWithCartUuid: (NSString*) cartUuid
    promoIntent: (SWGPostPromoIntent*) promoIntent
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGPromoIntent* output, NSError* error)) handler;
```

Generate a promo intent

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
SWGPostPromoIntent* promoIntent = [[SWGPostPromoIntent alloc] init]; // Promo intent data
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Generate a promo intent
[apiInstance cartsCartUuidPromoIntentPostWithCartUuid:cartUuid
              promoIntent:promoIntent
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGPromoIntent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidPromoIntentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **promoIntent** | [**SWGPostPromoIntent***](SWGPostPromoIntent.md)| Promo intent data | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGPromoIntent***](SWGPromoIntent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidPut**
```objc
-(NSURLSessionTask*) cartsCartUuidPutWithCartUuid: (NSString*) cartUuid
    cart: (SWGCartRequest*) cart
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGCart* output, NSError* error)) handler;
```

Replace a Cart

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
SWGCartRequest* cart = [[SWGCartRequest alloc] init]; // Cart request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Replace a Cart
[apiInstance cartsCartUuidPutWithCartUuid:cartUuid
              cart:cart
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGCart* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidPut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **cart** | [**SWGCartRequest***](SWGCartRequest.md)| Cart request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGCart***](SWGCart.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsCartUuidTicketsPost**
```objc
-(NSURLSessionTask*) cartsCartUuidTicketsPostWithCartUuid: (NSString*) cartUuid
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGCart* output, NSError* error)) handler;
```

Add tickets to a cart | Deprecated infavour of POST /carts/{cartUuid}/items

### Example 
```objc

NSString* cartUuid = @"cartUuid_example"; // Cart identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Add tickets to a cart | Deprecated infavour of POST /carts/{cartUuid}/items
[apiInstance cartsCartUuidTicketsPostWithCartUuid:cartUuid
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGCart* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsCartUuidTicketsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cartUuid** | **NSString***| Cart identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGCart***](SWGCart.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartsPost**
```objc
-(NSURLSessionTask*) cartsPostWithCart: (SWGCartRequest*) cart
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGCart* output, NSError* error)) handler;
```

Create a cart

### Example 
```objc

SWGCartRequest* cart = [[SWGCartRequest alloc] init]; // Cart request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGCartApi*apiInstance = [[SWGCartApi alloc] init];

// Create a cart
[apiInstance cartsPostWithCart:cart
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGCart* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCartApi->cartsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart** | [**SWGCartRequest***](SWGCartRequest.md)| Cart request | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGCart***](SWGCart.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

