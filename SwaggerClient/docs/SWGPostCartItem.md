# SWGPostCartItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **NSString*** | Product type | Can be &#39;musement-giftbox&#39; | [optional] 
**productIdentifier** | **NSString*** | Product unique identifier | Can be a numeric or alphanumeric identifier depending of the type of product. | [optional] 
**quantity** | **NSString*** | Quantity | [optional] 
**productSpecificData** | [**SWGPostGiftboxCustomizationInfo***](SWGPostGiftboxCustomizationInfo.md) | Extra product information. Model changed depending on the &#39;type&#39; of product | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


