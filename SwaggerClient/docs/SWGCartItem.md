# SWGCartItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **NSString*** | Unique identifier on Musement platform | [optional] 
**code** | **NSString*** | Unique public identifier | [optional] 
**product** | [**SWGGiftbox***](SWGGiftbox.md) |  | [optional] 
**quantity** | **NSNumber*** |  | [optional] 
**totalPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**status** | **NSString*** |  | [optional] 
**vouchers** | **NSArray&lt;NSString*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


