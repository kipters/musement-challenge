# SWGCheckoutForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**NSArray&lt;SWGFormFieldDefinition&gt;***](SWGFormFieldDefinition.md) |  | [optional] 
**tickets** | [**NSArray&lt;SWGFormTicket&gt;***](SWGFormTicket.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


