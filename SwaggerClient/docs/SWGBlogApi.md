# SWGBlogApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blogpostsCategoriesGet**](SWGBlogApi.md#blogpostscategoriesget) | **GET** /blogposts-categories | Get all available blogpost categories
[**blogpostsCategoriesIdGet**](SWGBlogApi.md#blogpostscategoriesidget) | **GET** /blogposts-categories/{id} | Get a specific blogpost category
[**blogpostsGet**](SWGBlogApi.md#blogpostsget) | **GET** /blogposts | Get a blogposts
[**blogpostsIdGet**](SWGBlogApi.md#blogpostsidget) | **GET** /blogposts/{id} | Get blogpost by identifier
[**blogpostsIdPositionGet**](SWGBlogApi.md#blogpostsidpositionget) | **GET** /blogposts/{id}/{position} | Get previous o next blogposts
[**citiesCityIdBlogpostsGet**](SWGBlogApi.md#citiescityidblogpostsget) | **GET** /cities/{cityId}/blogposts | Get the blogposts for the city


# **blogpostsCategoriesGet**
```objc
-(NSURLSessionTask*) blogpostsCategoriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGBlogpostCategoryAggregated>* output, NSError* error)) handler;
```

Get all available blogpost categories

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGBlogApi*apiInstance = [[SWGBlogApi alloc] init];

// Get all available blogpost categories
[apiInstance blogpostsCategoriesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGBlogpostCategoryAggregated>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBlogApi->blogpostsCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGBlogpostCategoryAggregated>***](SWGBlogpostCategoryAggregated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blogpostsCategoriesIdGet**
```objc
-(NSURLSessionTask*) blogpostsCategoriesIdGetWithId: (NSNumber*) _id
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGBlogpostCategory* output, NSError* error)) handler;
```

Get a specific blogpost category

### Example 
```objc

NSNumber* _id = @56; // Category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGBlogApi*apiInstance = [[SWGBlogApi alloc] init];

// Get a specific blogpost category
[apiInstance blogpostsCategoriesIdGetWithId:_id
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGBlogpostCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBlogApi->blogpostsCategoriesIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSNumber***| Category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGBlogpostCategory***](SWGBlogpostCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blogpostsGet**
```objc
-(NSURLSessionTask*) blogpostsGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    blogpostCategory: (NSString*) blogpostCategory
    homePageFeatured: (NSNumber*) homePageFeatured
    category: (NSNumber*) category
    vertical: (NSNumber*) vertical
    list: (NSString*) list
    city: (NSString*) city
    venue: (NSNumber*) venue
    page: (NSNumber*) page
    sortBy: (NSArray<NSString*>*) sortBy
        completionHandler: (void (^)(NSArray<SWGBlogpost>* output, NSError* error)) handler;
```

Get a blogposts

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* blogpostCategory = @"blogpostCategory_example"; // The blog post category uuid to filter by (optional)
NSNumber* homePageFeatured = @false; // If true will include only home page featured posts (optional) (default to false)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSString* list = @"list_example"; // List identifier to filter by (optional)
NSString* city = @"city_example"; // City identifier to filter by (optional)
NSNumber* venue = @56; // Venue identifier (optional)
NSNumber* page = @56; // Page from which starting to return found events (mandatory if limit is given) (optional)
NSArray<NSString*>* sortBy = @[@"sortBy_example"]; // Ordering criteria: [publish_date|relevance], prepend `-` for descending order (optional)

SWGBlogApi*apiInstance = [[SWGBlogApi alloc] init];

// Get a blogposts
[apiInstance blogpostsGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
              blogpostCategory:blogpostCategory
              homePageFeatured:homePageFeatured
              category:category
              vertical:vertical
              list:list
              city:city
              venue:venue
              page:page
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGBlogpost>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBlogApi->blogpostsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **blogpostCategory** | **NSString***| The blog post category uuid to filter by | [optional] 
 **homePageFeatured** | **NSNumber***| If true will include only home page featured posts | [optional] [default to false]
 **category** | **NSNumber***| Category identifier | [optional] 
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **list** | **NSString***| List identifier to filter by | [optional] 
 **city** | **NSString***| City identifier to filter by | [optional] 
 **venue** | **NSNumber***| Venue identifier | [optional] 
 **page** | **NSNumber***| Page from which starting to return found events (mandatory if limit is given) | [optional] 
 **sortBy** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Ordering criteria: [publish_date|relevance], prepend &#x60;-&#x60; for descending order | [optional] 

### Return type

[**NSArray<SWGBlogpost>***](SWGBlogpost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blogpostsIdGet**
```objc
-(NSURLSessionTask*) blogpostsIdGetWithId: (NSString*) _id
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGBlogpost* output, NSError* error)) handler;
```

Get blogpost by identifier

### Example 
```objc

NSString* _id = @"_id_example"; // Blogpost unique identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGBlogApi*apiInstance = [[SWGBlogApi alloc] init];

// Get blogpost by identifier
[apiInstance blogpostsIdGetWithId:_id
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGBlogpost* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBlogApi->blogpostsIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Blogpost unique identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGBlogpost***](SWGBlogpost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blogpostsIdPositionGet**
```objc
-(NSURLSessionTask*) blogpostsIdPositionGetWithId: (NSString*) _id
    position: (NSString*) position
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGBlogpost* output, NSError* error)) handler;
```

Get previous o next blogposts

### Example 
```objc

NSString* _id = @"_id_example"; // Blogpost unique identifier
NSString* position = @"position_example"; // Indicate if you want the prev or the next post
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGBlogApi*apiInstance = [[SWGBlogApi alloc] init];

// Get previous o next blogposts
[apiInstance blogpostsIdPositionGetWithId:_id
              position:position
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGBlogpost* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBlogApi->blogpostsIdPositionGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Blogpost unique identifier | 
 **position** | **NSString***| Indicate if you want the prev or the next post | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGBlogpost***](SWGBlogpost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **citiesCityIdBlogpostsGet**
```objc
-(NSURLSessionTask*) citiesCityIdBlogpostsGetWithCityId: (NSNumber*) cityId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGBlogpost>* output, NSError* error)) handler;
```

Get the blogposts for the city

### Example 
```objc

NSNumber* cityId = @56; // City identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGBlogApi*apiInstance = [[SWGBlogApi alloc] init];

// Get the blogposts for the city
[apiInstance citiesCityIdBlogpostsGetWithCityId:cityId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGBlogpost>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBlogApi->citiesCityIdBlogpostsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cityId** | **NSNumber***| City identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGBlogpost>***](SWGBlogpost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

