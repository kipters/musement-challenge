# SWGWidgetConfigurationSteps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stepCoverBox** | [**SWGWidgetConfigurationStepCoverBox***](SWGWidgetConfigurationStepCoverBox.md) |  | [optional] 
**stepCalendarBox** | [**SWGWidgetConfigurationStepCalendarBox***](SWGWidgetConfigurationStepCalendarBox.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


