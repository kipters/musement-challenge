# SWGCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**name** | **NSString*** |  | 
**level** | **NSString*** |  | 
**code** | **NSString*** |  | 
**eventImageUrl** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | 
**url** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


