# SWGCountryApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countriesCountryIdActivitiesGet**](SWGCountryApi.md#countriescountryidactivitiesget) | **GET** /countries/{countryId}/activities | Get all activities for a country
[**countriesCountryIdCitiesGet**](SWGCountryApi.md#countriescountryidcitiesget) | **GET** /countries/{countryId}/cities | Get all cities for a country
[**countriesCountryIdGet**](SWGCountryApi.md#countriescountryidget) | **GET** /countries/{countryId} | Get country by unique identifier
[**countriesGet**](SWGCountryApi.md#countriesget) | **GET** /countries | Get all countries


# **countriesCountryIdActivitiesGet**
```objc
-(NSURLSessionTask*) countriesCountryIdActivitiesGetWithCountryId: (NSNumber*) countryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
    vertical: (NSNumber*) vertical
    priorityCity: (NSNumber*) priorityCity
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Get all activities for a country

Get all events for a country sorted by relevance. If priority_city is specified the event for that city are returned first'

### Example 
```objc

NSNumber* countryId = @56; // Country identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* priorityCity = @56; // Prioritize results that belong to this city (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGCountryApi*apiInstance = [[SWGCountryApi alloc] init];

// Get all activities for a country
[apiInstance countriesCountryIdActivitiesGetWithCountryId:countryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
              vertical:vertical
              priorityCity:priorityCity
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCountryApi->countriesCountryIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **NSNumber***| Country identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **priorityCity** | **NSNumber***| Prioritize results that belong to this city | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **countriesCountryIdCitiesGet**
```objc
-(NSURLSessionTask*) countriesCountryIdCitiesGetWithCountryId: (NSNumber*) countryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGCityAggregated>* output, NSError* error)) handler;
```

Get all cities for a country

### Example 
```objc

NSNumber* countryId = @56; // Country identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGCountryApi*apiInstance = [[SWGCountryApi alloc] init];

// Get all cities for a country
[apiInstance countriesCountryIdCitiesGetWithCountryId:countryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGCityAggregated>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCountryApi->countriesCountryIdCitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **NSNumber***| Country identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGCityAggregated>***](SWGCityAggregated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **countriesCountryIdGet**
```objc
-(NSURLSessionTask*) countriesCountryIdGetWithCountryId: (NSNumber*) countryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGCountry* output, NSError* error)) handler;
```

Get country by unique identifier

### Example 
```objc

NSNumber* countryId = @56; // Country identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCountryApi*apiInstance = [[SWGCountryApi alloc] init];

// Get country by unique identifier
[apiInstance countriesCountryIdGetWithCountryId:countryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGCountry* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCountryApi->countriesCountryIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **NSNumber***| Country identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGCountry***](SWGCountry.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **countriesGet**
```objc
-(NSURLSessionTask*) countriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGCountry>* output, NSError* error)) handler;
```

Get all countries

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGCountryApi*apiInstance = [[SWGCountryApi alloc] init];

// Get all countries
[apiInstance countriesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGCountry>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGCountryApi->countriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGCountry>***](SWGCountry.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

