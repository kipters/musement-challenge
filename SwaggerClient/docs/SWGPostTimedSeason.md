# SWGPostTimedSeason

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSString*** | Internal name that identify the season | [optional] 
**fromDay** | **NSDate*** | The day the season starts | YYYY-MM-DD | [optional] 
**toDay** | **NSDate*** | The day the season ends | YYYY-MM-DD | [optional] 
**setups** | [**NSArray&lt;SWGPostActivitySeasonSetup&gt;***](SWGPostActivitySeasonSetup.md) | Season setups | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


