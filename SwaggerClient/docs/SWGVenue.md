# SWGVenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**tips** | [**NSArray&lt;SWGVenueTip&gt;***](SWGVenueTip.md) |  | [optional] 
**status** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**metaTitle** | **NSString*** |  | [optional] 
**metaDescription** | **NSString*** |  | [optional] 
**headline** | **NSString*** |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**reviewsNumber** | **NSNumber*** |  | [optional] 
**reviewsAvg** | **NSNumber*** |  | [optional] 
**relevance** | **NSNumber*** |  | [optional] 
**abstract** | **NSString*** |  | [optional] 
**_description** | **NSString*** | Note this field called description is the venue content (as on i8n entity). | [optional] 
**descriptionHtml** | **NSString*** |  | [optional] 
**address** | **NSString*** |  | [optional] 
**events** | [**NSArray&lt;SWGEvent&gt;***](SWGEvent.md) |  | [optional] 
**verticals** | [**NSArray&lt;SWGVertical&gt;***](SWGVertical.md) |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**eventsCount** | **NSNumber*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**country** | [**SWGCountry***](SWGCountry.md) |  | [optional] 
**showCalendar** | **NSNumber*** |  | [optional] 
**showFlavoursFilter** | **NSNumber*** |  | [optional] 
**showFiltersWhenEventsCount** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


