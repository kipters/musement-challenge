# SWGScheduleGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**featureCode** | **NSString*** |  | [optional] 
**name** | **NSString*** | Name (label) of Feature. | [optional] 
**_description** | **NSString*** |  | [optional] 
**_default** | **NSNumber*** |  | [optional] 
**slots** | [**NSArray&lt;SWGScheduleSlot&gt;***](SWGScheduleSlot.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


