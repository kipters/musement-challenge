# SWGDestination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**verticals** | [**NSArray&lt;SWGVertical&gt;***](SWGVertical.md) |  | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**saves** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**canonicalUrl** | **NSString*** |  | [optional] 
**phone** | **NSString*** |  | [optional] 
**website** | **NSString*** |  | [optional] 
**priceRange** | **NSNumber*** |  | [optional] 
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**address** | **NSString*** |  | [optional] 
**photos** | [**NSArray&lt;SWGDestinationPhoto&gt;***](SWGDestinationPhoto.md) |  | [optional] 
**comments** | [**NSArray&lt;SWGDestinationComment&gt;***](SWGDestinationComment.md) |  | [optional] 
**openingHours** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**specialities** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**rating** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


