# SWGMusementListThirdpartyItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**title** | **NSString*** |  | [optional] 
**subtitle** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**seoTitle** | **NSString*** |  | [optional] 
**seoDescription** | **NSString*** |  | [optional] 
**temporary** | **NSNumber*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**authors** | [**NSArray&lt;SWGCustomer&gt;***](SWGCustomer.md) |  | [optional] 
**views** | **NSNumber*** |  | [optional] 
**scroll** | **NSNumber*** |  | [optional] 
**saves** | **NSNumber*** |  | [optional] 
**listType** | [**SWGListType***](SWGListType.md) |  | [optional] 
**items** | [**NSArray&lt;SWGMusementListMusementItem&gt;***](SWGMusementListMusementItem.md) |  | [optional] 
**itemsCount** | [**SWGListItemsCount***](SWGListItemsCount.md) |  | [optional] 
**city** | [**SWGCity***](SWGCity.md) |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**verticals** | [**NSArray&lt;SWGVertical&gt;***](SWGVertical.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


