# SWGContentApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesActivityUuidContentsLocaleCodeGet**](SWGContentApi.md#activitiesactivityuuidcontentslocalecodeget) | **GET** /activities/{activityUuid}/contents/{localeCode} | Get activity content for a specific locale
[**activitiesActivityUuidContentsLocalePut**](SWGContentApi.md#activitiesactivityuuidcontentslocaleput) | **PUT** /activities/{activityUuid}/contents/{locale} | Update a content for an activity
[**activitiesActivityUuidContentsPost**](SWGContentApi.md#activitiesactivityuuidcontentspost) | **POST** /activities/{activityUuid}/contents | Add content for an activity
[**activityLanguagesGet**](SWGContentApi.md#activitylanguagesget) | **GET** /activity-languages | Get all possible languages for the activities


# **activitiesActivityUuidContentsLocaleCodeGet**
```objc
-(NSURLSessionTask*) activitiesActivityUuidContentsLocaleCodeGetWithActivityUuid: (NSString*) activityUuid
    localeCode: (NSString*) localeCode
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGActivityContent* output, NSError* error)) handler;
```

Get activity content for a specific locale

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
NSString* localeCode = @"localeCode_example"; // Locale code
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGContentApi*apiInstance = [[SWGContentApi alloc] init];

// Get activity content for a specific locale
[apiInstance activitiesActivityUuidContentsLocaleCodeGetWithActivityUuid:activityUuid
              localeCode:localeCode
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGActivityContent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGContentApi->activitiesActivityUuidContentsLocaleCodeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **localeCode** | **NSString***| Locale code | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGActivityContent***](SWGActivityContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidContentsLocalePut**
```objc
-(NSURLSessionTask*) activitiesActivityUuidContentsLocalePutWithActivityUuid: (NSString*) activityUuid
    activityContent: (SWGPutActivityContent*) activityContent
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGActivityContent* output, NSError* error)) handler;
```

Update a content for an activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPutActivityContent* activityContent = [[SWGPutActivityContent alloc] init]; // Activity content put request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGContentApi*apiInstance = [[SWGContentApi alloc] init];

// Update a content for an activity
[apiInstance activitiesActivityUuidContentsLocalePutWithActivityUuid:activityUuid
              activityContent:activityContent
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGActivityContent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGContentApi->activitiesActivityUuidContentsLocalePut: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **activityContent** | [**SWGPutActivityContent***](SWGPutActivityContent.md)| Activity content put request | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGActivityContent***](SWGActivityContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activitiesActivityUuidContentsPost**
```objc
-(NSURLSessionTask*) activitiesActivityUuidContentsPostWithActivityUuid: (NSString*) activityUuid
    activityContent: (SWGPostActivityContent*) activityContent
    xMusementVersion: (NSString*) xMusementVersion
        completionHandler: (void (^)(SWGActivityContent* output, NSError* error)) handler;
```

Add content for an activity

### Example 
```objc

NSString* activityUuid = @"activityUuid_example"; // Activity identifier
SWGPostActivityContent* activityContent = [[SWGPostActivityContent alloc] init]; // Activity content post request
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)

SWGContentApi*apiInstance = [[SWGContentApi alloc] init];

// Add content for an activity
[apiInstance activitiesActivityUuidContentsPostWithActivityUuid:activityUuid
              activityContent:activityContent
              xMusementVersion:xMusementVersion
          completionHandler: ^(SWGActivityContent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGContentApi->activitiesActivityUuidContentsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityUuid** | **NSString***| Activity identifier | 
 **activityContent** | [**SWGPostActivityContent***](SWGPostActivityContent.md)| Activity content post request | 
 **xMusementVersion** | **NSString***|  | [optional] 

### Return type

[**SWGActivityContent***](SWGActivityContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **activityLanguagesGet**
```objc
-(NSURLSessionTask*) activityLanguagesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGTranslatedMetadata>* output, NSError* error)) handler;
```

Get all possible languages for the activities

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGContentApi*apiInstance = [[SWGContentApi alloc] init];

// Get all possible languages for the activities
[apiInstance activityLanguagesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGTranslatedMetadata>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGContentApi->activityLanguagesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGTranslatedMetadata>***](SWGTranslatedMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

