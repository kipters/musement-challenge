# SWGEditorialCategoryApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**editorialCategoriesEditorialcategoryIdActivitiesGet**](SWGEditorialCategoryApi.md#editorialcategorieseditorialcategoryidactivitiesget) | **GET** /editorial-categories/{editorialcategoryId}/activities | Search activities within a given editorial category
[**editorialCategoriesEditorialcategoryIdFlavoursGet**](SWGEditorialCategoryApi.md#editorialcategorieseditorialcategoryidflavoursget) | **GET** /editorial-categories/{editorialcategoryId}/flavours | Get all flavours connected to an editorial category
[**editorialCategoriesEditorialcategoryIdGet**](SWGEditorialCategoryApi.md#editorialcategorieseditorialcategoryidget) | **GET** /editorial-categories/{editorialcategoryId} | Get an editorial category by ID
[**editorialCategoriesGet**](SWGEditorialCategoryApi.md#editorialcategoriesget) | **GET** /editorial-categories | Get all editorial categories


# **editorialCategoriesEditorialcategoryIdActivitiesGet**
```objc
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdActivitiesGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    vertical: (NSNumber*) vertical
    city: (NSNumber*) city
    category: (NSNumber*) category
    venue: (NSNumber*) venue
    dateFrom: (NSDate*) dateFrom
    dateTo: (NSDate*) dateTo
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
    sortBy: (NSString*) sortBy
        completionHandler: (void (^)(NSArray<SWGEvent>* output, NSError* error)) handler;
```

Search activities within a given editorial category

Get all activities for an editorial category sorted by relevance. Only the events that have at least one active date in the period specified between date_from and date_to are returned. If date_from and date_to are not set then the period that will be set will be one year from the date of the request'

### Example 
```objc

NSNumber* editorialcategoryId = @56; // Editorial category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* vertical = @56; // Vertical identifier (optional)
NSNumber* city = @56; // City identifier (optional)
NSNumber* category = @56; // Category identifier (optional)
NSNumber* venue = @56; // Venue identifier (optional)
NSDate* dateFrom = @"2013-10-20"; // Start date | Use format: YYYY-MM-DD (optional)
NSDate* dateTo = @"2013-10-20"; // To date | Use format: YYYY-MM-DD (optional)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)
NSString* sortBy = @"relevance"; // Sorting strategy (optional) (default to relevance)

SWGEditorialCategoryApi*apiInstance = [[SWGEditorialCategoryApi alloc] init];

// Search activities within a given editorial category
[apiInstance editorialCategoriesEditorialcategoryIdActivitiesGetWithEditorialcategoryId:editorialcategoryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              vertical:vertical
              city:city
              category:category
              venue:venue
              dateFrom:dateFrom
              dateTo:dateTo
              offset:offset
              limit:limit
              sortBy:sortBy
          completionHandler: ^(NSArray<SWGEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGEditorialCategoryApi->editorialCategoriesEditorialcategoryIdActivitiesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **editorialcategoryId** | **NSNumber***| Editorial category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **vertical** | **NSNumber***| Vertical identifier | [optional] 
 **city** | **NSNumber***| City identifier | [optional] 
 **category** | **NSNumber***| Category identifier | [optional] 
 **venue** | **NSNumber***| Venue identifier | [optional] 
 **dateFrom** | **NSDate***| Start date | Use format: YYYY-MM-DD | [optional] 
 **dateTo** | **NSDate***| To date | Use format: YYYY-MM-DD | [optional] 
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]
 **sortBy** | **NSString***| Sorting strategy | [optional] [default to relevance]

### Return type

[**NSArray<SWGEvent>***](SWGEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editorialCategoriesEditorialcategoryIdFlavoursGet**
```objc
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdFlavoursGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGFlavour* output, NSError* error)) handler;
```

Get all flavours connected to an editorial category

### Example 
```objc

NSNumber* editorialcategoryId = @56; // Editorial category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGEditorialCategoryApi*apiInstance = [[SWGEditorialCategoryApi alloc] init];

// Get all flavours connected to an editorial category
[apiInstance editorialCategoriesEditorialcategoryIdFlavoursGetWithEditorialcategoryId:editorialcategoryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGFlavour* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGEditorialCategoryApi->editorialCategoriesEditorialcategoryIdFlavoursGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **editorialcategoryId** | **NSNumber***| Editorial category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGFlavour***](SWGFlavour.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editorialCategoriesEditorialcategoryIdGet**
```objc
-(NSURLSessionTask*) editorialCategoriesEditorialcategoryIdGetWithEditorialcategoryId: (NSNumber*) editorialcategoryId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(SWGEditorialCategory* output, NSError* error)) handler;
```

Get an editorial category by ID

### Example 
```objc

NSNumber* editorialcategoryId = @56; // Editorial category identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGEditorialCategoryApi*apiInstance = [[SWGEditorialCategoryApi alloc] init];

// Get an editorial category by ID
[apiInstance editorialCategoriesEditorialcategoryIdGetWithEditorialcategoryId:editorialcategoryId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(SWGEditorialCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGEditorialCategoryApi->editorialCategoriesEditorialcategoryIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **editorialcategoryId** | **NSNumber***| Editorial category identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**SWGEditorialCategory***](SWGEditorialCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editorialCategoriesGet**
```objc
-(NSURLSessionTask*) editorialCategoriesGetWithXMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(NSArray<SWGEditorialCategory>* output, NSError* error)) handler;
```

Get all editorial categories

### Example 
```objc

NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSNumber* offset = @0; // Pagination offset (optional) (default to 0)
NSNumber* limit = @10; // Max number of items in the response (optional) (default to 10)

SWGEditorialCategoryApi*apiInstance = [[SWGEditorialCategoryApi alloc] init];

// Get all editorial categories
[apiInstance editorialCategoriesGetWithXMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              offset:offset
              limit:limit
          completionHandler: ^(NSArray<SWGEditorialCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGEditorialCategoryApi->editorialCategoriesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **offset** | **NSNumber***| Pagination offset | [optional] [default to 0]
 **limit** | **NSNumber***| Max number of items in the response | [optional] [default to 10]

### Return type

[**NSArray<SWGEditorialCategory>***](SWGEditorialCategory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

