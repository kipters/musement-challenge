# SWGDate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **NSString*** |  | [optional] 
**soldOut** | **NSNumber*** |  | [optional] 
**price** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**discountedPrice** | [**SWGPrice***](SWGPrice.md) |  | [optional] 
**discountAmount** | [**SWGPrice***](SWGPrice.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


