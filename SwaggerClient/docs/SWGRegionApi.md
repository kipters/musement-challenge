# SWGRegionApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listsListIdRegionsGet**](SWGRegionApi.md#listslistidregionsget) | **GET** /lists/{listId}/regions | Get all available regions for the list
[**verticalsVerticalIdRegionsGet**](SWGRegionApi.md#verticalsverticalidregionsget) | **GET** /verticals/{verticalId}/regions | Get all available regions for the vertical


# **listsListIdRegionsGet**
```objc
-(NSURLSessionTask*) listsListIdRegionsGetWithListId: (NSNumber*) listId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get all available regions for the list

### Example 
```objc

NSNumber* listId = @56; // List identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGRegionApi*apiInstance = [[SWGRegionApi alloc] init];

// Get all available regions for the list
[apiInstance listsListIdRegionsGetWithListId:listId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGRegionApi->listsListIdRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listId** | **NSNumber***| List identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verticalsVerticalIdRegionsGet**
```objc
-(NSURLSessionTask*) verticalsVerticalIdRegionsGetWithVerticalId: (NSNumber*) verticalId
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
        completionHandler: (void (^)(NSArray<SWGRegion>* output, NSError* error)) handler;
```

Get all available regions for the vertical

### Example 
```objc

NSNumber* verticalId = @56; // Vertical identifier
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)

SWGRegionApi*apiInstance = [[SWGRegionApi alloc] init];

// Get all available regions for the vertical
[apiInstance verticalsVerticalIdRegionsGetWithVerticalId:verticalId
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
          completionHandler: ^(NSArray<SWGRegion>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGRegionApi->verticalsVerticalIdRegionsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verticalId** | **NSNumber***| Vertical identifier | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]

### Return type

[**NSArray<SWGRegion>***](SWGRegion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

