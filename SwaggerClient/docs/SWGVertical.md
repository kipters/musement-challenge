# SWGVertical

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**name** | **NSString*** |  | 
**active** | **NSNumber*** |  | 
**code** | **NSString*** |  | 
**count** | **NSNumber*** |  | [optional] 
**slug** | **NSString*** |  | 
**url** | **NSString*** |  | 
**metaTitle** | **NSString*** |  | [optional] 
**metaDescription** | **NSString*** |  | [optional] 
**coverImageUrl** | **NSString*** |  | [optional] 
**topEvents** | [**NSArray&lt;SWGEvent&gt;***](SWGEvent.md) |  | [optional] 
**relevance** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


