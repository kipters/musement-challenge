# SWGScheduleProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**holderCode** | **NSString*** |  | [optional] 
**name** | **NSString*** | Name (label) of Holder. | [optional] 
**type** | **NSString*** |  | [optional] 
**productId** | **NSString*** |  | [optional] 
**minBuy** | **NSNumber*** |  | [optional] 
**maxBuy** | **NSNumber*** |  | [optional] 
**availability** | **NSNumber*** |  | [optional] 
**rawPrice** | [**SWGPrice***](SWGPrice.md) | This is the price to the user without any discount applied. (same currency as retailPrice) | [optional] 
**discountAmount** | [**SWGPrice***](SWGPrice.md) | Amount of discount in same currency as retailPrice (only SeatPrice.discount used because its not in Cart) | [optional] 
**retailPrice** | [**SWGPrice***](SWGPrice.md) | Price with discount applied (only SeatPrice.discount used because its not in Cart) | [optional] 
**serviceFee** | [**SWGPrice***](SWGPrice.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


