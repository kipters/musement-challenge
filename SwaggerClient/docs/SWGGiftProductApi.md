# SWGGiftProductApi

All URIs are relative to *https://developers.musement.com/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**giftsPost**](SWGGiftProductApi.md#giftspost) | **POST** /gifts | Currently can be used only with &#39;FROM-GIFTBOX&#39; strategy - it starts Redeem process for given Giftbox (giftbox_code) and with chosen Activity (giftbox_type_item_id)


# **giftsPost**
```objc
-(NSURLSessionTask*) giftsPostWithGiftProduct: (SWGPostGiftProduct*) giftProduct
    xMusementVersion: (NSString*) xMusementVersion
    acceptLanguage: (NSString*) acceptLanguage
    xMusementCurrency: (NSString*) xMusementCurrency
        completionHandler: (void (^)(SWGGiftProduct* output, NSError* error)) handler;
```

Currently can be used only with 'FROM-GIFTBOX' strategy - it starts Redeem process for given Giftbox (giftbox_code) and with chosen Activity (giftbox_type_item_id)

### Example 
```objc

SWGPostGiftProduct* giftProduct = [[SWGPostGiftProduct alloc] init]; // Gift information
NSString* xMusementVersion = @"xMusementVersion_example"; //  (optional)
NSString* acceptLanguage = @"en-US"; //  (optional) (default to en-US)
NSString* xMusementCurrency = @"USD"; //  (optional) (default to USD)

SWGGiftProductApi*apiInstance = [[SWGGiftProductApi alloc] init];

// Currently can be used only with 'FROM-GIFTBOX' strategy - it starts Redeem process for given Giftbox (giftbox_code) and with chosen Activity (giftbox_type_item_id)
[apiInstance giftsPostWithGiftProduct:giftProduct
              xMusementVersion:xMusementVersion
              acceptLanguage:acceptLanguage
              xMusementCurrency:xMusementCurrency
          completionHandler: ^(SWGGiftProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGGiftProductApi->giftsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **giftProduct** | [**SWGPostGiftProduct***](SWGPostGiftProduct.md)| Gift information | 
 **xMusementVersion** | **NSString***|  | [optional] 
 **acceptLanguage** | **NSString***|  | [optional] [default to en-US]
 **xMusementCurrency** | **NSString***|  | [optional] [default to USD]

### Return type

[**SWGGiftProduct***](SWGGiftProduct.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

