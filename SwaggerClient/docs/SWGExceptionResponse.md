# SWGExceptionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** | Error code | [optional] 
**message** | **NSString*** | Error message | [optional] 
**data** | **NSString*** | Extra information about the error | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


