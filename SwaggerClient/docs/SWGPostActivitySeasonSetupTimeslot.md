# SWGPostActivitySeasonSetupTimeslot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | **NSString*** | Time in the format HH:MM | [optional] 
**availability** | **NSNumber*** | Number of available places | Set -1 for infinite availability | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


